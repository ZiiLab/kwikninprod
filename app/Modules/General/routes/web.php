<?php

Route::group(['module' => 'General', 'middleware' => ['web'], 'namespace' => 'App\Modules\General\Controllers'], function() {

    Route::get('/', 'GeneralController@showHome')->name('showHome');

     Route::group(['prefix' => 'events', 'middleware' => ['user']],function(){
      Route::get('/', 'GeneralController@showEvents')->name('showEvents');
      Route::get('/{id}', 'GeneralController@showEventDetail')->name('showEventDetail');

  Route::get('/dashboard', 'GeneralController@showDashboard')->name('showDashboard');
  Route::get('/tags', 'GeneralController@showListTags')->name('showListTags');
  Route::get('/tags/{id}', 'GeneralController@hundleDeleteTag')->name('hundleDeleteTag');
  Route::get('/addEvent', 'GeneralController@showAddEvent')->name('showAddEvent');
  Route::get('/events', 'GeneralController@showListEvents')->name('showListEvents');
  Route::get('/events/{id}', 'GeneralController@hundleDeleteEvent')->name('hundleDeleteEvent');
  Route::get('/bookings', 'GeneralController@showListBookings')->name('showListBookings');
  Route::post('/tags', 'GeneralController@hundleAddTag')->name('hundleAddTag');
  Route::post('/addEvent', 'GeneralController@hundleAddEvent')->name('hundleAddEvent');
  Route::post('/bookings', 'GeneralController@apiBookingEvent')->name('apiBookingEvent');


    });

    Route::group(['prefix' => 'store', 'middleware' => ['user']],function(){
        Route::get('/', 'GeneralController@showStore')->name('showStore');

    });




});
