@extends('frontOffice.layout')


@section('content')

	<style class="cp-pen-styles">#carousel3d .carousel-3d-slide {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-flex: 1;
      -ms-flex: 1;
          flex: 1;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  text-align: center;
  background-color: #fff;
  padding: 10px;
  -webkit-transition: all .4s;
  transition: all .4s;
}
#carousel3d .carousel-3d-slide.current {
  background-color: #333;
  color: #fff;
}
#carousel3d .carousel-3d-slide.current span {
  font-size: 20px;
  font-weight: 500;
}

.prev[data-v-43e93932] {
    left: 40px;
    text-align: left;
    line-height: 15px!important;
        color: black;
				top: 0px;
}

.next[data-v-43e93932] {
    right: 60px;
    text-align: right;
        line-height: 15px!important;
        color: black;
				top: 0px;
}
.slide-local{
	width: 450px !important;
	height: 200px !important;
	padding: 0px!important;
	border:none!important;
	border-radius: 5px;
}
        .frontside {
            position: relative;
            -webkit-transform: rotateY(0deg);
            -ms-transform: rotateY(0deg);
            z-index: 2;
        }

        .backside {
            position: absolute;
            top: 0;
            left: 0;
            background: white;
            -webkit-transform: rotateY(-180deg);
            -moz-transform: rotateY(-180deg);
            -o-transform: rotateY(-180deg);
            -ms-transform: rotateY(-180deg);
            transform: rotateY(-180deg);
            -webkit-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
            -moz-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
            box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
        }

        .frontside,
        .backside {
            -webkit-backface-visibility: hidden;
            -moz-backface-visibility: hidden;
            -ms-backface-visibility: hidden;
            backface-visibility: hidden;
            -webkit-transition: 1s;
            -webkit-transform-style: preserve-3d;
            -moz-transition: 1s;
            -moz-transform-style: preserve-3d;
            -o-transition: 1s;
            -o-transform-style: preserve-3d;
            -ms-transition: 1s;
            -ms-transform-style: preserve-3d;
            transition: 1s;
            transform-style: preserve-3d;
        }

        .frontside .card,
        .backside .card {
            min-height: 200px;
        }

        .backside .card a {
            font-size: 18px;
            color: #007b5e !important;
        }

        .frontside .card .card-title,
        .backside .card .card-title {
            color: #007b5e !important;
        }

        .frontside .card .card-body img {
            width: 120px;
            height: 120px;
            border-radius: 50%;
        }

.carousel-3d-slider {
	    width: 480px!important;
	    height: 300px!important;
}
.carousel-3d-container {
	/* height: 350px!important; */
}
		.priceSpan {
			text-align: center;
			width: 50px;
			color: black;
			background: gold;
			right: 30px;
			position: absolute;
		}

		.bookmark-product{
			position: absolute;
			width: 23%;
			top: -11px;
			left: 5px;
		}

		.bookmark-event{
			position: absolute;
			width: 23%;
			top: -11px;
			left: 5px;
		}

		.bookmark-product-p{
			position: absolute;
			width: 23%;
			top: -11px;
			left: 5px;
		}

		.bookmark-event-e{
			position: absolute;
			width: 23%;
			top: -11px;
			left: 5px;
		}
</style>

	<div id="sidebar-bg">
	@include('frontOffice.inc.sidebar')
	<main id="col-main">
<div class="row">
<div class="col-md-12" style="margin-top: 20px;
    margin-bottom: -25px;
    text-align: center;">
	<h4  class="heading-extra-margin-bottom">Recommended Sessions For You</h4>
	@if(Session::has('message'))
		<div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-block">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<strong>{{ Session::get('message') }}</strong>
		</div>
	@endif
</div>
</div>
		<div class="row">
			<div style="text-align: center" class="col-md-12">
				<div id="carousel3d">
					<carousel-3d :perspective="0" :space="200"
								 :display="{{count($sessions)}}" :controls-visible="true" :controls-prev-html="'❬'" :controls-next-html="'❭'" :controls-width="30" :controls-height="60" :clickable="true" :autoplay="false" :autoplay-timeout="5000">
@foreach($sessions as $key => $session)
                            <slide class="slide-local" :index="{{$key}}">
                                <div style="height: 200px; width: 450px" class="frontside">
                                    <div style="height: 200px; width: 450px;background-color: #02536B" class="card">
                                        <div class="card-body text-center" style="padding: 0!important;">
                                            <figure class="figure">
                                                <img style="width: 50px; height: 50px" class=" img-fluid" src="{{asset($session->streamer->user->image)}}" alt="card image">
                                                <figcaption style="color: white" class="figure-caption">{{$session->streamer->user->first_name.' '.$session->streamer->user->last_name}}</figcaption>
                                            </figure>
                                            <h6 class="card-title" style="color: white!important;">{{$session->title}}</h6>
                                            <div class="row" style="margin: 0px">
                                                <div style="background-color: white; " class="col-md-4"><span style="color: black;font-size: 12px"> Session Date : {{$session->start_date->format('Y/m/d H:i:s')}}</span></div>
                                                <div style="background-color: white; " class="col-md-4">
                                                    <span style="color: black;font-size: 12px"> Subscriptions : {{$session->p_nbr}}</span>

                                                </div>
                                                <div style="background-color: white; " class="col-md-4">
                                                    <span style="color: black;font-size: 12px"> Session Price : {{$session->price}} $</span>
                                                    @if($session->streamer->user->id == Auth::id()) @else @if(\App\Modules\Broadcasting\Models\SessionBooking::where('user_id',Auth::id())->where('stream_id',$session->id)->first())
														<a style="cursor: not-allowed" href="#">Subscribed !</a> @else <a  href="{{route('handleSubscriptionToSession',$session->id)}}" target="_parent" class=""> <i class="fas fa-star"></i> Subscribe</a> @endif @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							</slide>
						@endforeach


					</carousel-3d>
				</div>
			</div>
		</div>
				<div id="video-search-header">
								<div id="search-icon-more"></div>
								<input type="text" placeholder="Find The Healer You Seek" aria-label="Search">
								<div id="video-search-header-filtering">
									<form id="video-search-header-filtering-padding">
										<div class="row">
											<div class="col-sm extra-padding">
												<h5>Type:</h5>

												<div class="row">
													<div class="col-sm">
														<label class="checkbox-pro-container">Type 1
														  <input type="checkbox" checked="checked" id="movies-type">
														  <span class="checkmark-pro"></span>
														</label>

														<label class="checkbox-pro-container">Type 2
														  <input type="checkbox" id="tv-type">
														  <span class="checkmark-pro"></span>
														</label>
													</div><!-- close .col -->
													<div class="col">
														<label class="checkbox-pro-container">Type 3
														  <input type="checkbox" id="movie-type">
														  <span class="checkmark-pro"></span>
														</label>

														<label class="checkbox-pro-container">Type 4
														  <input type="checkbox" id="documentary-type">
														  <span class="checkmark-pro"></span>
														</label>
													</div><!-- close .col -->
												</div><!-- close .row -->

												<div class="dotted-dividers-pro"></div>
											</div><!-- close .col -->
											<div class="col-sm extra-padding">
												<h5>Genres:</h5>
												<select class="custom-select">
												  <option selected>All Genres</option>
												  <option value="1">Action</option>
												  <option value="2">Adventure</option>
												  <option value="3">Drama</option>
												  <option value="4">Animation</option>
												  <option value="5">Documentary</option>
												  <option value="6">Drama</option>
												  <option value="7">Horror</option>
												  <option value="8">Thriller</option>
												  <option value="9">Fantasy</option>
												  <option value="10">Romance</option>
												  <option value="11">Sci-Fi</option>
												  <option value="12">Western</option>
												</select>
												<div class="dotted-dividers-pro"></div>
											</div><!-- close .col -->
											<div class="col-sm extra-padding">
												<h5>Country:</h5>
												<select class="custom-select">
												  <option selected>All Countries</option>
												  <option value="1">Argentina</option>
												  <option value="2">Australia</option>
												  <option value="3">Bahamas</option>
												  <option value="4">Belgium</option>
												  <option value="5">Brazil</option>
												  <option value="6">Canada</option>
												  <option value="7">Chile</option>
												  <option value="8">China</option>
												  <option value="9">Denmark</option>
												  <option value="10">Ecuador</option>
												  <option value="11">France</option>
												  <option value="12">Germany</option>
												  <option value="13">Greece</option>
												  <option value="14">Guatemala</option>
												  <option value="15">Italy</option>
												  <option value="16">Japan</option>
												  <option value="17">asdfasdf</option>
												  <option value="18">Korea</option>
												  <option value="19">Malaysia</option>
												  <option value="20">Monaco</option>
												  <option value="21">Morocco</option>
												  <option value="22">New Zealand</option>
												  <option value="23">Panama</option>
												  <option value="24">Portugal</option>
												  <option value="25">Russia</option>
												  <option value="26">United Kingdom</option>
												  <option value="27">United States</option>
												</select>
												<div class="dotted-dividers-pro"></div>
											</div><!-- close .col -->
										</div><!-- close .row -->
										<div id="video-search-header-buttons">
											<a href="#!" class="btn btn-green-pro">Filter Search</a>
											<a href="#!" class="btn">Reset</a>
										</div><!-- close #video-search-header-buttons -->
									</form><!-- #video-search-header-filtering-padding -->
								</div><!-- close #video-search-header-filtering -->
							</div><!-- close .video-search-header -->

				<div class="clearfix"></div>

				<div class="dashboard-container">

				<h4  class="heading-extra-margin-bottom">Recommended Healing Items For You</h4>
					<div style="border-bottom: inset; margin-bottom: 25px" class="row">


						@foreach($products as $product)
							<div class="col-12 col-md-6 col-lg-4 col-xl-4">


								<div class="item-playlist-container-skrn">
										@if (Auth::check())


									<a href="#" class="bookmark-product" data-product="{{$product->id}}"><img

											@if (hasBookmark($product->id,'App\Modules\User\Models\Product'))
												src="{{asset('images/bookmark.png')}}" alt="bookmark"

												@else
													src="{{asset('images/bookmark_add.png')}}" alt="bookmark_add"

											@endif

										> </a>

										@else

											<a href="#" class="bookmark-product-p" data-product="{{$product->id}}"><img src="{{asset('images/bookmark_add.png')}}" alt="bookmark_add" > </a>

											@endif
									<a class="showProduct" href="#" data-toggle="modal" data-target="#showProduct" data-id="{{$product->id}}" data-quantity="{{$product->quantity}}" data-img="{{$product->image}}" data-price="{{$product->price}}" data-name="{{$product->name}}" data-desc="{{$product->description}}">
										<img src="{{asset($product->image)}}" alt="Listing">
									</a>
									<span class="priceSpan">{{$product->price}} $</span>
									<div class="item-playlist-text-skrn">
										<img src="{{asset($product->store->user->image ? $product->store->user->image : 'frontOffice/images/unknown.png')}}" alt="User Profile">
										<h5>{{$product->name}}</h5>
										<h6><a href="{{route('showPublicProfile',$product->store->user->slug)}}">{{$product->store->name}}</a></h6>

									</div><!-- close .item-listing-text-skrn -->
								</div><!-- close .item-playlist-container-skrn -->
							</div><!-- close .col -->
						@endforeach




					<div style="text-align: center; margin-bottom: 20px" class="col-md-12">
						<a href="{{route('showStore')}}" class="btn">Show More</a>
					</div>


				</div><!-- close .row -->

								<h4  class="heading-extra-margin-bottom">Top Events</h4>
								<div class="row">


@foreach(\App\Modules\User\Models\Event::take(3)->get() as $event)
					<div class="col-12 col-md-6 col-lg-4 col-xl-4">
						<div style="height: 300px;" class="item-listing-container-skrn">
							@if (Auth::check())
							<a href="#" class="bookmark-event" data-event="{{$event->id}}"><img



								@if (hasBookmark($event->id,'App\Modules\User\Models\Event'))
									src="{{asset('images/bookmark.png')}}" alt="bookmark"

									@else
										src="{{asset('images/bookmark_add.png')}}" alt="bookmark_add"

								@endif

								> </a>
								@else
									<a href="#" class="bookmark-event-e"><img src="{{asset('images/bookmark_add.png')}}" alt="bookmark_add"> </a>
							@endif

							<a href="{{route('showEventDetail', $event->id)}}"><img style="height: 170.66px;" src="{{asset($event->medias->first()->link)}}" alt="Listing"></a>
							<div class="item-listing-text-skrn">
								<div class="item-listing-text-skrn-vertical-align">
									<h6><a href="{{route('showEventDetail', $event->id)}}">{{$event->title}}</a></h6>
									<p style="text-align: center">Publisher : <a href="{{route('showPublicEvents', $event->user->slug)}}">{{$event->user->first_name}}</a></p>

								</div>
								<!-- close .item-listing-text-skrn-vertical-align -->
							</div><!-- close .item-listing-text-skrn -->
						</div><!-- close .item-listing-container-skrn -->
					</div><!-- close .col -->
@endforeach
								</div>

					<ul class="page-numbers">
					<a style="border:none" class="btn" href="{{route('showEvents')}}">Show More</a>
				</ul>


			</div><!-- close .dashboard-container -->
			</main>
	</div>
	@include('User::frontOffice.modals.loginModal')
	@include('User::frontOffice.modals.userVerificationModal')
	@include('General::store.modals.showProduct')
	  @include('General::store.modals.buyProduct')





	<script>
        $(document).on("click", ".showProduct", function (event) {
					event.preventDefault();
					  var productId = $(this).data('id');
            var img = $(this).data('img');
            var name = $(this).data('name');
            var price = $(this).data('price');
            var desc = $(this).data('desc');
            $("#productImg").attr("src",img);
            $("#Pdesc").text(desc);
            $("#Pname").text(name);
            $("#Pprice").text(price +' $');

						$('#showProduct').data('productId',productId);

						var base_url = '{!! url('/') !!}';
						var href = base_url+'/user/profile/paypal/'+productId;
						$('#buyForm').attr('action',href);

						$('#error').html(' ');
						if ($(this).data('quantity') > 0) {
							$("input[type='number']").attr('max', $(this).data('quantity'));
                            $("#Pbuy").attr('disabled', false);
                            $("#Pbuy").css('cursor','pointer')
							$('#error').html('Quantity available: '+$(this).data('quantity'));


						}else {
							$("input[type='number']").attr('max', 1);
							$("#Pbuy").attr('disabled', true);
							$("#Pbuy").css('cursor','not-allowed')
							$('#error').html('NOT AVAILABLE');
						}



        });
	</script>

@endsection

@section('js')

<script src={{asset('frontOffice/js/carousel/prod-assets.js')}}></script>
<script src={{asset('frontOffice/js/carousel/cdn-vue.js')}}></script>
<script src={{asset('frontOffice/js/carousel/carousel.js')}}></script>
<script >new Vue({
  el: '#carousel3d',
  data: {
    slides: 7
  },
  components: {
    'carousel-3d': Carousel3d.Carousel3d,
    'slide': Carousel3d.Slide
  }
})
//# sourceURL=pen.js
</script>

@if ($message = Session::get('success'))

	<script type="text/javascript">
			toastr.success('{{$message}}')
	</script>
			<?php Session::forget('success');?>
@endif
<script type="text/javascript">
$('.bookmark-product').on('click',function(event) {

				event.preventDefault();
					var bookmarkId;
					var data;
					bookmarkId = $(this).data('product');

					data = {
					'bookmarkId' : bookmarkId,
					'bookmark' : 'product'
					}

					var img = $(this).find('img');


				$.post('{{route('apiHandleBookmark')}}',
				{
				'_token': $('meta[name=csrf-token]').attr('content'), data

				})
				.done(function (res) {

					if (res.event) {
							img[0].src = "{{asset('images/bookmark.png')}}";
							img[0].alt = "bookmark";
					}else {
						img[0].src = "{{asset('images/bookmark_add.png')}}";
						img[0].alt = "bookmark_add";
					}

				})
});
</script>

<script type="text/javascript">
$('.bookmark-event').on('click',function(event) {

				event.preventDefault();
					var bookmarkId;
					var data;
					bookmarkId = $(this).data('event');

					data = {
					'bookmarkId' : bookmarkId,
					'bookmark' : 'event'
					}

					var img = $(this).find('img');


				$.post('{{route('apiHandleBookmark')}}',
				{
				'_token': $('meta[name=csrf-token]').attr('content'), data

				})
				.done(function (res) {
						if (res.event) {
								img[0].src = "{{asset('images/bookmark.png')}}";
								img[0].alt = "bookmark";
						}else {
							img[0].src = "{{asset('images/bookmark_add.png')}}";
							img[0].alt = "bookmark_add";
						}
				})
});
</script>

@endsection
