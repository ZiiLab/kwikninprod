<style media="screen">
    .has-error input {
    border : 2px solid red !important;
  }

  .help-block strong{
    font-size: 10px;
    color: red;
  }

  .hide{
    display: none;
  }

  </style>
<style media="screen">
  #error{
      font-size: 9px;
      color: red;
      margin-top: 8px;
      text-align: center;

  }

  input[type=number]{
    width: 100%;
padding: 10px;
margin-top: 12px;
  }
    #Pbuy{
        margin-top:15px !important;
        margin-buttom:15px !important;
    }
</style>
<div  class="modal fade" id="buyProduct" tabindex="-1" role="dialog" aria-labelledby="startStreamingModal" aria-hidden="true" data-productId="">
    <button type="button" class="close float-close-pro" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <div style="max-width: 70%!important;" class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header-pro">
                <h6>Submit Purchase</h6>
            </div>
            <div class="modal-body-pro">

                <div class="card">
                    <div class="card-body">
                    <div class="row">
                      <div class="col-sm">
                              <h6>Please Specify the shippement address</h6>
                      </div>
                    </div>

                      <div class="row">
                          <div class="col-sm">
                              <div class="form-group">
                                  <label for="last-name" class="col-form-label">City:</label>
                                  <input type="text" class="form-control" name="city" id="city" placeholder="City" >
                                      @if ($errors->has('city'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('city') }}</strong>
                                      </span>
                                      @endif
                              </div>
                          </div><!-- close .col -->
                          <div class="col-sm">
                              <div class="form-group">
                                  <label for="last-name" class="col-form-label">Zip Code:</label>
                                  <input type="text" class="form-control" name="postal_code" id="postal_code" placeholder="xxxxxx">
                                      @if ($errors->has('postal_code'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('postal_code') }}</strong>
                                      </span>
                                      @endif
                              </div>
                          </div><!-- close .col -->
                      </div><!-- close .row -->
                      <div class="row">
                          <div class="col-sm">
                              <div class="form-group">
                                  <label for="e-mail" class="col-form-label">Full Address</label>
                                  <input type="text" class="form-control" name="address" id="address" placeholder="Address">
                                      @if ($errors->has('address'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('address') }}</strong>
                                      </span>
                                      @endif
                              </div>
                          </div><!-- close .col -->


                      </div><!-- close .row -->
                      <div class="row" style="    margin-top: 40px;
    margin-bottom: -30px;">
                        <div class="col-sm">
                                <h6>Please Select your payment methode</h6>
                        </div>
                      </div>
                      <div class="row payment-method-container">
                          <div class="registration-billing-form">
                              <div class="row">
                                  <div class="col-md">

                                      <div id="visaToClick" class="jumbotron jumbotron-fluid jumbotron-pro jumbotron-selected">
                                          <div class="container">
                                              <i class="checkVisa fas fa-check-circle"></i>
                                              <img src="{{asset('frontOffice/images/demo/billing-credit-card.png')}}" alt="Credit Card">
                                              <h6 class="light-weight-heading">Pay with Credit Card</h6>
                                          </div>
                                      </div><!-- close .jumbotron -->

                                  </div><!-- close .col-md -->
                                  <div class="col-md">

                                      <div id="paypalToClick" class="jumbotron jumbotron-fluid jumbotron-pro">
                                          <div class="container">
                                              <i class="checkPaypal"></i>
                                              <img src="{{asset('frontOffice/images/demo/billing-paypal.png')}}" alt="Credit Card">
                                              <h6 class="light-weight-heading">Pay with PayPal</h6>
                                          </div>
                                      </div><!-- close .jumbotron -->

                                  </div><!-- close .col-md -->
                              </div><!-- close .row -->


                              <div class="row" style="display: block;">
                                @if ($message = Session::get('error'))
                                  <div class="w3-panel w3-red w3-display-container">
                                      <span onclick="this.parentElement.style.display='none'"
                                      class="w3-button w3-red w3-large w3-display-topright">&times;</span>
                                      <p>{!! $message !!}</p>
                                  </div>
                                  <?php Session::forget('error');?>
                                  @endif
                                  <div id="visa">
                                      <div class="billing-form-pro">
                                          <form action="{{route('handleRegisterUserStepTwo')}}" method="post" id="form">
                                              {{csrf_field()}}

                                              <div class="form-group">
                                                  <label for="cardholder" class="col-form-label">Cardholder Name:</label>
                                                  <br>
                                                  <div class="form-check-inline">

                                                      <label class="form-check-label">
                                                          <input type="radio" class="form-check-input cardValue" name="card_holder_name" checked value="visa" @if (isset($data))
                                                          {{$data['card_holder_name']=="visa" ? 'checked='.'"checked"' : ''  }}
                                                          @endif

                                                          >Visa
                                                      </label>
                                                  </div>
                                                  <div class="form-check-inline">
                                                      <label class="form-check-label">
                                                          <input type="radio" class="form-check-input cardValue" name="card_holder_name" value="mastercard" @if (isset($data))
                                                          {{$data['card_holder_name']=="mastercard" ? 'checked='.'"checked"' : ''  }}
                                                          @endif

                                                          >Mastercard
                                                      </label>
                                                  </div>
                                                  <div class="form-check-inline">
                                                      <label class="form-check-label">
                                                          <input type="radio" class="form-check-input cardValue" name="card_holder_name" value="amex" @if (isset($data))
                                                          {{$data['card_holder_name']=="amex" ? 'checked='.'"checked"' : ''  }}
                                                          @endif

                                                          >American Express
                                                      </label>
                                                  </div>

                                                  <div class="form-check-inline">
                                                      <label class="form-check-label">
                                                          <input type="radio" class="form-check-input cardValue" name="card_holder_name" value="discover" @if (isset($data))
                                                          {{$data['card_holder_name']=="discover" ? 'checked='.'"checked"' : ''  }}
                                                          @endif

                                                          >Discover
                                                      </label>
                                                  </div>

                                                  @if ($errors->has('card_holder_name'))
                                                  <br>
                                                  <span class="help-block">
                                                      <strong>{{ $errors->first('card_holder_name') }}</strong>
                                                  </span>
                                                  @endif
                                              </div>

                                              <div class="form-group">
                                                  <label for="cc-number" class="col-form-label">Card Number: <small class="text-muted"><span class="cc-brand"></span></small></label>
                                                  <input @if (isset($data)) value="{{$data['card_number']}}"
                                                  @endif name="card_number" type="tel" class="form-control cc-number" autocomplete="cc-number" id="cc-number">
                                                  @if ($errors->has('card_number'))
                                                  <span class="help-block">
                                                      <strong>{{ $errors->first('card_number') }}</strong>
                                                  </span>
                                                  @endif
                                              </div>

                                              <div class="row adjust-margin-top adjust-margin-bottom">
                                                  <div class="col-sm">
                                                      <div class="form-group">
                                                          <label for="cc-exp" class="col-form-label">Expiration Date:</label>
                                                          <input @if (isset($data)) value="{{$data['exp_date']}}"
                                                          @endif id="cc-exp" class="form-control cc-exp" name="exp_date" type="tel" placeholder="MM / YY" autocomplete="cc-exp" >
                                                          @if ($errors->has('exp_date'))
                                                          <span class="help-block">
                                                              <strong>{{ $errors->first('exp_date') }}</strong>
                                                          </span>
                                                          @endif
                                                      </div>
                                                  </div>
                                                  <div class="col-sm">
                                                      <div class="form-group">
                                                          <label for="cc-cvc" class="col-form-label">CVC:</label>
                                                          <input @if (isset($data)) value="{{$data['cvv']}}"
                                                          @endif name="cvv" type="tel" class="cc-cvc form-control" id="cc-cvc" placeholder="XXX">
                                                          @if ($errors->has('cvv'))
                                                          <span class="help-block">
                                                              <strong>{{ $errors->first('cvv') }}</strong>
                                                          </span>
                                                          @endif
                                                      </div>
                                                  </div>

                                                  <div class="col-sm">
                                                      <div class="form-group">
                                                          <label for="zip" class="col-form-label">Zip:</label>
                                                          <input @if (isset($data)) value="{{$data['zip']}}"
                                                          @endif name="zip" type="text" class="form-control" id="zip" placeholder="00000">
                                                          @if ($errors->has('zip'))
                                                          <span class="help-block">
                                                              <strong>{{ $errors->first('zip') }}</strong>
                                                          </span>
                                                          @endif
                                                      </div>
                                                  </div>

                                              </div><!-- close .row -->


                                              <div class="form-group">

                                                  <div class="billing-plan-container">
                                                  </div><!-- close .billing-plan-container -->

                                              </div>

                                          </form>
                                      </div>
                                  </div>


                                  <a style="color:white" class="btn btn-green-pro btn-block">Submit</a>
                                  <div class="clearfix"></div>



                              </div>
                          </div><!-- close .row -->
                      </div><!-- close .row -->

                    </div>
                </div>

            </div><!-- close .modal-body -->


        </div><!-- close .modal-content -->
    </div><!-- close .modal-dialog -->
</div><!-- close .modal -->

@if ($message = Session::get('success'))

  @include('User::frontOffice.modals.userProfileModal')
  <?php Session::forget('success');?>

@endif
@if ($message = Session::get('error'))
  <script type="text/javascript">
  $('#visaToClick').removeClass('jumbotron-selected');
  $(this).addClass('jumbotron-selected');
  $('#visa').hide();
  $('#paypal').show();
  $('#paypal-button-container').show();
  $('#jsPayment').hide();
  $('.checkVisa').removeClass('fas fa-check-circle');
  $('#cc-exp').removeClass('cc-exp');
  $('.checkPaypal').addClass('fas fa-check-circle');
  $('#type').val(1);
  </script>
<?php Session::forget('error');?>
  @endif


<div id="jsPayment">
    <script src="{{asset('frontOffice/js/jquery.payment.js')}}"></script>

    <script>
        jQuery(function($) {
            $('[data-numeric]').payment('restrictNumeric');
            $('.cc-number').payment('formatCardNumber');
            $('.cc-exp').payment('formatCardExpiry');
            $('.cc-cvc').payment('formatCardCVC');

        });
    </script>
</div>

<script>
    $('document').ready(function() {

        // $('#paypal-button-container').hide();

        $('#paypalToClick').on('click', function() {
            $('#visaToClick').removeClass('jumbotron-selected');
            $(this).addClass('jumbotron-selected');
            $('#visa').hide();
            $('#paypal').show();
            $('#paypal-button-container').show();
            $('#jsPayment').hide();
            $('.checkVisa').removeClass('fas fa-check-circle');
            $('#cc-exp').removeClass('cc-exp');
            $('.checkPaypal').addClass('fas fa-check-circle');
            $('#type').val(1);
        });

        $('#visaToClick').on('click', function() {
            $.payment.validateCardNumber($('input.cc-num').val());
            $('#jsPayment').show();
            $(this).addClass('jumbotron-selected');
            $('#paypalToClick').removeClass('jumbotron-selected');
            $('#visa').show();
            $('#paypal').hide();
            $('.checkVisa').addClass('fas fa-check-circle');
            $('#cc-exp').addClass('cc-exp');
            $('.checkPaypal').removeClass('fas fa-check-circle');
            $('#type').val(0);
            $('#paypal-button-container').hide();
        });


        $('.cc-number').payment('formatCardNumber');
        $('.cc-exp').payment('formatCardExpiry');
        $('.cc-cvc').payment('formatCardCVC');


    })
</script>
