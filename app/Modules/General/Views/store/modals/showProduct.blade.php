
<style media="screen">
  #error{
      font-size: 9px;
      color: red;
      margin-top: 8px;
      text-align: center;

  }

  input[type=number]{
    width: 100%;
padding: 10px;
margin-top: 12px;
  }
    #Pbuy{
        margin-top:15px !important;
        margin-buttom:15px !important;
    }
</style>
<div class="modal fade" id="showProduct" tabindex="-1" role="dialog" aria-labelledby="startStreamingModal" aria-hidden="true" data-productId="">
    <button type="button" class="close float-close-pro" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header-pro">
                <h6>Product Details</h6>
            </div>
            <div class="modal-body-pro">

                <div class="card">
                    <div class="card-body">

                    <div class="row">
                        <div class="col-md-4">
                            <img id="productImg" alt="">
                            <a class="btn btn-block" href="#" style="margin-top: 20px; background-color : #02536B; color : white"
                              data-toggle="modal" data-target="#buyProduct">Buy</a>

                        </div>

                        <div class="col-md-8">
                            <ul class="list-group">
                                    <li class="list-group-item"><span class="float-left">Product Name :  </span>
                                        <span style="width: 100%" class="float-right" id="Pname"></span>
                                    </li>
                                <li class="list-group-item"><span class="float-left">Product Price : </span>
                                    <span style="width: 100%" class="float-right" id="Pprice"></span>
                                </li>
                                <li class="list-group-item"><span class="float-left">Product Description : </span>
                                    <span style="width: 100%" class="float-right" id="Pdesc"></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    </div>
                </div>

            </div><!-- close .modal-body -->


        </div><!-- close .modal-content -->
    </div><!-- close .modal-dialog -->
</div><!-- close .modal -->
