@extends('frontOffice.layout')


@section('content')

    <style>


        .priceSpan {
            text-align: center;
            width: 50px;
            color: black;
            background: gold;
            right: 30px;
            position: absolute;
        }
        .bookmark-product{
          position: absolute;
          width: 23%;
          top: -11px;
          left: 5px;
        }
    </style>

    <div id="sidebar-bg">
        @include('frontOffice.inc.sidebar')
        <main id="col-main">

            <div id="video-search-header">
                <div id="search-icon-more"></div>
                <input type="text" placeholder="Seek The Healing Items You Need" aria-label="Search">
                <div id="video-search-header-filtering">
                    <form id="video-search-header-filtering-padding">
                        <div class="row">
                            <div class="col-sm extra-padding">
                                <h5>Type:</h5>

                                <div class="row">
                                    <div class="col-sm">
                                        <label class="checkbox-pro-container">Type 1
                                            <input type="checkbox" checked="checked" id="movies-type">
                                            <span class="checkmark-pro"></span>
                                        </label>

                                        <label class="checkbox-pro-container">Type 2
                                            <input type="checkbox" id="tv-type">
                                            <span class="checkmark-pro"></span>
                                        </label>
                                    </div><!-- close .col -->
                                    <div class="col">
                                        <label class="checkbox-pro-container">Type 3
                                            <input type="checkbox" id="movie-type">
                                            <span class="checkmark-pro"></span>
                                        </label>

                                        <label class="checkbox-pro-container">Type 4
                                            <input type="checkbox" id="documentary-type">
                                            <span class="checkmark-pro"></span>
                                        </label>
                                    </div><!-- close .col -->
                                </div><!-- close .row -->

                                <div class="dotted-dividers-pro"></div>
                            </div><!-- close .col -->
                            <div class="col-sm extra-padding">
                                <h5>Genres:</h5>
                                <select class="custom-select">
                                    <option selected>All Genres</option>
                                    <option value="1">Action</option>
                                    <option value="2">Adventure</option>
                                    <option value="3">Drama</option>
                                    <option value="4">Animation</option>
                                    <option value="5">Documentary</option>
                                    <option value="6">Drama</option>
                                    <option value="7">Horror</option>
                                    <option value="8">Thriller</option>
                                    <option value="9">Fantasy</option>
                                    <option value="10">Romance</option>
                                    <option value="11">Sci-Fi</option>
                                    <option value="12">Western</option>
                                </select>
                                <div class="dotted-dividers-pro"></div>
                            </div><!-- close .col -->
                            <div class="col-sm extra-padding">
                                <h5>Country:</h5>
                                <select class="custom-select">
                                    <option selected>All Countries</option>
                                    <option value="1">Argentina</option>
                                    <option value="2">Australia</option>
                                    <option value="3">Bahamas</option>
                                    <option value="4">Belgium</option>
                                    <option value="5">Brazil</option>
                                    <option value="6">Canada</option>
                                    <option value="7">Chile</option>
                                    <option value="8">China</option>
                                    <option value="9">Denmark</option>
                                    <option value="10">Ecuador</option>
                                    <option value="11">France</option>
                                    <option value="12">Germany</option>
                                    <option value="13">Greece</option>
                                    <option value="14">Guatemala</option>
                                    <option value="15">Italy</option>
                                    <option value="16">Japan</option>
                                    <option value="17">asdfasdf</option>
                                    <option value="18">Korea</option>
                                    <option value="19">Malaysia</option>
                                    <option value="20">Monaco</option>
                                    <option value="21">Morocco</option>
                                    <option value="22">New Zealand</option>
                                    <option value="23">Panama</option>
                                    <option value="24">Portugal</option>
                                    <option value="25">Russia</option>
                                    <option value="26">United Kingdom</option>
                                    <option value="27">United States</option>
                                </select>
                                <div class="dotted-dividers-pro"></div>
                            </div><!-- close .col -->
                        </div><!-- close .row -->
                        <div id="video-search-header-buttons">
                            <a href="#!" class="btn btn-green-pro">Filter Search</a>
                            <a href="#!" class="btn">Reset</a>
                        </div><!-- close #video-search-header-buttons -->
                    </form><!-- #video-search-header-filtering-padding -->
                </div><!-- close #video-search-header-filtering -->
            </div><!-- close .video-search-header -->

            <div class="clearfix"></div>

            <div class="dashboard-container">

                <h4  class="heading-extra-margin-bottom">Kwiknin Market Store</h4>
                <div style="border-bottom: inset; margin-bottom: 25px" class="row">

               @foreach($products as $product)
                    <div class="col-12 col-md-6 col-lg-4 col-xl-4">
                        <div class="item-playlist-container-skrn">
                          <a href="#" class="bookmark-product" data-product="{{$product->id}}"><img

        											@if (hasBookmark($product->id,'App\Modules\User\Models\Product'))
        												src="{{asset('images/bookmark.png')}}" alt="bookmark"

        												@else
        													src="{{asset('images/bookmark_add.png')}}" alt="bookmark_add"

        											@endif

        										> </a>
                            <a class="showProduct" href="#" data-toggle="modal" data-target="#showProduct" data-id="{{$product->id}}" data-quantity="{{$product->quantity}}" data-img="{{$product->image}}" data-price="{{$product->price}}" data-name="{{$product->name}}" data-desc="{{$product->description}}">
                              <img src="{{asset($product->image)}}" alt="Listing">
                            </a>
                            <span class="priceSpan">{{$product->price}} $</span>
                            <div class="item-playlist-text-skrn">
                                <img src="{{asset($product->store->user->image ? $product->store->user->image : 'frontOffice/images/unknown.png')}}" alt="User Profile">
                                <h5>{{$product->name}}</h5>
                                <h6><a href="{{route('showPublicProfile',$product->store->user->slug)}}">{{$product->store->name}}</a></h6>
                            </div><!-- close .item-listing-text-skrn -->
                        </div><!-- close .item-playlist-container-skrn -->
                    </div><!-- close .col -->
                @endforeach
                   <div style="text-align: center" class="col-md-12">
                       {{$products->links()}}
                   </div>



                </div><!-- close .row -->



            </div><!-- close .dashboard-container -->
        </main>
    </div>

    @include('General::store.modals.showProduct')
    @include('General::store.modals.buyProduct')

    <script>
          $(document).on("click", ".showProduct", function (event) {
  					event.preventDefault();
  					  var productId = $(this).data('id');
              var img = $(this).data('img');
              var name = $(this).data('name');
              var price = $(this).data('price');
              var desc = $(this).data('desc');
              $("#productImg").attr("src",img);
              $("#Pdesc").text(desc);
              $("#Pname").text(name);
              $("#Pprice").text(price +' $');

  						$('#showProduct').data('productId',productId);

  						var base_url = '{!! url('/') !!}';
  						var href = base_url+'/user/profile/paypal/'+productId;
  						$('#buyForm').attr('action',href);
              $('#error').html(' ');
  						if ($(this).data('quantity') > 0) {
  							$("input[type='number']").attr('max', $(this).data('quantity'));


  							$('#error').html('Quantity available: '+$(this).data('quantity'));


  						}else {
  							$("input[type='number']").attr('max', 1);
  							$("input[type='number']").attr('disabled', true);
  							$('#error').html('NOT AVAILABLE');
  						}

          });
  	</script>
    <script type="text/javascript">
    $('.bookmark-product').on('click',function(event) {

    				event.preventDefault();
    					var bookmarkId;
    					var data;
    					bookmarkId = $(this).data('product');

    					data = {
    					'bookmarkId' : bookmarkId,
    					'bookmark' : 'product'
    					}

    					var img = $(this).find('img');


    				$.post('{{route('apiHandleBookmark')}}',
    				{
    				'_token': $('meta[name=csrf-token]').attr('content'), data

    				})
    				.done(function (res) {

    					if (res.event) {
    							img[0].src = "{{asset('images/bookmark.png')}}";
    							img[0].alt = "bookmark";
    					}else {
    						img[0].src = "{{asset('images/bookmark_add.png')}}";
    						img[0].alt = "bookmark_add";
    					}

    				})
    });
    </script>
@endsection
