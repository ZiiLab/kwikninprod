@extends('frontOffice.layout')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('frontOffice/slick/slick.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontOffice/slick/slick-theme.css')}}">
<link href='https://fonts.googleapis.com/css?family=Raleway:400,800,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="{{asset('frontOffice/HoverEffect/demo.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('frontOffice/HoverEffect/set1.css')}}" />
@endsection

@section('content')


<style media="screen">
    .blog-post.single {
    padding: 0 0 5px;
}
.post-media {
    position: relative;
}
.bd-f-img {
    margin-bottom: 25px;
}
img {
    vertical-align: middle;
    border-style: none;
}
.blog-post.single a {
    color: #313131;
}
p {
    font-size: 15px;
    line-height: 24px;
    font-weight: 400;
    margin-bottom: 0px;
}
.blog-post.single .post-meta .post-title a{
    font-size: 24px;
    line-height: 30px;
    margin-bottom: 10px;
    display: block;
    margin-top: 0px;
}
.blog-post.single  a:hover{
    color: #18536a;
}
.blog-post.single .post-content {
    padding-top: 10px;
}
.blockquote-wrapper {
    border: none;
    padding: 30px 40px;
    border: 1px solid #dedede;
    margin: 30px 0px 30px;
}
.blockquote-wrapper i{
    font-size: 30px;
    color: #18536a;
}
.blockquote-wrapper p {
    margin-bottom: 10px;
}

.blockquote-wrapper .author {
    font-size: 18px;
    font-weight: 600;
}

#imgRdv {
          box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.8), 0 2px 10px 0 rgba(0, 0, 0, 0.8);
          -webkit-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.8), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
          -moz-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
          background-image: url("{{asset($event->medias->first()->link)}}");
          width: auto;
          height: 250px;
          background-position: center;
          background-repeat: no-repeat;
          background-size: cover;
          margin-top: 30px;
}

.event-annoce-title{
  color: #18536a;
}
.event-annoce-title:hover{
  text-decoration: none;
  color: #18536a;
}

	</style>
<div id="sidebar-bg">
    @include('frontOffice.inc.sidebar')
    <main id="col-main">
        <div class="dashboard-container">
            <div class="row">
                <div class="col-md-8">



                    <article class="blog-post single">

                        <div class="post-media" id="imgRdv">

                        </div>


                        <div class="card my-4 bg-light">
                            <h1 class="card-header event-annoce-title" style="font-size: larger; margin:0px">{{$event->title}}</h1>
                            <div class="card-body">

                                <div class="post-content">
                                    <p class="po-c-first-p">{{$event->long_description}}</p>
                                    <p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>

                            </div>
                        </div>
                        </article>
                </div>

                <div class="col-md-4">
                    <div class="card my-4 bg-light">
                        <div class="card-body">

                            <h5>Start Date : {{ $event->start_date->format('d') }} {{ $event->start_date->format('M') }}, {{ $event->start_date->format('Y') }}</h5>
                            <h5>End Date     : {{ $event->end_date->format('d') }} {{ $event->end_date->format('M') }}, {{ $event->end_date->format('Y') }}</h5>

                        </div>

                          @if (Auth::check())
                                @if (Auth::user()->hasEventBooking($event->id))

                                    <td><a href="#" class="btn" id="participate" data-event="{{$event->id}}">Cancel ! </a></td>
                                  @else
                                      <td><a href="#" class="btn" id="participate" data-event="{{$event->id}}">Participate !</a></td>

                                @endif

                            @else
                              <td><a href="#" class="btn">Participate !</a></td>

                          @endif

                    </div>
                </div>
            </div>

        </div>
</div>

</div>
</div>
</main>
</div>
@endsection

@section('js')
<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>

<script src="{{asset('frontOffice/slick/slick.js')}}" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    $(document).on('ready', function() {
        $(".regular").slick({
            dots: true,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3
        });

    });
</script>

<script type="text/javascript">
$('#participate').on('click',function(event) {

				event.preventDefault();
					var eventId;
					var data;
					eventId = $(this).data('event');

					data = {
					'eventId' : eventId
					}



				$.post('{{route('apiBookingEvent')}}',
				{
				'_token': $('meta[name=csrf-token]').attr('content'), data

				})
				.done(function (res) {
              if (res.booking) {
                  $('#participate').html('Cancel !');
              }else {
                  $('#participate').html('Participate !');
              }
				})
});
</script>

@endsection
