@extends('frontOffice.layout')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('frontOffice/slick/slick.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontOffice/slick/slick-theme.css')}}">
<link href='https://fonts.googleapis.com/css?family=Raleway:400,800,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="{{asset('frontOffice/HoverEffect/demo.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('frontOffice/HoverEffect/set1.css')}}" />
@endsection

@section('content')


<style media="screen">
	.slider {
			width: 100%;
			margin: 10px auto;
	}

	.slick-slide {
		margin: 0px 30px;
	}

	.slick-slide img {
		width: 100%;
		height: 100%;
	}

	.slick-prev:before,
	.slick-next:before {
		color: black;
	}


	.slick-slide {
		transition: all ease-in-out .3s;
		opacity: .2;
	}

	.slick-active {
		opacity: .5;
	}

	.slick-current {
		opacity: 1;
	}

	.bookmark-event{
		position: absolute;
		width: 23%;
		top: -11px;
		left: 5px;
	}
	</style>
<div id="sidebar-bg">
	@include('frontOffice.inc.sidebar')
	<main id="col-main">
<div class="dashboard-container">

	<div class="row">
		<div class="col-md-12" style="margin-top: 20px;
    margin-bottom: -25px;
    text-align: center;">
			<h4  class="heading-extra-margin-bottom">Hot Events</h4>

		</div>
	</div>
		<section class="regular slider">


			@foreach ($hotEvents as $event)
				<div>
					<div class="grid">
						<figure class="effect-sadie">
							<img src="{{asset($event->medias->first()->link)}}" alt="{{$event->title}}"style="/*! height:100% */height: 284px;"/>
							<figcaption>
								<h2><span>{{$event->title}}</span></h2>
								<p>{{$event->short_description}}</p>
								<a href="{{route('showEventDetail',$event->id)}}">View more</a>
							</figcaption>
						</figure>
					</div>
				</div>
			@endforeach




		</section>

	<div class="row">
		<div class="col-md-12" style="
    text-align: center;">
			<h4  class="heading-extra-margin-bottom">All Events</h4>

		</div>
	</div>

		<div class="row">

			<div class="grid" style="padding: 1em 0 1em !important;">
				@foreach ($events as $event)


					<figure class="effect-ruby">
						<img  src="{{asset($event->medias->first()->link)}}" alt="{{$event->title}}" style="height: 360px;"/>
						<figcaption>
							<h2><span>{{$event->title}}</span></h2>
							<p>{{$event->short_description}}</p>
							<a href="{{route('showEventDetail',$event->id)}}">View more</a>
						</figcaption>
					</figure>
				@endforeach


		</div>


		</div>



			{{$events->links()}}


</div>

	</main>
</div>
@endsection

@section('js')
<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>

<script src="{{asset('frontOffice/slick/slick.js')}}" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
	$(document).on('ready', function() {
		$(".regular").slick({
			dots: true,
			infinite: true,
			slidesToShow: 3,
			slidesToScroll: 3
		});

	});
</script>

<script type="text/javascript">
$('.bookmark-event').on('click',function(event) {

				event.preventDefault();
					var bookmarkId;
					var data;
					bookmarkId = $(this).data('event');

					data = {
					'bookmarkId' : bookmarkId,
					'bookmark' : 'event'
					}

					var img = $(this).find('img');


				$.post('{{route('apiHandleBookmark')}}',
				{
				'_token': $('meta[name=csrf-token]').attr('content'), data

				})
				.done(function (res) {
						if (res.event) {
								img[0].src = "{{asset('images/bookmark.png')}}";
								img[0].alt = "bookmark";
						}else {
							img[0].src = "{{asset('images/bookmark_add.png')}}";
							img[0].alt = "bookmark_add";
						}
				})
});
</script>
@endsection
