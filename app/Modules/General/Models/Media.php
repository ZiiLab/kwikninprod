<?php

namespace App\Modules\General\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model {

  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   */
  public $timestamps = true;

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'medias';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'link',
    'event_id'
  ];


      public function event()
  {
      return $this->belongsTo('App\Modules\User\General\Event');
  }

}
