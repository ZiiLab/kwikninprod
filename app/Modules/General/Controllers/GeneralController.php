<?php

namespace App\Modules\General\Controllers;

use App\Modules\Broadcasting\Models\Stream;
use App\Modules\User\Models\Product;
use App\Modules\User\Models\Store;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

use App\Modules\User\Models\Event;

use App\Modules\User\Models\User;
use Twilio\Rest\Client as Client;
use Twilio\Jwt\AccessToken as AccessToken;
use Twilio\Jwt\Grants\VideoGrant;



class GeneralController extends Controller
{
    protected $sid;
    protected $token;
    protected $key;
    protected $secret;

    public function __construct()
    {
        $this->sid = config('services.twilio.sid');
        $this->token = config('services.twilio.token');
        $this->key = config('services.twilio.key');
        $this->secret = config('services.twilio.secret');
    }



    public function showHome()
    {
        return view("General::home", [

            'products' => Product::take(6)->get(),
            'sessions' => Stream::where('start_date', '>', Carbon::now())->get()
        ]);
    }



    public function showEvents()
    {
      return view('General::events.index',[
        'hotEvents' => Event::take(6)->get(),
        'events' => Event::paginate(6)
      ]);
    }


    public function showEventDetail($id)
    {
      $event =  Event::find($id);
      if (!$event) {
        return redirect()->route('showHome');
      }

      return view('General::events.details',[
        'event'=>$event
      ]);
    }

    public function showStore() {

        return view('General::store.index',[
            'products' => Product::paginate(9)
        ]);
    }

    public function apiHandleCheckQuantity(Request $request)
    {

      $data =$request->data;

      return response()->json($request);
      if (!isset($request->data['quantity'])) {
            return response()->json(['status'=>101]);
      }
        $product = Product::find($request->data['productId']);

        if ($request->data['quantity'] >$product->quantity ) {
            return response()->json(['status'=>102]);
        }
        return response()->json(['status'=>200]);
    }

  


}
