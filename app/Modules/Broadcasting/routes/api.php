<?php

Route::group(['module' => 'Broadcasting', 'middleware' => ['api'], 'namespace' => 'App\Modules\Broadcasting\Controllers'], function() {

    Route::resource('Broadcasting', 'BroadcastingController');

});
