<?php

Route::group(['module' => 'Broadcasting', 'middleware' => ['web','user'], 'namespace' => 'App\Modules\Broadcasting\Controllers'], function() {
     Route::get('/stream/{streamTitle}/{id}', 'BroadcastingController@showStreamerPage')->name('showStreamerPage');
     Route::get('/messages', 'BroadcastingController@apiGetMessages')->name('apiGetMessages');
     Route::get('/message', 'BroadcastingController@apiHandlePostMessage')->name('apiHandlePostMessage');
     Route::get('/sessions/bookings', 'BroadcastingController@apiGetParticipiants')->name('apiGetParticipiants');

     Route::post('/start', 'BroadcastingController@handleStartStreaming')->name('handleStartStreaming');
     Route::get('/start', 'BroadcastingController@handleStartSession')->name('handleStartSession');


});
