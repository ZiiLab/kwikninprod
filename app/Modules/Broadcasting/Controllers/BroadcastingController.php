<?php

namespace App\Modules\Broadcasting\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Broadcasting\Models\Message;
use App\Modules\Broadcasting\Models\Stream;
use App\Modules\User\Models\User;
use Auth;
use App\Events\MeesagePosted;
use Twilio\Rest\Client as Client;
use Twilio\Jwt\AccessToken as AccessToken;
use Twilio\Jwt\Grants\VideoGrant;
use Illuminate\Support\Facades\Log;


class BroadcastingController extends Controller
{
  /**
   * @desc show welcoming page
   * @return view
   */

    protected $sid;
    protected $token;
    protected $key;
    protected $secret;

    public function __construct()
    {
        $this->sid = config('services.twilio.sid');
        $this->token = config('services.twilio.token');
        $this->key = config('services.twilio.key');
        $this->secret = config('services.twilio.secret');
    }

/*  public function showStreamerPage($streamId){

      $stream = Stream::find($streamId);

      return view('Broadcasting::frontOffice.streamerPage',[
      'stream' =>  $stream

      ]);

  }*/


  public function apiGetMessages(Request $request){
    $stream =  Stream::find($request['sId']);
  	return $stream->messages;
  }

  public function apiHandlePostMessage(Request $request){

    $body = $request['body'];
    $streamId = $request['sId'];

    $message = Message::create([
      'body' =>$body,
      'user_id' => Auth::id(),
      'stream_id' => $streamId
    ]);

    event(new MeesagePosted($message,Auth::user()));

    return ['status' => 200];

  }

  public function handleStartStreaming(Request $request){

    if(!$request->data['title']){
       return ['status' => 101];
    }

      if(!$request->data['price']){
          return ['status' => 104];
      }

      if(!$request->data['start_date']){
          return ['status' => 102];
      }

    if($request->data['catId'] ==0){
       return ['status' => 103];
    }

    $date = explode(' ',$request->data['start_date'])[0];
    $time = explode(' ',$request->data['start_date'])[1];

    $start_date = $date.' '.$time;




    $stream = Stream::create([
      'title'  => $request->data['title'],
      'type' => null,
      'price' => $request->data['price'],
      'start_date' => Carbon::parse($start_date),
      'description' => $request->data['description'],
      'streamer_id' => Auth::user()->streamer->id,
      'category_id' => $request->data['catId'],

    ]);

      return ['route' => route('showSessions')];



          }

          public function handleStartSession(Request $request) {
              $client = new Client($this->sid, $this->token);

              $exists = $client->video->rooms->read([ 'uniqueName' => $request->get('roomName')]);

              if (empty($exists)) {
                  $client->video->rooms->create([
                      'uniqueName' => $request->roomName,
                      'type' => 'group',
                      'recordParticipantsOnConnect' => false
                  ]);

                  Log::debug("created new room: ".$request->get('roomName'));
              }

              return redirect()->action('\App\Modules\Broadcasting\Controllers\BroadcastingController@showStreamerPage', [
                  'roomName' => $request->get('roomName'),
                  'sId' => $request->get('sId')
              ]);
          }


            public function showStreamerPage($roomName, $id)
            {

                $stream =Stream::find($id);

                $identity = Auth::user()->slug;

                Log::debug("joined with identity: $identity");

                $token = new AccessToken($this->sid, $this->key, $this->secret, 3600, $identity);

                $videoGrant = new VideoGrant();
                $videoGrant->setRoom($roomName);

                $token->addGrant($videoGrant);

                return view('Broadcasting::frontOffice.streamerPage', [
                    'accessToken' => $token->toJWT(),
                    'roomName' => $roomName,
                    'stream' => $stream
                ]);
            }



            public function apiGetParticipiants(Request $request) {
              $session = Stream::find($request->get('id'));

              $participation = [];

              foreach ($session->bookings as $booking) {
                  $user = User::find($booking->user_id);
                  $data = [
                      'created_at' => Carbon::parse($session->created_at)->diffForHumans(),
                      'user' => $user
                      ];

                  $participation[] = $data;
                  }

                  if(count($participation) > 0) {

                     return ['bookings' => $participation];
                  }else {
                      return ['status' => 400];
                  }
            }
}
