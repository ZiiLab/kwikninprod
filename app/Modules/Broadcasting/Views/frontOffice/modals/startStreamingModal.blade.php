
<div class="modal fade" id="startStreamingModal" tabindex="-1" role="dialog" aria-labelledby="startStreamingModal" aria-hidden="true">
    <button type="button" class="close float-close-pro" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header-pro">
                <h2>Howdy Healer</h2>
                <h6>Fill these informations To Create A Session !</h6>
            </div>
            <div class="modal-body-pro">

        <div class="form-group">
<label for="study_field" class="col-form-label"> Session Title :</label>
<input data-placement="right" data-toggle="tooltip" type="text" class="form-control title" name="title" id="title"  placeholder="Title here ...">
</div>
<div class="form-group">
    <label for="study_field" class="col-form-label"> Starting Date :</label>

</div>
                <div class="form-group">
                    <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                        <input name="start_date" type="text" class="form-control datetimepicker-input" data-target="#datetimepicker1"/>
                        <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="price" class="col-form-label">Price :</label>
                    <input onkeydown="javascript: return event.keyCode == 69 ? false : true" type="number" class="form-control price" name="price" id="price"  placeholder="Price here ...">


                </div>

                 <div class="form-group">
                                        <label for="first-name" class="col-form-label">Description :</label>
                                        <textarea id="description" placeholder="Description here ..." class="form-control" name="description" rows="4" cols="78 "></textarea>


                                    </div>

                <div class="form-group cat">
                                  <label for="first-name" class="col-form-label">Session Category :</label>
                                          <ul class="registration-genres-choice">

                                            @foreach ($categories as $categorie)
                                              <li data-category = {{$categorie->id}}>
                                                  <i class="fas fa-check-circle"></i>
                                                  <img src="https://via.placeholder.com/200x300" alt="Body">
                                                  <h6>{{$categorie->label}}</h6>
                                              </li>
                                            @endforeach



                                        </ul>


                                <div class="clearfix"></div>
                            </div><!-- close .registration-genres-step -->


<a href="#" id="start" class="not-a-member-pro"> <span>Submit !</span></a>





            </div><!-- close .modal-body -->


        </div><!-- close .modal-content -->
    </div><!-- close .modal-dialog -->
</div><!-- close .modal -->
<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker();
    });
</script>

<script>
    (function ($) {
        $.fn.buttonLoader = function (action) {
            var self = $(this);
            //start loading animation
            if (action == 'start') {
                if ($(self).attr("disabled") == "disabled") {
                    e.preventDefault();
                }
                //disable buttons when loading state
                $('.has-spinner').attr("disabled", "disabled");
                $(self).attr('data-btn-text', $(self).text());
                //binding spinner element to button and changing button text
                $(self).html('<span class="spinner"><i class="fa fa-spinner fa-spin"></i></span>Loading');
                $(self).addClass('active');
            }
            //stop loading animation
            if (action == 'stop') {
                $(self).html($(self).attr('data-btn-text'));
                $(self).removeClass('active');
                //enable buttons after finish loading
                $('.has-spinner').removeAttr("disabled");
            }
        }
    })(jQuery);
</script>
    <script>

        var catId = 0;

        $('.registration-genres-choice li').on('click', function () {
            $(".registration-genres-choice li").each(function() {
                if($(this).hasClass('active')) {
                    $(this).removeClass('active');
                }

            });
             catId = $(this).data('category');

        });


    function startStreaming() {
        var btn = $('#start');
        var li;



        var data = {
            'title' : $('#title').val(),
            'type' : null,
            'price' :  $('#price').val(),
            'start_date' :$('.datetimepicker-input').val() ,
            'description' : $('#description').val(),
            'catId' : catId
        }
        $(".title").removeAttr("data-original-title");
        $(".datetimepicker-input").removeAttr("data-original-title");
        $(".price").removeAttr("data-original-title");
        $(btn).buttonLoader('start');

            $.post('{{route('handleStartStreaming')}}',
            {
            '_token': $('meta[name=csrf-token]').attr('content'), data : data

            })
            .done(function (res) {

                    btn.buttonLoader('stop');
                      console.log(res);
                    if(res['status']){
                        if(res['status'] == 101) {
                          $(".title").attr('data-original-title', "Title is required !").tooltip('show');

                        }

                        if(res['status'] == 104) {
                            $(".price").attr('data-original-title', "Price is required !").tooltip('show');

                        }
                        if(res['status'] == 102) {
                          $(".datetimepicker-input").attr('data-original-title', "Pick A Date !").tooltip('show');

                        }
                        if(res['status'] == 103) {
                          $(".cat").css({"border": "1px solid", "border-color": "red"});

                        }
                    }
                    else {
                    window.location = res['route'];

                    }
            })
      }
    function startFromEnter(event) {

      if ($('#startStreamingModal').hasClass('show')) {
        if (event.keyCode === 13) {
            startStreaming();
          }
      }
    }

    $('#start').on('click', startStreaming);

    $(document).on('keyup',startFromEnter);
</script>
