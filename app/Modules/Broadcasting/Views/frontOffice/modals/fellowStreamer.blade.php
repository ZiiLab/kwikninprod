<div class="modal fade" id="fellowModal" tabindex="-1" role="dialog" aria-labelledby="fellowModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header-pro">
                <h2>This Stream is private for members only</h2>
                <div class="alert alert-primary  fade show" role="alert" style="text-align:center;">
                  <strong>Please Fellow This streamer to watch his stream !</strong>.

                  <strong><a href="{{route('showPublicProfile',$stream->streamer->user->slug)}}">Check His Profile</a></strong>
                  <button type="button" class="close" aria-label="Close">
                    <span aria-hidden="true"><i class="fa fa-paper-plane " aria-hidden="true" style="font-size: 17px;"></i></span>
                  </button>
                </div>
            </div>

            <div class="container">
                <div class="registration-step-final-footer">
                      <a href="{{route('showStreamerPage',$stream->id)}}" id="follow" data-user = {{$stream->streamer->id}} class="btn btn-block btn-green-pro">Fellow</a>
                </div>
            </div>

        </div><!-- close .modal-content -->
    </div><!-- close .modal-dialog -->
</div><!-- close .modal -->
<script type="text/javascript">
$(function() {
    $('#follow').on('click',function() {

      var streamerId =  $(this).data('user');

      var data = {

        'streamerId' : streamerId

      }


      $.post('{{route('apiHandleFollow')}}',
        {
            '_token': $('meta[name=csrf-token]').attr('content'), data

        })
        .done(function (res) {

            if (res.status) {
              $('#follow').html('<img src="{{asset('images/unlike.png')}}" alt="dislike"> Unfolow');
            }else{
              $('#follow').html('<img src="{{asset('images/like.png')}}" alt="like"> Follow');
            }

            $('#followers').html('<span>'+res.followers+'</span> Following')

      })

    });
});
</script>
