@extends('frontOffice.layout')


@section('css')

  <link rel="stylesheet" href="{{asset('frontOffice/chatWidget/style.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

@endsection
@section('content')
    <script src="{{asset('js/skd.js')}}"></script>
    <script>

        var webCamObj = {};

        function detectWebcam(callback) {
            let md = navigator.mediaDevices;
            if (!md || !md.enumerateDevices) return callback(false);
            md.enumerateDevices().then(devices => {
                callback(devices.some(device => 'videoinput' === device.kind));
        })
        }

        detectWebcam(function(hasWebcam) {
            console.log('Webcam: ' + (hasWebcam ? 'yes' : 'no'));

            if(hasWebcam) {
                webCamObj = {
                    audio: true,
                    video: {width: 400}
                }
            }else {
                webCamObj = {
                    audio: false,
                    video: false
                }
            }
        })

        Twilio.Video.createLocalTracks(webCamObj).then(function(localTracks) {
            return Twilio.Video.connect('{{ $accessToken }}', {
                name: '{{ $roomName }}',
                tracks: localTracks,
                video: { width: 400 }
            });
        }).then(function(room) {

            room.participants.forEach(participantConnected);

            var previewContainer = document.getElementById(room.localParticipant.sid);
            if (!previewContainer || !previewContainer.querySelector('video')) {
                participantConnected(room.localParticipant);
            }

            room.on('participantConnected', function(participant) {
                participantConnected(participant);
            });

            room.on('participantDisconnected', function(participant) {
                participantDisconnected(participant);
            });
        });
        // additional functions will be added after this point

        function participantConnected(participant) {
            const slug = '{{ $stream->streamer->user->slug }}'
            if(participant.identity === slug) {
            const div = document.createElement('div');
            div.id = participant.sid;
            div.setAttribute("style", "float: left; margin: 10px;");

            participant.tracks.forEach(function(track) {
                trackAdded(div, track)
            });

            participant.on('trackAdded', function(track) {
                trackAdded(div, track)
            });
            participant.on('trackRemoved', trackRemoved);

            document.getElementById('media-div').appendChild(div);
        }
          }

        function participantDisconnected(participant) {
            console.log('Participant "%s" disconnected', participant.identity);

            participant.tracks.forEach(trackRemoved);
            document.getElementById(participant.sid).remove();
        }

        function trackAdded(div, track) {
            div.appendChild(track.attach());
            var video = div.getElementsByTagName("video")[0];
            if (video) {
                video.setAttribute("style", "width:100%;");
                video.controls = true;
            }
        }

        function trackRemoved(track) {
            track.detach().forEach( function(element) { element.remove() });
        }
    </script>
<div id="sidebar-bg">
    @include('frontOffice.inc.sidebar')


    <div id="app">
    <main id="col-main-with-sidebar-streamer">

        <div id="movie-detail-header-pro">
           {{-- @if(Auth::id() == $stream->streamer->user->id)
                --}}{{-- <video id="video" width="100%" autoplay="true"></video> --}}{{--
                <div width="100%" style="background:red;">

                </div>
            @else

                <div  class="progression-studios-slider-more-options">
                    <i class="fas fa-ellipsis-h"></i>
                    <ul>
                        <li><a href="#!" id="library">Add to Library</a></li>
                    </ul>
                </div>

                <div style="    float: left;
                cursor: pointer;
                position: fixed;
                z-index: 99;
                margin: 0px;
                padding: 22px 22px 0px 12px;" class="">
                <a  href="" class="btn ">Donate</a>
                </div>
                <img style="margin-top: -200px" width="100%" id='frame'>

            @endif--}}

            <div class="content">


                <div id="media-div">
                </div>
            </div>




            {{--<a class="movie-detail-header-play-btn afterglow" href="#!"><i class="fas fa-play"></i></a>--}}


            {{--<div id="movie-detail-gradient-pro"></div>--}}
        </div><!-- close #movie-detail-header-pro -->


        <div id="movie-detail-rating">
            <div class="dashboard-container">
                <div class="row">
                    <div class="col-sm-10">


                    </div>
                    <div class="col-sm-2">
                        <h5>Viewers : @{{ usersInRoom.length }}</h5>
                    </div>

                </div><!-- close .row -->
            </div><!-- close .dashboard-container -->
        </div><!-- close #movie-detail-rating -->

        <div class="dashboard-container">


            <div class="movie-details-section">
                <h2>Description</h2>
                <p>{{$stream->description}}</p>
            </div><!-- close .movie-details-section -->





        </div><!-- close .dashboard-container -->
    </main><!--
--><div id="content-sidebar-pro-streamer">

      <div id="content-sidebar-image">

          <div class="row chat-window col-xs-12 col-md-12" id="chat_window_1" style="padding:0;margin:0">
              <div class="col-xs-12 col-md-12" style="padding:0">
                  <div style="height: 494px" class="card">
                      <div class="card-heading top-bar">
                          <div class="col-md-8 col-xs-8" style="margin-left:42px;">
                              <p class="card-title"><span class="glyphicon glyphicon-comment"></span> Chat </p>
                          </div>
                          <div class="col-md-4 col-xs-4" style="text-align: right;">
                              <a href="#"><span id="minim_chat_window" class="glyphicon glyphicon-minus icon_minim"></span></a>
                              <a href="#"><span class="glyphicon glyphicon-remove icon_close" data-id="chat_window_1"></span></a>
                          </div>
                      </div>
                      <div class="card-body msg_container_base">

                          <chat-log :messages="messages"></chat-log>
                      </div>
                      <chat-composer v-on:messagesent="addMessage"></chat-composer>
                  </div>
              </div>
          </div>
      </div>

</div><!-- close #content-sidebar-pro -->
</div>
</div>
<script src="{{asset('js/app.js')}}" charset="utf-8"></script>



    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" charset="utf-8"></script>




    @endsection
