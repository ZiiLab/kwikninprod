<?php

namespace App\Modules\Broadcasting\Models;

use Illuminate\Database\Eloquent\Model;

class SessionBooking extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'session_bookings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'stream_id'
    ];

    public function session() {
        return $this->hasOne('App\Modules\Broadcasting\Models\Stream','id', 'stream_id');
    }

    public function user() {
        return $this->belongsTo('App\Modules\User\Models\USer','id', 'user_id');
    }

}
