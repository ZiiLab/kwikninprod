<?php

namespace App\Modules\Broadcasting\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'subscriptions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'streamer_id',
        'user_id'
    ];

    public function streamer()
    {
        return $this->hasOne('App\Modules\Broadcasting\Models\Streamer','id','streamer_id');
    }

    public function follower()
    {
        return $this->hasOne('App\Modules\User\Models\User','id','user_id');
    }
}
