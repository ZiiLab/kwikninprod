<?php

namespace App\Modules\Broadcasting\Models;

use Illuminate\Database\Eloquent\Model;

class StreamerCategory extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'streamers_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'streamer_id',
        'category_id'
    ];


    public function streamer()
    {
        return $this->hasOne('App\Modules\Broadcasting\Models\Streamer');

    }

    public function category()
    {
        return $this->hasOne('App\Modules\Broadcasting\Models\Category');

    }


}
