<?php

namespace App\Modules\Broadcasting\Models;

use Illuminate\Database\Eloquent\Model;

class Stream extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'streams';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'type',
        'description',
        'streamer_id',
        'category_id',
        'start_date',
        'p_nbr',
        'price'
    ];

    protected $dates = [
        'start_date',
    ];

    public function streamer()
    {
        return $this->hasOne('App\Modules\Broadcasting\Models\Streamer','id','streamer_id');
    }

    public function category()
    {
        return $this->hasOne('App\Modules\Broadcasting\Models\Category','id','category_id');
    }

      public function messages()
    {
        return $this->hasMany('App\Modules\Broadcasting\Models\Message');
    }

    public function bookings() {
        return $this->hasMany('App\Modules\Broadcasting\Models\SessionBooking');
    }
}
