<?php

namespace App\Modules\Broadcasting\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Broadcasting\Models\Subscription;

class Streamer extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'streamers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'user_id'
    ];

    public function user()
    {
        return $this->hasOne('App\Modules\User\Models\User','id','user_id');
    }

      public function streams()
    {
        return $this->hasMany('App\Modules\Broadcasting\Models\Stream');
    }

       public function subscriptions()
    {
        return $this->hasMany('App\Modules\Broadcasting\Models\Subscription');
    }

        public function categories()
    {
        return $this->hasMany('App\Modules\Broadcasting\Models\StreamerCategory');
    }

        public function followers()
    {

         $followers = [];

         foreach($this->subscriptions as $subscription) {

            $followers[] = $subscription->follower;
         }

         return $followers;
    }

    public function subs() {

        return Subscription::where('user_id', $this->id)->get();
    }

}
