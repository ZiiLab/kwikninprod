<?php

namespace App\Modules\Broadcasting\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'messages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'body',
        'stream_id',
        'user_id'
    ];


    public function stream()
    {
        return $this->belongsTo('App\Modules\Broadcasting\Models\Stream');
    }

     public function user()
    {
        return $this->belongsTo('App\Modules\User\Models\User');
    }
}
