<?php

Route::group(['module' => 'Review', 'middleware' => ['web'], 'namespace' => 'App\Modules\Review\Controllers'], function() {

    Route::resource('Review', 'ReviewController');

});
