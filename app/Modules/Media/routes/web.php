<?php

Route::group(['module' => 'Media', 'middleware' => ['web'], 'namespace' => 'App\Modules\Media\Controllers'], function() {

    Route::resource('Media', 'MediaController');

});
