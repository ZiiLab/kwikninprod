<?php

Route::group(['module' => 'Media', 'middleware' => ['api'], 'namespace' => 'App\Modules\Media\Controllers'], function() {

    Route::resource('Media', 'MediaController');

});
