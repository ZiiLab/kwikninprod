@extends('frontOffice.layout')

@section('content')
  <style media="screen">
    #follow img{
      width: 25px;
    }

    .btn {
      color: white;
      background: #02536B;
    }
  </style>
    <div id="sidebar-bg">

        @include('frontOffice.inc.sidebar')

        <div id="content-sidebar-pro">

            <div id="content-sidebar-info">

                <img src=" @if (!is_null($user->image)) {{asset($user->image)}} @else {{asset('frontOffice/images/unknown.png')}} @endif"  alt="{{$user->first_name}}">

                    @if(Auth::id() == $user->id)
                    @else
                  <a style="position: absolute;right: 0;width: 20%;" href="#" class="bookmark-user" data-user="{{$user->id}}"><img

                      @if (hasBookmark($user->id,'App\Modules\User\Models\User'))
                        src="{{asset('images/bookmark.png')}}" alt="bookmark"

                        @else
                          src="{{asset('images/bookmark_add.png')}}" alt="bookmark_add"

                      @endif

                    ></a>
                  @endif

                <div id="profile-sidebar-name">
                    <h5>{{$user->first_name}} {{$user->last_name}}</h5>
                </div>
                <div id="profile-sidebar-gradient"></div>

                @current($user->id)
                   <a href="{{route('showProfile')}}" class="edit-profile-sidebar"><i class="fas fa-pencil-alt"></i></a>
                    @endcurrent

            </div>





            <div class="content-sidebar-section">
                <ul id="profile-watched-stats">
                    <li id="followers"><span>{{count($user->streamer->streams)}}</span> Sessions</li>
                    <li><span>{{count($user->store->products())}}</span> Products</li>
                    <li><span>{{count($user->events)}}</span> Events</li>
                </ul>
            </div><!-- close .content-sidebar-section -->

            <div class="content-sidebar-section">
                <ul id="profile-watched-stats">
                  <li><span><a href="#" data-toggle="modal" data-target="#bookPrivateSession" class="btn">Book Private Session</a></span></li>

                  </ul>
            </div><!-- close .content-sidebar-section -->




        </div><!-- close #content-sidebar-pro -->
        <main  id="col-main-with-sidebar">

            <div class="dashboard-container">

                <ul class="dashboard-sub-menu">
                    <li ><a href="{{route('showPublicInfos',$user->slug)}}">Personelle Information</a></li>
                    <li class="current"><a href="{{route('showPublicProfile',$user->slug)}}">Store</a></li>
                    <li ><a href="{{route('showPublicEvents',$user->slug)}}">Events</a></li>
                    <li ><a href="{{route('showPublicChannel',$user->slug)}}">Channel</a></li>
                </ul><!-- close .dashboard-sub-menu -->

                @if(Session::has('message'))
                    <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ Session::get('message') }}</strong>
                    </div>
                @endif

                    <div class="row">
                        @foreach($products as $product)
                        <div class="col-12 col-md-6 col-lg-6 col-xl-4">
                            <div style="text-align: center" class="item-listing-container-skrn">
                                <a class="showProduct" href="#" data-toggle="modal" data-target="#showProduct" data-img="{{asset($product->image)}}" data-price="{{$product->price}}" data-name="{{$product->name}}" data-desc="{{$product->description}}"><img src="{{asset($product->image)}}" alt="Listing"></a>
                                <div class="item-listing-text-skrn">
                                    <div class="item-listing-text-skrn-vertical-align"><a  class="showProduct" href="#" data-toggle="modal" data-target="#showProduct" data-img="{{asset($product->image)}}" data-price="{{$product->price}}" data-name="{{$product->name}}" data-desc="{{$product->description}}">{{ $product->name }}</a>

                                    </div><!-- close .item-listing-text-skrn-vertical-align -->

                                </div><!-- close .item-listing-text-skrn -->
                            </div><!-- close .item-listing-container-skrn -->
                        </div><!-- close .col -->
                        @endforeach


                    </div><!-- close .row -->

                <div class="row">
                    <div class="col-md-12">

                        <div>
                            {{$products->links()}}
                        </div>
                    </div>
                </div>







            </div><!-- close .dashboard-container -->
        </main>
    </div>
  @include('General::store.modals.showProduct')
  @include('General::store.modals.buyProduct')

  @include('User::frontOffice.modals.bookPrivateSession')
  @include('User::frontOffice.modals.book')

  <script>
      $(document).on("click", ".showProduct", function () {
          var img = $(this).data('img');
          var name = $(this).data('name');
          var price = $(this).data('price');
          var desc = $(this).data('desc');
          $("#productImg").attr("src",img);
          $("#Pdesc").text(desc);
          $("#Pname").text(name);
          $("#Pprice").text(price +' $');


      });
  </script>

  <script type="text/javascript">
  $('.bookmark-user').on('click',function(event) {

  				event.preventDefault();
  					var bookmarkId;
  					var data;
  					bookmarkId = $(this).data('user');

  					data = {
  					'bookmarkId' : bookmarkId,
  					'bookmark' : 'user'
  					}

  					var img = $(this).find('img');


  				$.post('{{route('apiHandleBookmark')}}',
  				{
  				'_token': $('meta[name=csrf-token]').attr('content'), data

  				})
  				.done(function (res) {
  						if (res.event) {
  								img[0].src = "{{asset('images/bookmark.png')}}";
  								img[0].alt = "bookmark";
  						}else {
  							img[0].src = "{{asset('images/bookmark_add.png')}}";
  							img[0].alt = "bookmark_add";
  						}
  				})
  });
  </script>

@endsection
