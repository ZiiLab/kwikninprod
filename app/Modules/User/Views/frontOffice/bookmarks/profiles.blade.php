@extends('frontOffice.layout')

@section('content')

    <style>

        .card-counter{
            box-shadow: 2px 2px 10px #DADADA;
            margin: 5px;
            padding: 20px 10px;
            background-color: #fff;
            height: 100px;
            border-radius: 5px;
            transition: .3s linear all;
        }

        .card-counter:hover{
            box-shadow: 4px 4px 20px #DADADA;
            transition: .3s linear all;
        }

        .card-counter.primary{
            background-color: #007bff;
            color: #FFF;
        }

        .card-counter.danger{
            background-color: #ef5350;
            color: #FFF;
        }

        .card-counter.success{
            background-color: #66bb6a;
            color: #FFF;
        }

        .card-counter.info{
            background-color: #26c6da;
            color: #FFF;
        }

        .card-counter i{
            font-size: 5em;
            opacity: 0.2;
        }

        .card-counter .count-numbers{
            position: absolute;
            right: 35px;
            top: 20px;
            font-size: 32px;
            display: block;
        }

        .card-counter .count-name{
            position: absolute;
            right: 35px;
            top: 65px;
            font-style: italic;
            text-transform: capitalize;
            opacity: 0.5;
            display: block;
            font-size: 18px;
        }
        .activeTab {
            color: white;
            background-color: #02536B;
        }

        .table td{
          vertical-align:middle !important;
        }

        .btn {
          margin-bottom: 10px;
        }
    </style>


    <div id="sidebar-bg">
        @include('frontOffice.inc.sidebar')
        <main id="col-main">

            <div class="dashboard-container">


                            <div class="dashboard-sub-menu" style="text-align: center; margin-top: 25px">
                                  <h1>Profiles</h1>
                            </div>
                <div class="container-fluid">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ Session::get('message') }}</strong>
                        </div>
                    @endif
                    <div style="border-top: 1px solid rgba(0,0,0, 0.09);" class="row">

                      <div style="margin-top: 15px" class="col-md-2">
                          <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="btn "  href="{{route('showUserBookmark')}}" >Dashboard</a>
                                <a class="btn activeTab"  href="{{route('showBookmarkProfiles')}}" >Bookmarked Profiles</a>
                                <a class="btn " href="{{route('showBookmarkProducts')}}" >Bookmarked Products</a>
                                <a class="btn " href="{{route('showBookmarkEvents')}}" >Bookmarked Events</a>
                            </div>
                          </div>

                      </div>
                        <div style="text-align: center; margin-top: 25px" class="col-md-10">
                            <div class="card">
                                <div class="card-body">
                                    <table id="mytable" class="table table-bordred table-striped">

                                        <thead>

                                        <th>Name</th>
                                        <th>Profile</th>
                                        <th>Products</th>
                                        <th>Events </th>

                                        </thead>
                                        <tbody>
                                            @foreach($profiles as $profile)

                                        <tr>


                                            <td>{{ $profile->bookmarked->getFullName() }}</td>
                                            <td><a target="_blank"  href="{{route('showPublicInfos',$profile->bookmarked->slug)}}"><i class="fas fa-external-link-alt"></i> </a></td>
                                            <td><a target="_blank" href="{{route('showPublicProfile',$profile->bookmarked->slug)}}"><i class="fas fa-external-link-alt"></i> </a></td>
                                            <td><a target="_blank" href="{{route('showPublicEvents',$profile->bookmarked->slug)}}"><i class="fas fa-external-link-alt"></i> </a></td>
                                        </tr>
                                            @endforeach
                                        </tbody>

                                    </table>
                                    <div>
                                        {{ $profiles->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- close .dashboard-container -->


        </main>

    </div>


@endsection
