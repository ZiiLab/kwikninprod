@extends('frontOffice.layout')

@section('content')

    <style>
        .btn {
            margin-top: 10px;
        }
        .card-counter{
            box-shadow: 2px 2px 10px #DADADA;
            margin: 5px;
            padding: 20px 10px;
            background-color: #fff;
            height: 100px;
            border-radius: 5px;
            transition: .3s linear all;
        }

        .card-counter:hover{
            box-shadow: 4px 4px 20px #DADADA;
            transition: .3s linear all;
        }

        .card-counter.primary{
            background-color: #007bff;
            color: #FFF;
        }

        .card-counter.danger{
            background-color: #ef5350;
            color: #FFF;
        }

        .card-counter.success{
            background-color: #66bb6a;
            color: #FFF;
        }

        .card-counter.info{
            background-color: #26c6da;
            color: #FFF;
        }

        .card-counter i{
            font-size: 5em;
            opacity: 0.2;
        }

        .card-counter .count-numbers{
            position: absolute;
            right: 35px;
            top: 20px;
            font-size: 32px;
            display: block;
        }

        .card-counter .count-name{
            position: absolute;
            right: 35px;
            top: 65px;
            font-style: italic;
            text-transform: capitalize;
            opacity: 0.5;
            display: block;
            font-size: 18px;
        }
        .activeTab {
            color: white;
            background-color: #02536B;
        }
    </style>


<div id="sidebar-bg">
    @include('frontOffice.inc.sidebar')
    <main id="col-main" style="height: 75vh;overflow-y: auto;">

        <div class="dashboard-container">

          <div class="dashboard-sub-menu" style="text-align: center; margin-top: 25px">
                <h1>Bookmarks</h1>
          </div>

    <div class="container-fluid">
        @if(Session::has('message'))
            <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ Session::get('message') }}</strong>
            </div>
        @endif
        <div style="border-top: 1px solid rgba(0,0,0, 0.09);" class="row">

            <div style="margin-top: 15px" class="col-md-2">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                  <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                      <a class="btn activeTab"  href="{{route('showUserBookmark')}}" >Dashboard</a>
                      <a class="btn"  href="{{route('showBookmarkProfiles')}}" >Bookmarked Profiles</a>
                      <a class="btn " href="{{route('showBookmarkProducts')}}" >Bookmarked Products</a>
                      <a class="btn " href="{{route('showBookmarkEvents')}}" >Bookmarked Events</a>
                  </div>
                </div>

            </div>
            <div style="text-align: center; margin-top: 25px" class="col-md-10">
                <div class="card">
                    <div class="card-body">


                            <div class="row">
                                <div class="col-md-4 ">
                                    <div class="card-counter primary">
                                        <i class="icon-Add-UserStar"></i>
                                        <span class="count-numbers">{{count($profiles)}}</span>
                                        <span class="count-name">Profiles</span>
                                    </div>
                                    @if(count($profiles) == 0)
                                    <h6>No bookmarks yet!</h6>
                                    @else
                                     <a href="{{route('showBookmarkProfiles')}}">Bookmarked Profiles</a>
                                    @endif
                                </div>
                                <div class="col-md-4 ">
                                    <div class="card-counter info">
                                        <i class=" icon-Basket-Items"></i>
                                        <span class="count-numbers">{{count($products)}}</span>
                                        <span class="count-name">Products</span>
                                    </div>
                                    @if(count($products) == 0)
                                        <a href="{{route('showStore')}}" >Explore Products</a>
                                    @else
                                        <a href="{{route('showBookmarkProducts')}}">Bookmarked Products</a>
                                    @endif

                                </div>
                                <div class="col-md-4 ">
                                    <div class="card-counter info">
                                        <i class="icon-Alarm"></i>
                                        <span class="count-numbers">{{count($events)}}</span>
                                        <span class="count-name">Events</span>
                                    </div>
                                    @if(count($events) == 0)
                                        <a href="{{route('showEvents')}}" >Explore Events</a>
                                    @else
                                        <a href="{{route('showBookmarkEvents')}}">Bookmarked Events</a>
                                    @endif

                                </div>

                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

        </div><!-- close .dashboard-container -->


</main>

</div>

@endsection
