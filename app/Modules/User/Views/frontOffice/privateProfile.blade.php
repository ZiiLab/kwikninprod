@extends('frontOffice.layout')

@section('content')



<style media="screen">
    .help-block strong{
      font-size: 10px;
      color: red;
    }
    .fileContainer {
        overflow: hidden;
        position: relative;
    }

    .fileContainer [type=file] {
        cursor: inherit;
        display: block;
        font-size: 999px;
        filter: alpha(opacity=0);
        min-height: 100%;
        min-width: 100%;
        opacity: 0;
        position: absolute;
        right: 0;
        text-align: right;
        top: 0;
    }

    .btn.delete-photo{
        width: 173px;
      }

    .tabs{
      font-family: 'Segoe UI Regular', sans-serif;
      font-weight: 400;
      font-size: 13px;
      border: 1px solid #e7e7e7;
      background: #f9f9f9;
      color: #666666;
      width: 173px;

      padding: 10px 9px;
      text-align: center;
      margin-bottom: 15px;
    }

    .nav-link.tabs.active{
      border-color: #02536B;
      background: #02536B;
    }

    .service-icon i{
      margin: -1px;
      border: 1px solid #009C8F;
      border-radius: 50%;
      color: #009C8F;
      float: left;
      line-height: 2em;
      width: 2em;
      height: 2em
    }





  </style>
<div id="sidebar-bg">
    @include('frontOffice.inc.sidebar')
    <main id="col-main">

        <div class="dashboard-container">


            <ul class="dashboard-sub-menu">
                <li class="current"><a href="{{route('showProfile')}}">Account Settings</a></li>
                <li><a href="{{route('showHealerInfos')}}">My Info</a></li>
                <li><a href="{{route('showSessions')}}">My Sessions</a></li>
                <li  ><a href="{{route('showUserStore')}}">My Store</a></li>
                <li><a href="{{route('showUserEvent')}}">My Events</a></li>
                <li><a href="{{route('showUserCalander')}}">My Calander</a></li>
                <li><a href="{{route('showUserChannel')}}">My Channel</a></li>

            </ul><!-- close .dashboard-sub-menu -->

            <div class="container-fluid">
                <div class="row">

                    <div class="col-12  col-lg-3">

                        <form action="{{route('hundleUpdateProfilePhoto')}}" method="post" id="formPhoto" enctype="multipart/form-data" style="margin-bottom: -10px;">

                            @csrf
                            <div id="account-edit-photo">


                                <div><img src=" @if (!is_null(Auth::user()->image)) {{asset(Auth::user()->image)}} @else {{asset('frontOffice/images/unknown.png')}} @endif


                                  " alt="Account Image"></div>
                                <label class="fileContainer btn btn-green-pro">
                                    Change Profile Picture
                                    <input type="file" id="photo" name="photo" />
                                </label>
                                <p><a href="#!" id="delete" class="btn delete-photo">Delete Photo</a></p>
                            </div>


                        </form>

                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical" style=" align-items: center;margin-right: 10px; ">
                            <a class="nav-link tabs active service-icon" id="v-pills-information-tab" data-toggle="pill" href="#v-pills-information" role="tab" aria-controls="v-pills-information" aria-selected="true"><i class="fa fa-info"></i> General Information</a>
                            <a class="nav-link tabs service-icon" id="v-pills-password-tab" data-toggle="pill" href="#v-pills-password" role="tab" aria-controls="v-pills-password" aria-selected="false"><i class="fa fa-key" aria-hidden="true"></i> Change Password</a>
                        </div>



                    </div><!-- close .col -->
                    <div class="col">
                        <div class="tab-content" id="v-pills-tabContent">

                            <div class="tab-pane fade show active" id="v-pills-information" role="tabpanel" aria-labelledby="v-pills-information-tab">
                                <div class="form-group">
                                    @if(Session::has('message'))
                                    <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ Session::get('message') }}</strong>
                                    </div>
                                    @endif
                                    <form class="account-settings-form" method="post" action="{{route('handleUserUpdateProfile')}}">
                                        @csrf
                                        <h5>General Information</h5>
                                        <p class="small-paragraph-spacing">By letting us know your address, we can make our support experience much more personal.</p>
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="form-group">
                                                    <label for="first-name" class="col-form-label">First Name:</label>
                                                    <input type="text" class="form-control" name="first_name" id="first-name" value="{{Auth::user()->first_name}}">
                                                    @if ($errors->has('first_name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('first_name') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div><!-- close .col -->
                                            <div class="col-sm">
                                                <div class="form-group">
                                                    <label for="last-name" class="col-form-label">Last Name:</label>
                                                    <input type="text" class="form-control" name="last_name" id="last-name" value="{{Auth::user()->last_name}}">
                                                    @if ($errors->has('last_name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('last_name') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div><!-- close .col -->
                                            <div class="col-sm">
                                                <div class="form-group">
                                                    <label for="last-name" class="col-form-label">Username:</label>
                                                    <input type="text" class="form-control" name="slug" id="slug" value="{{Auth::user()->slug}}">
                                                    @if ($errors->has('slug'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('slug') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div><!-- close .col -->
                                        </div><!-- close .row -->
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="form-group">
                                                    <label for="country" class="col-form-label">Country :</label>
                                                    <select class="custom-select" id="country" name="country">
                                                        @if(Auth::user()->address)
                                                        <option @if(Auth::user()->address->country == 'Argentina') selected @endif value="Argentina">Argentina</option>
                                                        <option @if(Auth::user()->address->country == 'Australia') selected @endif value="Australia">Australia</option>
                                                        <option @if(Auth::user()->address->country == 'Bahamas') selected @endif value="Bahamas">Bahamas</option>
                                                        <option @if(Auth::user()->address->country == 'Belgium') selected @endif value="Belgium">Belgium</option>
                                                        <option @if(Auth::user()->address->country == 'Brazil') selected @endif value="Brazil">Brazil</option>
                                                        <option @if(Auth::user()->address->country == 'Canada') selected @endif value="Canada">Canada</option>
                                                        <option @if(Auth::user()->address->country == 'Chile') selected @endif value="Chile">Chile</option>
                                                        <option @if(Auth::user()->address->country == 'China') selected @endif value="China">China</option>
                                                        <option @if(Auth::user()->address->country == 'Denmark') selected @endif value="Denmark">Denmark</option>
                                                        <option @if(Auth::user()->address->country == 'Ecuador') selected @endif value="Ecuador">Ecuador</option>
                                                        <option @if(Auth::user()->address->country == 'France') selected @endif value="France">France</option>
                                                        <option @if(Auth::user()->address->country == 'Germany') selected @endif value="Germany">Germany</option>
                                                        <option @if(Auth::user()->address->country == 'Greece') selected @endif value="Greece">Greece</option>
                                                        <option @if(Auth::user()->address->country == 'Guatemala') selected @endif value="Guatemala">Guatemala</option>
                                                        <option @if(Auth::user()->address->country == 'Italy') selected @endif value="Italy">Italy</option>
                                                        <option @if(Auth::user()->address->country == 'Japan') selected @endif value="Japan">Japan</option>
                                                        <option @if(Auth::user()->address->country == 'Korea') selected @endif value="Korea">Korea</option>
                                                        <option @if(Auth::user()->address->country == 'Malaysia') selected @endif value="Malaysia">Malaysia</option>
                                                        <option @if(Auth::user()->address->country == 'Monaco') selected @endif value="Monaco">Monaco</option>
                                                        <option @if(Auth::user()->address->country == 'Morocco') selected @endif value="Morocco">Morocco</option>
                                                        <option @if(Auth::user()->address->country == 'New Zealand') selected @endif value="New Zealand">New Zealand</option>
                                                        <option @if(Auth::user()->address->country == 'Panama') selected @endif value="Panama">Panama</option>
                                                        <option @if(Auth::user()->address->country == 'Portugal') selected @endif value="Portugal">Portugal</option>
                                                        <option @if(Auth::user()->address->country == 'Russia') selected @endif value="Russia">Russia</option>
                                                        <option @if(Auth::user()->address->country == 'United Kingdom') selected @endif value="United Kingdom">United Kingdom</option>
                                                        <option @if(Auth::user()->address->country == 'United States') selected @endif value="United States">United States</option>
                                                        @else
                                                            <option  value="Argentina">Argentina</option>
                                                            <option  value="Australia">Australia</option>
                                                            <option value="Bahamas">Bahamas</option>
                                                            <option  value="Belgium">Belgium</option>
                                                            <option  value="Brazil">Brazil</option>
                                                            <option  value="Canada">Canada</option>
                                                            <option value="Chile">Chile</option>
                                                            <option value="China">China</option>
                                                            <option  value="Denmark">Denmark</option>
                                                            <option value="Ecuador">Ecuador</option>
                                                            <option  value="France">France</option>
                                                            <option value="Germany">Germany</option>
                                                            <option  value="Greece">Greece</option>
                                                            <option  value="Guatemala">Guatemala</option>
                                                            <option value="Italy">Italy</option>
                                                            <option value="Japan">Japan</option>
                                                            <option value="Korea">Korea</option>
                                                            <option value="Malaysia">Malaysia</option>
                                                            <option  value="Monaco">Monaco</option>
                                                            <option  value="Morocco">Morocco</option>
                                                            <option  value="New Zealand">New Zealand</option>
                                                            <option  value="Panama">Panama</option>
                                                            <option value="Portugal">Portugal</option>
                                                            <option  value="Russia">Russia</option>
                                                            <option  value="United Kingdom">United Kingdom</option>
                                                            <option   value="United States">United States</option>
                                                            @endif
                                                    </select>
                                                    @if ($errors->has('country'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('country') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div><!-- close .col -->
                                            <div class="col-sm">
                                                <div class="form-group">
                                                    <label for="last-name" class="col-form-label">City:</label>
                                                    <input type="text" class="form-control" name="city" id="city" placeholder="City" @if (!is_null(Auth::user()->address_id))
                                                    value="{{Auth::user()->address->city}}"
                                                    @endif>
                                                        @if ($errors->has('city'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('city') }}</strong>
                                                        </span>
                                                        @endif
                                                </div>
                                            </div><!-- close .col -->
                                            <div class="col-sm">
                                                <div class="form-group">
                                                    <label for="last-name" class="col-form-label">Zip Code:</label>
                                                    <input type="text" class="form-control" name="postal_code" id="postal_code" placeholder="xxxxxx" @if (!is_null(Auth::user()->address_id))
                                                    value="{{Auth::user()->address->postal_code}}"
                                                    @endif>
                                                        @if ($errors->has('postal_code'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('postal_code') }}</strong>
                                                        </span>
                                                        @endif
                                                </div>
                                            </div><!-- close .col -->
                                        </div><!-- close .row -->
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="form-group">
                                                    <label for="e-mail" class="col-form-label">Address</label>
                                                    <input type="text" class="form-control" name="address" id="address" placeholder="Address" @if (!is_null(Auth::user()->address_id))
                                                    value="{{Auth::user()->address->address}}"
                                                    @endif>
                                                        @if ($errors->has('address'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('address') }}</strong>
                                                        </span>
                                                        @endif
                                                </div>
                                            </div><!-- close .col -->


                                        </div><!-- close .row -->
                                        <hr>

                                        <h5>Account Information</h5>
                                        <p class="small-paragraph-spacing">You can change the email address and your phone here.</p>

                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="form-group">
                                                    <label for="e-mail" class="col-form-label">E-mail</label>
                                                    <input type="text" class="form-control" name="email" id="e-mail" placeholder="E-mail" value="{{Auth::user()->email}}">
                                                    @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div><!-- close .col -->
                                            <div class="col-sm">
                                                <div class="form-group">
                                                    <label for="e-mail" class="col-form-label">Phone Number</label>
                                                    <input type="text" class="form-control" name="phone" id="phone" value="{{Auth::user()->phone}}" placeholder="1-555-555-555">
                                                    @if ($errors->has('phone'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('phone') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div><!-- close .col -->

                                        </div><!-- close .row -->

                                        <hr>

                                        <div class="clearfix"></div>
                                        @if (Auth::user()->type == 0)
                                        <p><button type="submit" value="healer" name="healer" class="btn btn-green-pro">Save Changes</button></p>

                                        @else
                                        <p><button value="seeker" name="seeker" type="submit" class="btn btn-green-pro">Save Changes</button></p>
                                        @endif
                                        <br>
                                    </form>
                                </div>
                            </div> <!-- End Information Tab -->

                            <div class="tab-pane fade" id="v-pills-password" role="tabpanel" aria-labelledby="v-pills-password-tab">
                              <div class="form-group">
                                  @if(Session::has('message'))
                                  <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-block">
                                      <button type="button" class="close" data-dismiss="alert">×</button>
                                      <strong>{{ Session::get('message') }}</strong>
                                  </div>
                                  @endif
                              <form class="account-settings-form" method="post" action="{{route('handleUserUpdatePassword')}}">
                                  @csrf
                                  <h5>Change Password</h5>
                                  <p class="small-paragraph-spacing">You can change the password you use for your account here.</p>

                                  <div class="row">
                                      <div class="col-sm">
                                          <div class="form-group">
                                              <label for="password" class="col-form-label">Current Password:</label>
                                              <input type="password" class="form-control" name="oldPassword" id="oldPassword">
                                              @if ($errors->getBag('password')->has('oldPassword'))
                                              <span class="help-block">
                                                  <strong>{{ $errors->getBag('password')->first('oldPassword') }}</strong>
                                              </span>
                                              @endif
                                          </div>
                                      </div><!-- close .col -->

                                    </div><!-- close .row -->

                                    <div class="row">
                                      <div class="col-sm">
                                          <div class="form-group">
                                              <label for="new-password" class="col-form-label">New Password:</label>
                                              <input type="password" class="form-control" name="password" id="password" placeholder="Minimum of 6 characters">
                                              @if ($errors->getBag('password')->has('password'))
                                              <span class="help-block">
                                                  <strong>{{ $errors->getBag('password')->first('password') }}</strong>
                                              </span>
                                              @endif
                                          </div>
                                      </div><!-- close .col -->
                                        </div><!-- close .row -->
                                    <div class="row">
                                      <div class="col-sm">
                                          <div class="form-group">
                                              <label for="confirm-password" class="col-form-label">Confirm Password: </label>
                                              <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password">
                                          </div>
                                      </div><!-- close .col -->
                                  </div><!-- close .row -->




                                  <div class="clearfix"></div>

                                  <p><button type="submit" value="healer" name="healer" class="btn btn-green-pro">Save Changes</button></p>


                                  <br>
                              </form>
                              </div>
                            </div> <!-- End Change Password Tab -->


                        </div>
                    </div><!-- close .col -->

                </div><!-- close .row -->
            </div><!-- close .container-fluid -->

        </div><!-- close .dashboard-container -->



</main>

</div>
<script type="text/javascript">


    $('#photo').change(function() {
        $('#formPhoto').submit();
        $('#photo').val("");
    })

    document.getElementById("delete").onclick = function() {
        document.getElementById("formPhoto").submit();
    };
</script>

@if($errors->hasBag('password'))
    <script>
          $(document).ready(function(){
              $('#v-pills-information-tab').removeClass('active');
              $('#v-pills-information').removeClass('show');
              $('#v-pills-information').removeClass('active');

              $('#v-pills-password-tab').addClass('active');
              $('#v-pills-password').addClass('show');
              $('#v-pills-password').addClass('active');

          })
    </script>
@endif
@endsection
