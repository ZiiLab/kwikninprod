@extends('frontOffice.layout')

@section('content')

    <style>
        .btn {
            margin-top: 10px;
        }
        .card-counter{
            box-shadow: 2px 2px 10px #DADADA;
            margin: 5px;
            padding: 20px 10px;
            background-color: #fff;
            height: 100px;
            border-radius: 5px;
            transition: .3s linear all;
        }

        .card-counter:hover{
            box-shadow: 4px 4px 20px #DADADA;
            transition: .3s linear all;
        }

        .card-counter.primary{
            background-color: #007bff;
            color: #FFF;
        }

        .card-counter.danger{
            background-color: #ef5350;
            color: #FFF;
        }

        .card-counter.success{
            background-color: #66bb6a;
            color: #FFF;
        }

        .card-counter.info{
            background-color: #26c6da;
            color: #FFF;
        }

        .card-counter i{
            font-size: 5em;
            opacity: 0.2;
        }

        .card-counter .count-numbers{
            position: absolute;
            right: 35px;
            top: 20px;
            font-size: 32px;
            display: block;
        }

        .card-counter .count-name{
            position: absolute;
            right: 35px;
            top: 65px;
            font-style: italic;
            text-transform: capitalize;
            opacity: 0.5;
            display: block;
            font-size: 18px;
        }
        .activeTab {
            color: white;
            background-color: #02536B;
        }
    </style>


    <div id="sidebar-bg">
        @include('frontOffice.inc.sidebar')
        <main id="col-main">

            <div class="dashboard-container">


                <ul class="dashboard-sub-menu">
                    <li><a href="{{route('showProfile')}}">Account Settings</a></li>
                    <li><a href="{{route('showHealerInfos')}}">My Info</a></li>
                    <li ><a href="{{route('showSessions')}}">My Sessions</a></li>
                    <li  ><a href="{{route('showUserStore')}}">My Store</a></li>
                    <li><a href="{{route('showUserEvent')}}">My Events</a></li>
                    <li><a href="{{route('showUserCalander')}}">My Calander</a></li>
                    <li class="current"><a href="{{route('showUserChannel')}}">My Channel</a></li>
                </ul><!-- close .dashboard-sub-menu -->
                <div class="container-fluid">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ Session::get('message') }}</strong>
                        </div>
                    @endif

                    <div  class="row">
                    <div  class="col-md-7">
                        <div class="card">
                            <div class="card-body">
                              @if(count($sessions) == 0)
                              <ul class="list-group participationsList">

                                <li class="list-group-item">
                                <p style="text-align : center">No uploaded sessions found yet !</p>
                                </li>
                                  </ul>
                              @else

                                    <table id="mytable" class="table table-bordred table-striped">

                                        <thead>

                                        <th>Title</th>
                                        <th>Category</th>
                                        <th>Price</th>
                                        </thead>
                                        <tbody>
                                        @foreach($sessions as $session)
                                            <tr>
                                                <td>{{$session->title}}</td>
                                                <td>{{$session->category->label}}</td>
                                                <td>{{$session->free == 1 ? 'free' : $session->price.' $'}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>

                                    </table>
                              @endif

                                <a data-toggle="modal" data-target="#uploadSession" href="#" class="btn btn-block">Upload New Session</a>
                            </div>
                        </div>
                    </div>

                        <div class="col-md-5">
                            <div class="card">
                                <div  class="card-body">
                                  <ul class="list-group participationsList">
                                    <li class="list-group-item">
                                    <p style="text-align : center">No downloads yet !</p>
                                    </li>

                                  </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div><!-- close .dashboard-container -->


        </main>

    </div>


    @include('User::frontOffice.channel.modals.upload')

@endsection
