
@if($errors->hasBag('upload'))
    <script>
        $(document).ready(function(){
            $('#uploadSession').modal('show');
        })
    </script>
@endif

<style>
  .help-block {
    color: red;
    font-size: 12px;
  }
</style>

<div class="modal fade" id="uploadSession" tabindex="-1" role="dialog" aria-labelledby="startStreamingModal" aria-hidden="true">
    <button type="button" class="close float-close-pro" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header-pro">
                <h2>Upload a new session</h2>
                <h6>Fill these informations To Upload A Session !</h6>
            </div>
            <div class="modal-body-pro">
                <form action="{{ route('handleUploadSession') }}" method="post" enctype="multipart/form-data">

                  {{ csrf_field() }}
        <div class="form-group">
<label for="study_field" class="col-form-label"> Session Title :</label>
<input data-placement="right" data-toggle="tooltip" type="text" class="form-control title" name="title" id="title"  placeholder="Title here ...">
@if ($errors->getBag('upload')->has('title'))
    <span class="help-block">
{{ $errors->getBag('upload')->first('title') }}
</span>
@endif
</div>


                <div class="form-group">
                    <label for="price" class="col-form-label">Price :</label>
                    <input type="number" class="form-control price" name="price" id="price"  placeholder="Price here ...">
                </div>
                <div class="form-check form-group">

                  <input class="form-check-input " type="checkbox" name="free[]" value="" id="defaultCheck1">
                  <label class="form-check-label" for="defaultCheck1">
                    Free ?
                  </label>
                </div>

                 <div class="form-group">
                                        <label for="first-name" class="col-form-label">Description :</label>
                                        <textarea id="description" placeholder="Description here ..." class="form-control" name="desc" rows="4" cols="78 "></textarea>


                                    </div>

                <div class="form-group cat">
                                  <label for="first-name" class="col-form-label">Session Category :</label>
                                          <ul class="registration-genres-choice">

                                            @foreach ($categories as $categorie)
                                              <li data-category = {{$categorie->id}}>
                                                  <i class="fas fa-check-circle"></i>
                                                  <img src="https://via.placeholder.com/200x300" alt="Body">
                                                  <h6>{{$categorie->label}}</h6>
                                              </li>

                                            @endforeach


                                        </ul>


                                <div class="clearfix"></div>
                                @if ($errors->getBag('upload')->has('category'))
                                    <span class="help-block">
                                {{ $errors->getBag('upload')->first('category') }}
                                </span>
                                @endif
                            </div><!-- close .registration-genres-step -->
                            <div class="form-group">
                                <label for="first-name" class="col-form-label">Choose video to upload :</label>
                                             <input name="video" id="file" type="file" class="form-control"><br/>
                                             @if ($errors->getBag('upload')->has('video'))
                                                 <span class="help-block">
                                             {{ $errors->getBag('upload')->first('video') }}
                                             </span>
                                             @endif

                                         </div>
                                         <input type="hidden" name="category" id="cat_id" value="">



                            <button type="submit" id="startUpload" class="btn btn-block"> <span>Submit !</span></button>
  </form>




            </div><!-- close .modal-body -->


        </div><!-- close .modal-content -->
    </div><!-- close .modal-dialog -->
</div><!-- close .modal -->

<script>

$('.registration-genres-choice li').on('click', function () {
    $(".registration-genres-choice li").each(function() {
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
        }

    });
        $('#cat_id').val($(this).data('category'));
  });
    (function ($) {
        $.fn.buttonLoader = function (action) {
            var self = $(this);
            //start loading animation
            if (action == 'start') {
                if ($(self).attr("disabled") == "disabled") {
                    e.preventDefault();
                }
                //disable buttons when loading state
                $('.has-spinner').attr("disabled", "disabled");
                $(self).attr('data-btn-text', $(self).text());
                //binding spinner element to button and changing button text
                $(self).html('<span class="spinner"><i class="fa fa-spinner fa-spin"></i></span>Uploading video, please wait ...');
                $(self).addClass('active');
            }
            //stop loading animation
            if (action == 'stop') {
                $(self).html($(self).attr('data-btn-text'));
                $(self).removeClass('active');
                //enable buttons after finish loading
                $('.has-spinner').removeAttr("disabled");
            }
        }
    })(jQuery);
</script>

<script type="text/javascript">

  $('#startUpload').on('click' , function () {
    $(btn).buttonLoader('start');
  });

</script>
