<div class="modal fade" id="welcomeModal" tabindex="-1" role="dialog" aria-labelledby="welcomeModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header-pro">
                <h2>Welcome to the Kwiknin community</h2>
                <div class="alert alert-primary  fade show" role="alert" style="text-align:center;">
                  <strong>The register is done !</strong>We have sent you an activation link to your e-mail.
                  <button type="button" class="close" aria-label="Close">
                    <span aria-hidden="true"></span>
                  </button>
                </div>
            </div>

            <div class="container">
                <div class="registration-step-final-footer">
                      <a href="{{route('showWModelVerification')}}" class="btn btn-block btn-green-pro">Ok</a>
                </div>
            </div>

        </div><!-- close .modal-content -->
    </div><!-- close .modal-dialog -->
</div><!-- close .modal -->
