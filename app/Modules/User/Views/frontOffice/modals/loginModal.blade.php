
<div class="modal fade" id="LoginModal" tabindex="-1" role="dialog" aria-labelledby="LoginModal" aria-hidden="true">
    <button type="button" class="close float-close-pro" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header-pro">
                <h2>Welcome Back</h2>
                <h6>Sign in to your account to continue using Kwiknin</h6>
            </div>
            <div class="modal-body-pro social-login-modal-body-pro">

                <div class="registration-social-login-container">
                 <!--    <div style="display: none" class="card bg-danger text-white invalid">
                        <div class="card-body">Invalid Credentials !</div>
                    </div>
                    <div style="display: none" class="card bg-danger text-white disabled">
                        <div class="card-body">Account not activated !</div>
                    </div> -->
                        <div class="form-group">
                            <input type="text" data-placement="right"  class="form-control InvalidE" id="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="password" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <button type="button" id="signin" class="btn btn-green-pro btn-display-block">Sign In</button>
                        </div>
                        <div class="container-fluid">
                            <div class="row no-gutters">
                                <div class="col checkbox-remember-pro"><input type="checkbox" id="checkbox-remember"><label for="checkbox-remember" class="col-form-label">Remember me</label></div>
                                <div class="col forgot-your-password"><a href="#!">Forgot your password?</a></div>
                            </div>
                        </div><!-- close .container-fluid -->



                    <div class="registration-social-login-or"></div>

                </div><!-- close .registration-social-login-container -->

                <div class="registration-social-login-options">

                    <img src="{{ asset ('frontOffice') }}/images/kwiknin-logo.jpg" alt="">

                </div><!-- close .registration-social-login-options -->

                <div class="clearfix"></div>



            </div><!-- close .modal-body -->

            <a class="not-a-member-pro" id="regBtn" data-toggle="modal" href="#" data-target="#registerModal"  >Not a member? <span>Join Today!</span></a>
        </div><!-- close .modal-content -->
    </div><!-- close .modal-dialog -->
</div><!-- close .modal -->
@include('User::frontOffice.modals.registerModal')
<script>
    (function ($) {
        $.fn.buttonLoader = function (action) {
            var self = $(this);
            //start loading animation
            if (action == 'start') {
                if ($(self).attr("disabled") == "disabled") {
                    e.preventDefault();
                }
                //disable buttons when loading state
                $('.has-spinner').attr("disabled", "disabled");
                $(self).attr('data-btn-text', $(self).text());
                //binding spinner element to button and changing button text
                $(self).html('<span class="spinner"><i class="fa fa-spinner fa-spin"></i></span>Loading');
                $(self).addClass('active');
            }
            //stop loading animation
            if (action == 'stop') {
                $(self).html($(self).attr('data-btn-text'));
                $(self).removeClass('active');
                //enable buttons after finish loading
                $('.has-spinner').removeAttr("disabled");
            }
        }
    })(jQuery);
</script>
    <script>

    function signinForm() {
        var btn = $('#signin');

        var data = {
            'email' : $('#email').val(),
            'password' : $('#password').val()
        }

        $(btn).buttonLoader('start');
        $(".InvalidE").removeAttr('data-original-title');
        $.post('{{route('apiHandleUserLogin')}}',
            {
                '_token': $('meta[name=csrf-token]').attr('content'), data : data

            })
            .done(function (res) {

                btn.buttonLoader('stop');

                if(res['status'] == 402){
                      $(".InvalidE").attr('data-original-title', "The email and password you've entered does not much any account").tooltip('show');

                }else if(res['status'] == 401){
                       $(".InvalidE").attr('data-original-title', "The email and password you've entered does not much any account").tooltip('show');
                }
                else {
                    window.location = res['url'];

                }
            })
            .fail(function (res) {
            console.log(res);
        })
    }

    function signinFormEnter(event) {

      if ($('#LoginModal').hasClass('show')) {
        if (event.keyCode === 13) {
            signinForm();
          }
      }
    }

    $('#signin').on('click', signinForm);

    $(document).on('keyup',signinFormEnter);

    $('#regBtn').click(function() {
      $('#LoginModal').modal('hide')
    // $("regBtn").unbind();
   })

</script>
@if (Session::get('data'))

  <script type="text/javascript">
  $(function () {
      $('#LoginModal').modal('show');
  })


  </script>
      <?php Session::forget('success');?>


@endif
