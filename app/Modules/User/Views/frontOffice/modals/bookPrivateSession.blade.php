<style media="screen">
    .has-error input {
    border : 2px solid red !important;
  }

  .help-block strong{
    font-size: 10px;
    color: red;
  }

  .hide{
    display: none;
  }

  </style>
<style media="screen">
  #error{
      font-size: 9px;
      color: red;
      margin-top: 8px;
      text-align: center;

  }

  input[type=number]{
    width: 100%;
padding: 10px;
margin-top: 12px;
  }
    #Pbuy{
        margin-top:15px !important;
        margin-buttom:15px !important;
    }
</style>
<div  class="modal fade" id="bookPrivateSession" tabindex="-1" role="dialog" aria-labelledby="startStreamingModal" aria-hidden="true" data-productId="">
    <button type="button" class="close float-close-pro" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <div style="max-width: 70%!important;" class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header-pro">
                <h6> Book Session </h6>
            </div>
            <div class="modal-body-pro">

                <div class="card">
                    <div class="card-body">
                      <div id='calendar'></div>
                </div>

            </div><!-- close .modal-body -->


        </div><!-- close .modal-content -->
    </div><!-- close .modal-dialog -->
  </div><!-- close .modal -->
</div><!-- close .modal -->
<script>
  $(document).ready(function () {

        var SITEURL = "{{url('/')}}";
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

        var calendar = $('#calendar').fullCalendar({

            editable: true,

            header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay'
      },
            events : [
            @foreach($sessionEvents as $sessionEvent)
            {
                id : '{{ $sessionEvent->id }}',
                title : '{{ $sessionEvent->title }}',
                @if($sessionEvent->allDay == 1)
                  start : '{{ $sessionEvent->start_date }}',
                allDay: true,
                @else
                    start : '{{ $sessionEvent->start_date }}',
                    end : '{{ $sessionEvent->end_date }}',
                    allDay: false,
                @endif

                editable: true,
                @if($sessionEvent->availability == 0)
                color: 'red',
                textColor: 'white',
                @elseif($sessionEvent->availability == 1)
                color: 'green',
                textColor: 'white',
                @elseif($sessionEvent->availability == 3)
                color: 'yellow',
                textColor: 'red',
                @endif

                              },


            @endforeach
        ],
            displayEventTime: true,
            editable: true,
            eventRender: function (event, element, view) {
                if (event.allDay === 'true') {
                    event.allDay = true;
                } else {
                    event.allDay = false;
                }
            },
            selectable: true,
            selectHelper: true,
            select: function (start, end, allDay) {

              var starttime = moment(start).format('YYYY-MM-DD HH:mm:ss');
              var endtime = moment(end).format('YYYY-MM-DD HH:mm:ss');
              var allDay = !start.hasTime() && !end.hasTime();

              $('#psstarttime').val(starttime);
              $('#psendtime').val(endtime);
              $('#psallDay').val(allDay);
              $('#book').modal('show');
            }


        });
  });

  function displayMessage(message) {
    $(".response").html("<div class='success'>"+message+"</div>");
    setInterval(function() { $(".success").fadeOut(); }, 1000);
  }
</script>
