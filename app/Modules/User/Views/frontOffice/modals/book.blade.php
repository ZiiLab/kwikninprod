
<div class="modal fade" id="book" tabindex="-1" role="dialog" aria-labelledby="startStreamingModal" aria-hidden="true">
    <button type="button" class="close float-close-pro" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header-pro">
                <h6>Fill these informations To Boook The Session !</h6>
            </div>
            <div class="modal-body-pro">
<form class="" action="{{ route('hanldeBookPrivateEvent', $user->slug) }}" method="post">
{{ csrf_field() }}

        <div class="form-group">
<label for="study_field" class="col-form-label"> Session Title :</label>
<input data-placement="right" data-toggle="tooltip" type="text" class="form-control title" name="title" id="title"  placeholder="Title here ..." required>
</div>



                 <div class="form-group">
                                        <label for="first-name" class="col-form-label">Description :</label>
                                        <textarea required id="description" placeholder="Description here ..." class="form-control" name="description" rows="4" cols="78 "></textarea>


                                    </div>

                <div class="form-group cat">
                                  <label for="first-name" class="col-form-label">Session Category :</label>
                                          <ul class="registration-genres-choice">

                                            @foreach ($categories as $categorie)
                                              <li data-category = {{$categorie->id}}>
                                                  <i class="fas fa-check-circle"></i>
                                                  <img src="https://via.placeholder.com/200x300" alt="Body">
                                                  <h6>{{$categorie->label}}</h6>
                                              </li>
                                            @endforeach



                                        </ul>


                                <div class="clearfix"></div>
                            </div><!-- close .registration-genres-step -->

                              <input type="hidden" name="category" id="catId" value="">
                              <input type="hidden" name="starttime" id="psstarttime"  value="">
                              <input type="hidden" name="endtime" id="psendtime" value="">
                              <input type="hidden" name="allday" id="psallDay" value="">
                            <button type="submit" class="btn btn-block"> <span>Submit !</span></button>

</form>



            </div><!-- close .modal-body -->


        </div><!-- close .modal-content -->
    </div><!-- close .modal-dialog -->
</div><!-- close .modal -->

<script type="text/javascript">
$('.registration-genres-choice li').on('click', function () {
    $(".registration-genres-choice li").each(function() {
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
        }

    });
     $('#catId').val($(this).data('category'));

});
</script>
