<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModal" aria-hidden="true">
    <button type="button" class="close float-close-pro" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header-pro">
                <h2>Welcome To Kwiknin</h2>
                <h6>Sign up to continue</h6>
            </div>

<div class="container">
            <div class="row">

                    <div class="col-md-4"></div>
                <div class="col-md-4">
                          <a class="not-a-member-pro" href="{{route('showRegisterUserStepOne','healer')}}"><span>Sign Me Up</span></a>
                </div>
                <div class="col-md-4"></div>

            </div>
          </div>

            <a id="loginBtn" style="margin-top: 25px" class="not-a-member-pro" href="#" data-toggle="modal" data-target="#LoginModal">Already a member? <span>Sign In</span></a>
        </div><!-- close .modal-content -->
    </div><!-- close .modal-dialog -->
</div><!-- close .modal -->

<script>
    (function ($) {
        $.fn.buttonLoader = function (action) {
            var self = $(this);
            //start loading animation
            if (action == 'start') {
                if ($(self).attr("disabled") == "disabled") {
                    e.preventDefault();
                }
                //disable buttons when loading state
                $('.has-spinner').attr("disabled", "disabled");
                $(self).attr('data-btn-text', $(self).text());
                //binding spinner element to button and changing button text
                $(self).html('<span class="spinner"><i class="fa fa-spinner fa-spin"></i></span>Loading');
                $(self).addClass('active');
            }
            //stop loading animation
            if (action == 'stop') {
                $(self).html($(self).attr('data-btn-text'));
                $(self).removeClass('active');
                //enable buttons after finish loading
                $('.has-spinner').removeAttr("disabled");
            }
        }
    })(jQuery);
</script>
    <script>


    $('#signin').click(function () {
        var btn = $(this);

        var data = {
            'email' : $('#email').val(),
            'password' : $('#password').val()
        }

        $(btn).buttonLoader('start');

        $.post('{{route('apiHandleUserLogin')}}',
            {
                '_token': $('meta[name=csrf-token]').attr('content'), data : data

            })
            .done(function (res) {

                $(btn).buttonLoader('stop');

                if(res['status'] == 402){
                  $('.invalid').show();
                }else if(res['status'] == 401){
                    $('.disabled').show();
                }
                else {
                    window.location = res['url'];

                }
            })

    });

   $('#loginBtn').click(function() {
      $('#registerModal').modal('hide')
   })

</script>
