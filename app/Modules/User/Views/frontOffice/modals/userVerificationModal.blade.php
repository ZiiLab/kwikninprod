<style type="text/css">


          #data label{
              display: block;
      margin: 10px 0 5px 0;
      font-size: 11px;
      line-height: normal;
      font-weight: bold;
          }
          .text-error{
      color: red;
      font-size: 10px;
      line-height: normal;
          }

          .error{
            border: 1px solid red;
          }

    #data input[type="text"].error{
      border: 1px solid red;
    }
    #data h1{
      padding: 0 2%;
    }
    #data p{
      margin: 0;
      background: lightgray;
    }
    #data input[type="text"],
    #data input[type="password"]{
      width: 50px;
      height: 50px;
      margin-right: 5px;
      margin-bottom: 0;
      text-align: center;
    }
    #data input[type="text"]:not(:last-of-type),
    #data input[type="password"]:not(:last-of-type){
      margin-right: 5px;
    }
    #data input[type="text"].error,
    #data input[type="password"].error{
      border-color: #ff0000;
    }
    .col{
      float: left;
      margin: 2%;
      padding: 1% 2% 2% 2%;
      width: 42%;
      background: white;
    }
  </style>

<div class="modal fade" id="userVerificationModal" tabindex="-1" role="dialog" aria-labelledby="userVerificationModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
      <div class="modal-content">
          <div class="modal-header-pro">
              <h2>We have send you an activation email, where you can find the pin code.</h2>
                <div id="data">
                    <label>Pin</label>
                    <div data-pin></div>

                    <p class="message"></p>
                </div>
                <br />
                <div class="form-group">
                    <button type="button" id="validate" class="btn btn-green-pro btn-display-block">Validate</button>
                </div>
          </div>


      </div><!-- close .modal-content -->


        </div><!-- close .modal-content -->
    </div><!-- close .modal-dialog -->
</div><!-- close .modal -->

<script>
    (function ($) {
        $.fn.buttonLoader = function (action) {
            var self = $(this);
            //start loading animation
            if (action == 'start') {
                if ($(self).attr("disabled") == "disabled") {
                    e.preventDefault();
                }
                //disable buttons when loading state
                $('.has-spinner').attr("disabled", "disabled");
                $(self).attr('data-btn-text', $(self).text());
                //binding spinner element to button and changing button text
                $(self).html('<span class="spinner"><i class="fa fa-spinner fa-spin"></i></span>Loading');
                $(self).addClass('active');
            }
            //stop loading animation
            if (action == 'stop') {
                $(self).html($(self).attr('data-btn-text'));
                $(self).removeClass('active');
                //enable buttons after finish loading
                $('.has-spinner').removeAttr("disabled");
            }
        }
    })(jQuery);
</script>
<script type="text/javascript">
    (function ($, document) {
      $(document).ready(function () {
        $('[data-pin]').pin({
          displayMessage: $('.message'),
          allowSequential: false,
          allowRepeat: false,
          count: 6
        });

        $('[name="next"]').on('click', function (e) {
          e.preventDefault();
          $('[data-pin]').data('plugin_pin').init(true);


        });
      });
    })(jQuery, document);
      </script>
  <script src="{{asset('frontOffice/js/jquery.pin.js')}}" type="text/javascript"></script>
@if(Session::has('userStatus'))
	@if (Session::get('userStatus') == 0)
	<script type="text/javascript">

		$(function() {


      $('#validate').on('click',function () {

       var pin = $('[name="pin_0"]').val();
        pin += $('[name="pin_1"]').val();
        pin += $('[name="pin_2"]').val();
        pin += $('[name="pin_3"]').val();
        pin += $('[name="pin_4"]').val();
        pin += $('[name="pin_5"]').val();



        var btn = $('#validate');

        var data = {
            'pin' : pin
        }

        $(btn).buttonLoader('start');

        $.post('{{route('apiHandleUserCodeVlidation')}}',
            {
                '_token': $('meta[name=csrf-token]').attr('content'), data : data

            })
            .done(function (res) {

                btn.buttonLoader('stop');

                if(res['status'] == 402){
                      $(".message").html("The activation code is invalid");

                      $('#data input').addClass('error');

                }else if(res['status'] == 401){
                       $(".message").html("Your account actived");
                }
                else {
                    window.location = res['url'];

                }


            })


      })


				$('#userVerificationModal').modal({show:true});
		})

		</script>
			@endif
@endif
