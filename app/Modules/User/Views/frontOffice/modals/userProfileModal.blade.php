<div class="modal fade" id="userProfileModal" tabindex="-1" role="dialog" aria-labelledby="userProfileModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-md" role="document">
      <div class="modal-content">
          <div class="modal-header-pro">
              <h2>Thank you for complete your profile</h2>
              <div class="alert alert-primary  fade show" role="alert" style="text-align:center;">
                <strong>The Payments Information has been added to your profile.
              </div>
          </div>

          <div class="container">
              <div class="registration-step-final-footer">
                    <a href="{{route('showHome')}}" class="btn btn-block btn-green-pro">Ok</a>
              </div>
          </div>

      </div><!-- close .modal-content -->
  </div><!-- close .modal-dialog -->
</div><!-- close .modal -->

<script>
    (function ($) {
        $.fn.buttonLoader = function (action) {
            var self = $(this);
            //start loading animation
            if (action == 'start') {
                if ($(self).attr("disabled") == "disabled") {
                    e.preventDefault();
                }
                //disable buttons when loading state
                $('.has-spinner').attr("disabled", "disabled");
                $(self).attr('data-btn-text', $(self).text());
                //binding spinner element to button and changing button text
                $(self).html('<span class="spinner"><i class="fa fa-spinner fa-spin"></i></span>Loading');
                $(self).addClass('active');
            }
            //stop loading animation
            if (action == 'stop') {
                $(self).html($(self).attr('data-btn-text'));
                $(self).removeClass('active');
                //enable buttons after finish loading
                $('.has-spinner').removeAttr("disabled");
            }
        }
    })(jQuery);
</script>




	<script type="text/javascript">

		$(function() {
				$('#userProfileModal').modal({show:true});
		});

		</script>
