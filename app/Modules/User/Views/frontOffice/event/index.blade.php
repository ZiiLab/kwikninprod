@extends('frontOffice.layout')

@section('content')

    <style>
        .btn {
            margin-top: 10px;
        }
        .card-counter{
            box-shadow: 2px 2px 10px #DADADA;
            margin: 5px;
            padding: 20px 10px;
            background-color: #fff;
            height: 100px;
            border-radius: 5px;
            transition: .3s linear all;
        }

        .card-counter:hover{
            box-shadow: 4px 4px 20px #DADADA;
            transition: .3s linear all;
        }

        .card-counter.primary{
            background-color: #007bff;
            color: #FFF;
        }

        .card-counter.danger{
            background-color: #ef5350;
            color: #FFF;
        }

        .card-counter.success{
            background-color: #66bb6a;
            color: #FFF;
        }

        .card-counter.info{
            background-color: #26c6da;
            color: #FFF;
        }

        .card-counter i{
            font-size: 5em;
            opacity: 0.2;
        }

        .card-counter .count-numbers{
            position: absolute;
            right: 35px;
            top: 20px;
            font-size: 32px;
            display: block;
        }

        .card-counter .count-name{
            position: absolute;
            right: 35px;
            top: 65px;
            font-style: italic;
            text-transform: capitalize;
            opacity: 0.5;
            display: block;
            font-size: 18px;
        }
        .activeTab {
            color: white;
            background-color: #02536B;
        }
    </style>


<div id="sidebar-bg">
    @include('frontOffice.inc.sidebar')
    <main id="col-main">

        <div class="dashboard-container">


            <ul class="dashboard-sub-menu">
                <li><a href="{{route('showProfile')}}">Account Settings</a></li>
                <li><a href="{{route('showHealerInfos')}}">My Info</a></li>
                <li><a href="{{route('showSessions')}}">My Sessions</a></li>
                <li  ><a href="{{route('showUserStore')}}">My Store</a></li>
                <li class="current"><a href="{{route('showUserEvent')}}">My Events</a></li>
                <li><a href="{{route('showUserCalander')}}">My Calander</a></li>
                <li><a href="{{route('showUserChannel')}}">My Channel</a></li>
            </ul><!-- close .dashboard-sub-menu -->
    <div class="container-fluid">
        @if(Session::has('message'))
            <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ Session::get('message') }}</strong>
            </div>
        @endif
        <div style="border-top: 1px solid rgba(0,0,0, 0.09);" class="row">

            <div style="margin-top: 15px" class="col-md-2">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                  <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                      <a class="btn activeTab"  href="{{route('showDashboard')}}" >Dashboard</a>
                      <a class="btn"  href="{{route('handleAddEvent')}}" >Add New Event</a>

                      <a class="btn"  href="{{route('showListEvents')}}" >Event List</a>
                      <a class="btn"  href="{{route('showListBookings')}}">Bookings</a>
                  </div>
                </div>

            </div>
            <div style="text-align: center; margin-top: 25px" class="col-md-10">
                <div class="card">
                    <div class="card-body">

                      @if(Auth::user()->events)
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="card-counter info">
                                        <i class="icon-Alarm"></i>
                                        <span class="count-numbers">{{count(Auth::user()->events)}}</span>
                                        <span class="count-name">Events</span>
                                    </div>
                                    @if(count(Auth::user()->events) == 0)
                                        <a href="{{route('handleAddEvent')}}" >Start With Creating Events</a>
                                    @else
                                        <a href="{{route('showListEvents')}}" >Events List</a>
                                    @endif

                                </div>
                                <div class="col-md-6">
                                    <div class="card-counter danger">
                                        <i class="icon-Billing"></i>
                                        <span class="count-numbers">{{count($bookings)}}</span>
                                        <span class="count-name">Bookings</span>
                                    </div>

                                    @if(count($bookings) == 0)
                                        <a href="#" >No bookings Yet!</a>

                                    @else
                                        <a href="{{route('showListBookings')}}" >More Details</a>
                                    @endif


                                </div>
                            </div>
                          @else
                            <p> It seems that you dont have a events yet !</p>
                            <a href="{{route('showListTags')}}" class="btn">Start With Creating Some Catageories !</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

        </div><!-- close .dashboard-container -->


</main>

</div>

@endsection
