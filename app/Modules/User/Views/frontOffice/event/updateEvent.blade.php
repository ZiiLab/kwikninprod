@extends('frontOffice.layout')


@section('content')

    <style>
        .help-block strong{
            font-size: 10px;
            color: red;
        }
        #footer-pro {
            position: initial !important;
            margin-top: 50px;

        }

        .fileContainer {
            overflow: hidden;
            position: relative;
        }

        .fileContainer [type=file] {
            cursor: inherit;
            display: block;
            font-size: 999px;
            filter: alpha(opacity=0);
            min-height: 100%;
            min-width: 100%;
            opacity: 0;
            position: absolute;
            right: 0;
            text-align: right;
            top: 0;
        }

        .btn {
            margin-top: 10px;
        }
        .card-counter{
            box-shadow: 2px 2px 10px #DADADA;
            margin: 5px;
            padding: 20px 10px;
            background-color: #fff;
            height: 100px;
            border-radius: 5px;
            transition: .3s linear all;
        }

        .card-counter:hover{
            box-shadow: 4px 4px 20px #DADADA;
            transition: .3s linear all;
        }

        .card-counter.primary{
            background-color: #007bff;
            color: #FFF;
        }

        .card-counter.danger{
            background-color: #ef5350;
            color: #FFF;
        }

        .card-counter.success{
            background-color: #66bb6a;
            color: #FFF;
        }

        .card-counter.info{
            background-color: #26c6da;
            color: #FFF;
        }

        .card-counter i{
            font-size: 5em;
            opacity: 0.2;
        }

        .card-counter .count-numbers{
            position: absolute;
            right: 35px;
            top: 20px;
            font-size: 32px;
            display: block;
        }

        .card-counter .count-name{
            position: absolute;
            right: 35px;
            top: 65px;
            font-style: italic;
            text-transform: capitalize;
            opacity: 0.5;
            display: block;
            font-size: 18px;
        }
        .activeTab {
            color: white;
            background-color: #02536B;
        }
    </style>


    <div id="sidebar-bg">
        @include('frontOffice.inc.sidebar')
        <main id="col-main">

            <div class="dashboard-container">


                <ul class="dashboard-sub-menu">
                  <li><a href="{{route('showProfile')}}">Account Settings</a></li>
                  <li><a href="{{route('showHealerInfos')}}">My Info</a></li>
                  <li><a href="{{route('showSessions')}}">My Sessions</a></li>
                  <li  ><a href="{{route('showUserStore')}}">My Store</a></li>
                  <li class="current"><a href="{{route('showUserEvent')}}">My Events</a></li>
                  <li><a href="{{route('showUserCalander')}}">My Calander</a></li>
                  <li><a href="{{route('showUserChannel')}}">My Channel</a></li>
                </ul><!-- close .dashboard-sub-menu -->
                <div class="container-fluid">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ Session::get('message') }}</strong>
                        </div>
                    @endif
                    <div style="border-top: 1px solid rgba(0,0,0, 0.09);" class="row">

                        <div style="margin-top: 15px" class="col-md-2">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="btn "  href="{{route('showDashboard')}}" >Dashboard</a>
                                <a class="btn"  href="{{route('handleAddEvent')}}" >Add New Event</a>

                                <a class="btn"  href="{{route('showListEvents')}}" >Event List</a>
                                <a class="btn"  href="{{route('showListBookings')}}">Bookings</a>
                            </div>

                        </div>
                        <div style="text-align: center; margin-top: 25px" class="col-md-10">
                            <div class="card">
                                <div class="card-body">
                                    <form action="{{route('handleUpdateEvent',$event->id)}}" method="post"  enctype="multipart/form-data" style="margin-bottom: -10px;">
                                        @csrf

                                        <div class="row">

                                            <div class="col-md-4">

                                                <div id="account-edit-photo">


                                                    <div><img src="{{asset($event->medias->first()->link)}}" id="productImage" alt="Event Image"></div>
                                                    <label class="fileContainer btn btn-green-pro">
                                                        Choose Event Image
                                                        <input type="file" id="imgInput" name="photo" />
                                                    </label>
                                                    @if ($errors->has('photo'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('photo') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>

                                                <div class="form-group">
                                                    <label for="first-name" class="col-form-label">Start Date:</label>
                                                    <input type="date" value="{{$event->start_date->format('Y-m-d')}}" class="form-control" name="start_date">
                                                    @if ($errors->has('start_date'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('start_date') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>

                                                <div class="form-group">
                                                    <label for="first-name" class="col-form-label">End Date:</label>
                                                    <input type="date" value="{{$event->end_date->format('Y-m-d')}}" class="form-control" name="end_date">
                                                    @if ($errors->has('end_date'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('end_date') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>



                                            </div>

                                            <div class="col-md-8">


                                                <h5>Add New Event</h5>
                                                <div class="row">
                                                    <div class="col-sm">
                                                        <div class="form-group">
                                                            <label for="first-name" class="col-form-label">Title:</label>
                                                            <input type="text" value="{{$event->title}}" class="form-control" name="title">
                                                            @if ($errors->has('title'))
                                                                <span class="help-block">
                                                        <strong>{{ $errors->first('title') }}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div><!-- close .col -->


                                                </div><!-- close .row -->

                                                <div class="row">
                                                    <div class="col-sm">
                                                        <div class="form-group">
                                                            <label for="country" class="col-form-label">Pick Tags :</label>
                                                            <select class="custom-select"  multiple  name="tags[]">
                                                                @foreach(\App\Modules\User\Models\Tag::all() as $category)
                                                                    @if($event->tags->contains($category->id))
                                                                        <option selected value="{{$category->id}}"> {{$category->name}}</option>
                                                                    @else
                                                                        <option  value="{{$category->id}}"> {{$category->name}}</option>
                                                                    @endif
                                                                @endforeach

                                                            </select>
                                                            @if ($errors->has('tags'))
                                                                <span class="help-block">
                                                        <strong>{{ $errors->first('tags') }}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div><!-- close .col -->
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm">
                                                        <div class="form-group">
                                                            <label for="last-name" class="col-form-label">Short Description:</label>
                                                            <input type="text" value="{{$event->short_description}}" class="form-control" name="short_description" >
                                                            @if ($errors->has('short_description'))
                                                                <span class="help-block">
                                                        <strong>{{ $errors->first('short_description') }}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div><!-- close .col -->


                                                </div>

                                                <div class="row">
                                                    <div class="col-sm">
                                                        <div class="form-group">
                                                            <label for="first-name" class="col-form-label">Description:</label>
                                                            <textarea class="form-control" name="long_description" rows="6" cols="78 ">{{$event->short_description}}</textarea>

                                                            @if ($errors->has('long_description'))
                                                                <span class="help-block">
                                            <strong>{{ $errors->first('long_description') }}</strong>
                                        </span>
                                                            @endif
                                                        </div>
                                                    </div><!-- close .col -->

                                                </div>





                                                <div class="clearfix"></div>
                                                <p><button type="submit" class="btn btn-green-pro btn-block">Submit</button></p>


                                            </div>

                                        </div>

                                </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- close .dashboard-container -->


        </main>

    </div>

    @include('User::frontOffice.store.modals.createStore')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#productImage').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInput").change(function() {
            readURL(this);
        });
    </script>

@endsection
