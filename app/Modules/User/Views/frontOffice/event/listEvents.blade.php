@extends('frontOffice.layout')

@section('content')

    <style>
        .btn {
            margin-top: 10px;
        }
        .card-counter{
            box-shadow: 2px 2px 10px #DADADA;
            margin: 5px;
            padding: 20px 10px;
            background-color: #fff;
            height: 100px;
            border-radius: 5px;
            transition: .3s linear all;
        }

        .card-counter:hover{
            box-shadow: 4px 4px 20px #DADADA;
            transition: .3s linear all;
        }

        .card-counter.primary{
            background-color: #007bff;
            color: #FFF;
        }

        .card-counter.danger{
            background-color: #ef5350;
            color: #FFF;
        }

        .card-counter.success{
            background-color: #66bb6a;
            color: #FFF;
        }

        .card-counter.info{
            background-color: #26c6da;
            color: #FFF;
        }

        .card-counter i{
            font-size: 5em;
            opacity: 0.2;
        }

        .card-counter .count-numbers{
            position: absolute;
            right: 35px;
            top: 20px;
            font-size: 32px;
            display: block;
        }

        .card-counter .count-name{
            position: absolute;
            right: 35px;
            top: 65px;
            font-style: italic;
            text-transform: capitalize;
            opacity: 0.5;
            display: block;
            font-size: 18px;
        }
        .activeTab {
            color: white;
            background-color: #02536B;
        }
    </style>


    <div id="sidebar-bg">
        @include('frontOffice.inc.sidebar')
        <main id="col-main">

            <div class="dashboard-container">


                <ul class="dashboard-sub-menu">
                  <li><a href="{{route('showProfile')}}">Account Settings</a></li>
                  <li><a href="{{route('showHealerInfos')}}">My Info</a></li>
                  <li><a href="{{route('showSessions')}}">My Sessions</a></li>
                  <li  ><a href="{{route('showUserStore')}}">My Store</a></li>
                  <li class="current"><a href="{{route('showUserEvent')}}">My Events</a></li>
                  <li><a href="{{route('showUserCalander')}}">My Calander</a></li>
                  <li><a href="{{route('showUserChannel')}}">My Channel</a></li>
                </ul><!-- close .dashboard-sub-menu -->
                <div class="container-fluid">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ Session::get('message') }}</strong>
                        </div>
                    @endif
                    <div style="border-top: 1px solid rgba(0,0,0, 0.09);" class="row">

                        <div style="margin-top: 15px" class="col-md-2">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                              <a class="btn "  href="{{route('showDashboard')}}" >Dashboard</a>
                              <a class="btn"  href="{{route('handleAddEvent')}}" >Add New Event</a>

                              <a class="btn activeTab"  href="{{route('showListEvents')}}" >Event List</a>
                              <a class="btn"  href="{{route('showListBookings')}}">Bookings</a>
                            </div>

                        </div>
                        <div style="text-align: center; margin-top: 25px" class="col-md-10">
                            <div class="card">
                                <div class="card-body">
                                    <table id="mytable" class="table table-bordred table-striped">

                                        <thead>

                                        <th>Title</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Tags</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                        </thead>
                                        <tbody>
                                        @foreach($events as $event)
                                            <tr>
                                                <td>{{$event->title}}</td>
                                                <td>{{$event->start_date->format('Y/m/d')}}</td>
                                                <td>{{$event->end_date->format('Y/m/d')}}</td>
                                                <td>@foreach($event->tags as $tag) <span class="badge badge-secondary">{{$tag->name}}</span> @endforeach</td>
                                                <td><p data-placement="top" data-toggle="tooltip" title="Edit"><a href="{{route('showUpdateEvent',$event->id )}}" ><span class="fa fa-pencil"></span></a></p></td>
                                                <td><p data-placement="top" data-toggle="tooltip" title="Delete"><a  href="{{route('hundleDeleteEvent',$event->id)}}" ><span class="fas fa-trash"></span></a></p></td>
                                            </tr>
                                        @endforeach
                                        </tbody>

                                    </table>
                                    <div>
                                        {{ $events->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- close .dashboard-container -->


        </main>

    </div>

    @include('User::frontOffice.store.modals.createStore')

@endsection
