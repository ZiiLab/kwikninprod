@extends('frontOffice.layout')

@section('content')

    <style>
        .hovereffect {
            width: 100%;
            height: 100%;
            float: left;
            overflow: hidden;
            position: relative;
            text-align: center;
            cursor: default;
        }

        .hovereffect .overlay {
            position: absolute;
            overflow: hidden;
            width: 80%;
            height: 80%;
            left: 10%;
            top: 10%;
            border-bottom: 1px solid #FFF;
            border-top: 1px solid #FFF;
            -webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
            transition: opacity 0.35s, transform 0.35s;
            -webkit-transform: scale(0,1);
            -ms-transform: scale(0,1);
            transform: scale(0,1);
        }

        .hovereffect:hover .overlay {
            opacity: 1;
            filter: alpha(opacity=100);
            -webkit-transform: scale(1);
            -ms-transform: scale(1);
            transform: scale(1);
        }

        .hovereffect img {
            display: block;
            position: relative;
            -webkit-transition: all 0.35s;
            transition: all 0.35s;
        }

        .hovereffect:hover img {
            filter: url('data:image/svg+xml;charset=utf-8,<svg xmlns="http://www.w3.org/2000/svg"><filter id="filter"><feComponentTransfer color-interpolation-filters="sRGB"><feFuncR type="linear" slope="0.6" /><feFuncG type="linear" slope="0.6" /><feFuncB type="linear" slope="0.6" /></feComponentTransfer></filter></svg>#filter');
            filter: brightness(0.6);
            -webkit-filter: brightness(0.6);
        }

        .hovereffect h2 {
            text-transform: uppercase;
            text-align: center;
            position: relative;
            font-size: 17px;
            background-color: transparent;
            color: #FFF;
            padding: 1em 0;
            opacity: 0;
            filter: alpha(opacity=0);
            -webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
            transition: opacity 0.35s, transform 0.35s;
            -webkit-transform: translate3d(0,-100%,0);
            transform: translate3d(0,-100%,0);
        }

        .hovereffect a, .hovereffect p {
            color: #FFF;
            padding: 1em 0;
            opacity: 0;
            filter: alpha(opacity=0);
            -webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
            transition: opacity 0.35s, transform 0.35s;
            -webkit-transform: translate3d(0,100%,0);
            transform: translate3d(0,100%,0);
        }

        .hovereffect:hover a, .hovereffect:hover p, .hovereffect:hover h2 {
            opacity: 1;
            filter: alpha(opacity=100);
            -webkit-transform: translate3d(0,0,0);
            transform: translate3d(0,0,0);
        }

        .btn {
          color: white;
          background: #02536B;
        }

    </style>
    <div id="sidebar-bg">
        @include('frontOffice.inc.sidebar')

        <div id="content-sidebar-pro">

            <div id="content-sidebar-info">
                <img src=" @if (!is_null($user->image)) {{asset($user->image)}} @else {{asset('frontOffice/images/unknown.png')}} @endif


                                  "  alt="{{$user->first_name}}">
                <div id="profile-sidebar-name">
                    <h5>{{$user->first_name}} {{$user->last_name}}</h5>
                </div>
                <div id="profile-sidebar-gradient"></div>

                @current($user->id)
                    <a href="{{route('showProfile')}}" class="edit-profile-sidebar"><i class="fas fa-pencil-alt"></i></a>
                @endcurrent

            </div>




            <div class="content-sidebar-section">
                <ul id="profile-watched-stats">
                    <li id="followers"><span>{{count($user->streamer->streams)}}</span> Sessions</li>
                    <li><span>{{count($user->store->products())}}</span> Products</li>
                    <li><span>{{count($user->events)}}</span> Events</li>
                </ul>
            </div><!-- close .content-sidebar-section -->
            <div class="content-sidebar-section">
                <ul id="profile-watched-stats">
                    <li><span><a href="#" data-toggle="modal" data-target="#bookPrivateSession" class="btn">Book Private Session</a></span></li>

                  </ul>
            </div><!-- close .content-sidebar-section -->


        </div><!-- close #content-sidebar-pro -->
        <main style="margin-bottom: 50px" id="col-main-with-sidebar">

            <div  class="dashboard-container">

                <ul class="dashboard-sub-menu">
                    <li class="current"><a href="{{route('showPublicInfos',$user->slug)}}">Personelle Information</a></li>
                    <li ><a href="{{route('showPublicProfile',$user->slug)}}">Store</a></li>
                    <li ><a href="{{route('showPublicEvents',$user->slug)}}">Events</a></li>
                    <li ><a href="{{route('showPublicChannel',$user->slug)}}">Channel</a></li>

                </ul><!-- close .dashboard-sub-menu -->
                @if(Session::has('message'))
                    <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ Session::get('message') }}</strong>
                    </div>
                @endif

                <div style="margin-bottom: 25px" class="row">
                   <div class="col-md-12">
                       <div class="card">

                           <div class="card-body">
                                <div class="row">

                                    <p>
                                       {{$user->additionalInfos ? $user->additionalInfos->bio : 'This user has no bio.'}}
                                    </p>
                                </div>

                           </div>
                       </div>
                   </div>
                                  </div>

                {{--@if(count($user->educations) == 0)--}}

                    {{--<p>No</p>--}}
                    {{--@else--}}
                {{--<p>Has</p>--}}

                    {{--@endif--}}



                @if(count($user->certifications) == 0)
                    <div style="margin-top: 25px;height: 280px;" class="row">
                        <div class="col-md-12">
                            <div class="card">

                                <div class="card-body">
                                    <div class="row">
                                        <p>
                                            <div class="25">This user has no registered certifications.
                                            </div>
                                        </p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                @else

                   <div class="row">
                       <div class="col-md-12">
                           <div class="card">
                               <div class="card-body">
                       @foreach($user->certifications as $certification)

                       <div class="col-lg-4 col-md-6 col-sm-8 col-xs-10">
                        <div class="hovereffect">
                            <img style="width: 250px!important;height: 200px!important;" class="img-responsive" src="{{asset($certification->asset)}}" alt="">
                            <div class="overlay">
                                <h2>{{$certification->title}}</h2>
                                <p>
                                    <a data-titre="{{$certification->title}}" data-desc="{{$certification->description}}" data-inst="{{$certification->institution}}" data-src="{{asset($certification->asset)}}" class="openModal" data-toggle="modal" data-target="#certif" href="#">Details</a>
                                </p>
                            </div>
                            </div>
                            </div>
                        </div>
                        </div>


                    </div>

                    @endforeach
                   </div>
                @endif




            </div><!-- close .dashboard-container -->
        </main>
    </div>
    @include('User::frontOffice.modals.certificateInfosModal')
    @include('User::frontOffice.modals.bookPrivateSession')
    @include('User::frontOffice.modals.book')
    <script>
        $(document).on("click", ".openModal", function () {
            $("#titre").text( $(this).data('titre') );
            $("#desc").text(  $(this).data('desc') );
            $("#inst").text(  $(this).data('inst') );
            $('#src').attr('src',$(this).data('src'));

        });
    </script>
@endsection
