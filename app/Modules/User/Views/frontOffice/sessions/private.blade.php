@extends('frontOffice.layout')

@section('content')

    <style>
        .btn {
          margin-top: 10px;
          color: white;
          background: #02536B;
        }
        .card-counter{
            box-shadow: 2px 2px 10px #DADADA;
            margin: 5px;
            padding: 20px 10px;
            background-color: #fff;
            height: 100px;
            border-radius: 5px;
            transition: .3s linear all;
        }

        .card-counter:hover{
            box-shadow: 4px 4px 20px #DADADA;
            transition: .3s linear all;
        }

        .card-counter.primary{
            background-color: #007bff;
            color: #FFF;
        }

        .card-counter.danger{
            background-color: #ef5350;
            color: #FFF;
        }

        .card-counter.success{
            background-color: #66bb6a;
            color: #FFF;
        }

        .card-counter.info{
            background-color: #26c6da;
            color: #FFF;
        }

        .card-counter i{
            font-size: 5em;
            opacity: 0.2;
        }

        .card-counter .count-numbers{
            position: absolute;
            right: 35px;
            top: 20px;
            font-size: 32px;
            display: block;
        }

        .card-counter .count-name{
            position: absolute;
            right: 35px;
            top: 65px;
            font-style: italic;
            text-transform: capitalize;
            opacity: 0.5;
            display: block;
            font-size: 18px;
        }
        .activeTab {
            color: white;
            background-color: #02536B;
        }
    </style>


    <div id="sidebar-bg">
        @include('frontOffice.inc.sidebar')
        <main id="col-main">

            <div class="dashboard-container">


                <ul class="dashboard-sub-menu">
                    <li><a href="{{route('showProfile')}}">Account Settings</a></li>
                    <li><a href="{{route('showHealerInfos')}}">My Info</a></li>
                    <li class="current"><a href="{{route('showSessions')}}">My Sessions</a></li>
                    <li  ><a href="{{route('showUserStore')}}">My Store</a></li>
                    <li><a href="{{route('showUserEvent')}}">My Events</a></li>
                    <li><a href="{{route('showUserCalander')}}">My Calander</a></li>
                    <li><a href="{{route('showUserChannel')}}">My Channel</a></li>
                </ul><!-- close .dashboard-sub-menu -->
                <div class="container-fluid">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ Session::get('message') }}</strong>
                        </div>
                    @endif

                    <div  class="row">
                        <ul style="display: flex;" >
                            <li style="margin-right: 40px"><a href="{{route('showSessions')}}">My Sessions</a></li>
                            <li style="margin-right: 40px;"><a href="{{route('showParticipations')}}">My Participations</a></li>
                            <li><a  style="color: gray;margin-right: 40px;" href="{{route('showPrivateSessions')}}">My Private Sessions</a></li>
                            <li><a href="{{ route('showPrivateParticipations') }}">My Private Participations</a></li>

                        </ul>
                    </div>
                    <div  class="row">
                    <div  class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <table id="mytable" class="table table-bordred table-striped">

                                    <thead>

                                    <th>Title</th>
                                    <th>Category</th>
                                    <th>Participiant</th>
                                    <th>Starts In</th>
                                    </thead>
                                    <tbody>
                                        @foreach($sessionEvents as $session)
                                        <tr>
                                            <td>{{$session->title}}</td>
                                            <td>{{ $session->category->label }}</td>
                                            <td><a target="_blank" href={{ route('showPublicProfile', $session->customer->slug ) }}>{{$session->customer->getFullName()}}</a></td>
                                            <td style="width: 130px" class="ct"><p  data-time="{{$session->start_date->format('D M d Y H:i:s')}}" data-id="{{$session->id}}" data-title="{{$session->title}}" ></p></td>
                                        </tr>
                                    @endforeach

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>

                    </div>
                </div>

            </div><!-- close .dashboard-container -->


        </main>

    </div>

    <script>

        function countDown(startDate,elm,sId,title) {
            console.log(startDate);
            console.log(elm);
            var countDownDate = new Date(startDate).getTime();
            var x = setInterval(function() {

                var now = new Date().getTime();

                var distance = countDownDate - now;

                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                elm[0].innerHTML = days + "d " + hours + "h "
                    + minutes + "m " + seconds + "s ";
                if (distance < 0) {
                    clearInterval(x);
                    elm[0].innerHTML = '<a class="btn btn-block" style="margin-top: 0px!important" href="{{ route('handleStartSession')}}?roomName='+title+'&sId='+ sId+'" >Start</a>';
                }
            }, 1000);
        }

        $("tr td.ct > p").each(function() {
            var elm = $(this);
           countDown($(this).data('time'),elm,$(this).data('id'),$(this).data('title'));

        });
    </script>

@endsection
