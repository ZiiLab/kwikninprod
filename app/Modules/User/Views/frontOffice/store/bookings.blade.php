@extends('frontOffice.layout')

@section('content')

    <style>
        .btn {
            margin-top: 10px;
        }
        .card-counter{
            box-shadow: 2px 2px 10px #DADADA;
            margin: 5px;
            padding: 20px 10px;
            background-color: #fff;
            height: 100px;
            border-radius: 5px;
            transition: .3s linear all;
        }

        .card-counter:hover{
            box-shadow: 4px 4px 20px #DADADA;
            transition: .3s linear all;
        }

        .card-counter.primary{
            background-color: #007bff;
            color: #FFF;
        }

        .card-counter.danger{
            background-color: #ef5350;
            color: #FFF;
        }

        .card-counter.success{
            background-color: #66bb6a;
            color: #FFF;
        }

        .card-counter.info{
            background-color: #26c6da;
            color: #FFF;
        }

        .card-counter i{
            font-size: 5em;
            opacity: 0.2;
        }

        .card-counter .count-numbers{
            position: absolute;
            right: 35px;
            top: 20px;
            font-size: 32px;
            display: block;
        }

        .card-counter .count-name{
            position: absolute;
            right: 35px;
            top: 65px;
            font-style: italic;
            text-transform: capitalize;
            opacity: 0.5;
            display: block;
            font-size: 18px;
        }
        .activeTab {
            color: white;
            background-color: #02536B;
        }
    </style>


    <div id="sidebar-bg">
        @include('frontOffice.inc.sidebar')
        <main id="col-main">

            <div class="dashboard-container">


                <ul class="dashboard-sub-menu">
                  <li><a href="{{route('showProfile')}}">Account Settings</a></li>
                  <li><a href="{{route('showHealerInfos')}}">My Info</a></li>
                  <li><a href="{{route('showSessions')}}">My Sessions</a></li>
                  <li  class="current"><a href="{{route('showUserStore')}}">My Store</a></li>
                  <li><a href="{{route('showUserEvent')}}">My Events</a></li>
                  <li><a href="{{route('showUserCalander')}}">My Calander</a></li>
                  <li><a href="{{route('showUserChannel')}}">My Channel</a></li>
                </ul><!-- close .dashboard-sub-menu -->
                <div class="container-fluid">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ Session::get('message') }}</strong>
                        </div>
                    @endif
                    <div style="border-top: 1px solid rgba(0,0,0, 0.09);" class="row">

                        <div style="margin-top: 15px" class="col-md-2">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="btn "  href="{{route('showUserStore')}}" >Dashboard</a>
                                <a class="btn"  href="{{route('showAddProduct')}}" >Add New Product</a>

                                <a class="btn "  href="{{route('showProductList')}}" >Products List</a>
                                <a class="btn activeTab"  href="{{route('showUserBookings')}}">Bookings</a>
                            </div>

                        </div>
                        <div style="text-align: center; margin-top: 25px" class="col-md-10">
                            <div class="card">
                                <div class="card-body">
                                    <table id="mytable" class="table table-bordred table-striped">

                                        <thead>

                                        <th>Product</th>
                                        <th>Quantity</th>
                                        <th>Customer</th>
                                        <th>Total Price</th>
                                        <th>Transaction Date</th>
                                        </thead>
                                        <tbody>
                                            @foreach($bookings as $booking)
                                        <tr>
                                            <td>
                                              {{$booking->product->name}}
                                            </td>
                                            <td>{{$booking->quantity}}</td>
                                            <td><a href="{{route('showPublicProfile',$booking->user->slug)}}">{{$booking->user->getFullName()}}</a></td>
                                            <td>{{$booking->quantity * $booking->product->price}} $</td>
                                            <td>{{ \Carbon\Carbon::parse($booking->created_at)->diffForHumans() }}</td>

                                        </tr>
                                            @endforeach
                                        </tbody>

                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- close .dashboard-container -->


        </main>

    </div>

    @include('User::frontOffice.store.modals.createStore')
    	@include('General::store.modals.showProduct')
    <script>
          $(document).on("click", ".showProduct", function (event) {
  					event.preventDefault();
  					  var productId = $(this).data('id');
              var img = $(this).data('img');
              var name = $(this).data('name');
              var price = $(this).data('price');
              var desc = $(this).data('desc');
              $("#productImg").attr("src",img);
              $("#Pdesc").text(desc);
              $("#Pname").text(name);
              $("#Pprice").text(price +' $');

  						$('#showProduct').data('productId',productId);

  						var base_url = '{!! url('/') !!}';
  						var href = base_url+'/user/profile/paypal/'+productId;
  						$('#buyForm').attr('action',href);

  						$('#error').html(' ');
  						if ($(this).data('quantity') > 0) {
  							$("input[type='number']").attr('max', $(this).data('quantity'));


  							$('#error').html('Quantity available: '+$(this).data('quantity'));


  						}else {
  							$("input[type='number']").attr('max', 1);
  							$("input[type='number']").attr('disabled', true);
  							$('#error').html('NOT AVAILABLE');
  						}



          });
  	</script>
@endsection
