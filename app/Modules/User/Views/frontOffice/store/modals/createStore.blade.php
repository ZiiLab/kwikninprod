
<div class="modal fade" id="createStore" tabindex="-1" role="dialog" aria-labelledby="startStreamingModal" aria-hidden="true">
    <button type="button" class="close float-close-pro" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header-pro">
                <h2>Howdy {{Auth::user()->first_name}}</h2>
                <h6>Fill these informations to create your store !</h6>
            </div>
            <div class="modal-body-pro">
                <form action="{{route('handleCreateStore')}}" method="post">
                    {{csrf_field()}}
                <div class="form-group">
                    <label for="study_field" class="col-form-label"> Store Name :</label>
                    <input data-placement="right" data-toggle="tooltip" type="text" class="form-control title" name="name" id="title"  placeholder="Name here ...">
                </div>

                <div class="form-group">
                    <label for="first-name" class="col-form-label">Description :</label>
                    <textarea id="description" placeholder="Description here ..." class="form-control" name="description" rows="4" cols="78 "></textarea>


                </div>



                <button  type="submit" class="btn btn-block"> <span>Create Store !</span></button>



                </form>

            </div><!-- close .modal-body -->


        </div><!-- close .modal-content -->
    </div><!-- close .modal-dialog -->
</div><!-- close .modal -->
