
<div class="modal fade" id="editCategory" tabindex="-1" role="dialog" aria-labelledby="startStreamingModal" aria-hidden="true">
    <button type="button" class="close float-close-pro" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header-pro">
                <h6>Edit Category !</h6>
            </div>
            <div class="modal-body-pro">
                <form action="{{route('handleEditCategory')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="study_field" class="col-form-label"> Category Name :</label>
                        <input required data-placement="right" data-toggle="tooltip" type="text" class="form-control title" name="name" id="categoryName">
                        <input type="hidden" class="form-control title" name="id" id="categoryId">
                    </div>

                    <button  type="submit" class="btn btn-block"> <span>Update</span></button>



                </form>

            </div><!-- close .modal-body -->


        </div><!-- close .modal-content -->
    </div><!-- close .modal-dialog -->
</div><!-- close .modal -->
