@extends('frontOffice.layout')

@section('content')

    <style>
        .btn {
            margin-top: 10px;
        }
        .card-counter{
            box-shadow: 2px 2px 10px #DADADA;
            margin: 5px;
            padding: 20px 10px;
            background-color: #fff;
            height: 100px;
            border-radius: 5px;
            transition: .3s linear all;
        }

        .card-counter:hover{
            box-shadow: 4px 4px 20px #DADADA;
            transition: .3s linear all;
        }

        .card-counter.primary{
            background-color: #007bff;
            color: #FFF;
        }

        .card-counter.danger{
            background-color: #ef5350;
            color: #FFF;
        }

        .card-counter.success{
            background-color: #66bb6a;
            color: #FFF;
        }

        .card-counter.info{
            background-color: #26c6da;
            color: #FFF;
        }

        .card-counter i{
            font-size: 5em;
            opacity: 0.2;
        }

        .card-counter .count-numbers{
            position: absolute;
            right: 35px;
            top: 20px;
            font-size: 32px;
            display: block;
        }

        .card-counter .count-name{
            position: absolute;
            right: 35px;
            top: 65px;
            font-style: italic;
            text-transform: capitalize;
            opacity: 0.5;
            display: block;
            font-size: 18px;
        }
        .activeTab {
            color: white;
            background-color: #02536B;
        }
    </style>


    <div id="sidebar-bg">
        @include('frontOffice.inc.sidebar')
        <main id="col-main">

            <div class="dashboard-container">


                <ul class="dashboard-sub-menu">
                  <li><a href="{{route('showProfile')}}">Account Settings</a></li>
                  <li><a href="{{route('showHealerInfos')}}">My Info</a></li>
                  <li><a href="{{route('showSessions')}}">My Sessions</a></li>
                  <li  class="current"><a href="{{route('showUserStore')}}">My Store</a></li>
                  <li><a href="{{route('showUserEvent')}}">My Events</a></li>
                </ul><!-- close .dashboard-sub-menu -->
                <div class="container-fluid">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ Session::get('message') }}</strong>
                        </div>
                    @endif
                    <div style="border-top: 1px solid rgba(0,0,0, 0.09);" class="row">

                        <div style="margin-top: 15px" class="col-md-2">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="btn "  href="{{route('showUserStore')}}" >Dashboard</a>
                                <a class="btn activeTab"  href="{{route('showAddCategory')}}" >Categories List</a>
                                <a class="btn"  href="{{route('showProductList')}}" >Products List</a>
                                <a class="btn"  href="{{route('showAddProduct')}}" >Add New Product</a>
                                <a class="btn "  href="{{route('showUserBookings')}}">Bookings</a>

                            </div>

                        </div>
                        <div style="text-align: center; margin-top: 25px" class="col-md-10">
                            <div class="card">
                                <div class="card-body">

                                        <div class="row">
                                            <div class="col-md-4">

                                                <ul class="list-group">
                                                    @foreach($store->categories as $category)
                                                    <li class="list-group-item"><span class="float-left">{{$category->name}}</span>
                                                        <a style="margin-left: 10px" class="float-right" href="{{route('handleDeleteCategory', $category->id)}}"><i class="fas fa-trash"></i> </a>
                                                        <a class="float-right editCategroy" href="#"  data-toggle="modal" data-target="#editCategory" data-id="{{$category->id}}" data-name="{{$category->name}}"><i class="fas fa-edit"></i> </a>

                                                        </li>
@endforeach
                                                </ul>

                                            </div>

                                            <div class="col-md-2">
                                                <label for="study_field" class="col-form-label"> Category Name :</label>
                                            </div>


{{csrf_field()}}
                                            <div class="col-md-6">
                                                <form action="{{route('handleAddCategory')}}" method="post">
                                                    {{csrf_field()}}
                                                <input type="text" class="form-control title" name="name"  placeholder="Name here ...">
                                                <button type="submit" class="btn btn-block">Add New Category</button>
                                                </form>
                                            </div>





                                        </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- close .dashboard-container -->


        </main>

    </div>

    @include('User::frontOffice.store.modals.editCategory')


    <script>
        $(document).on("click", ".editCategroy", function () {
            var catName = $(this).data('name');
            var catId = $(this).data('id');
            $("#categoryName").val( catName );
            $("#categoryId").val( catId );

        });
    </script>
@endsection
