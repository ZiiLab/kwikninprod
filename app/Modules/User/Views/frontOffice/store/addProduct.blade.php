@extends('frontOffice.layout')

@section('content')

    <style>
        .help-block strong{
            font-size: 10px;
            color: red;
        }
        #footer-pro {
            position: initial !important;
            margin-top: 50px;

        }

        .fileContainer {
            overflow: hidden;
            position: relative;
        }

        .fileContainer [type=file] {
            cursor: inherit;
            display: block;
            font-size: 999px;
            filter: alpha(opacity=0);
            min-height: 100%;
            min-width: 100%;
            opacity: 0;
            position: absolute;
            right: 0;
            text-align: right;
            top: 0;
        }

        .btn {
            margin-top: 10px;
        }
        .card-counter{
            box-shadow: 2px 2px 10px #DADADA;
            margin: 5px;
            padding: 20px 10px;
            background-color: #fff;
            height: 100px;
            border-radius: 5px;
            transition: .3s linear all;
        }

        .card-counter:hover{
            box-shadow: 4px 4px 20px #DADADA;
            transition: .3s linear all;
        }

        .card-counter.primary{
            background-color: #007bff;
            color: #FFF;
        }

        .card-counter.danger{
            background-color: #ef5350;
            color: #FFF;
        }

        .card-counter.success{
            background-color: #66bb6a;
            color: #FFF;
        }

        .card-counter.info{
            background-color: #26c6da;
            color: #FFF;
        }

        .card-counter i{
            font-size: 5em;
            opacity: 0.2;
        }

        .card-counter .count-numbers{
            position: absolute;
            right: 35px;
            top: 20px;
            font-size: 32px;
            display: block;
        }

        .card-counter .count-name{
            position: absolute;
            right: 35px;
            top: 65px;
            font-style: italic;
            text-transform: capitalize;
            opacity: 0.5;
            display: block;
            font-size: 18px;
        }
        .activeTab {
            color: white;
            background-color: #02536B;
        }
    </style>


    <div id="sidebar-bg">
        @include('frontOffice.inc.sidebar')
        <main id="col-main">

            <div class="dashboard-container">


                <ul class="dashboard-sub-menu">
                  <li><a href="{{route('showProfile')}}">Account Settings</a></li>
                  <li><a href="{{route('showHealerInfos')}}">My Info</a></li>
                  <li><a href="{{route('showSessions')}}">My Sessions</a></li>
                  <li  class="current"><a href="{{route('showUserStore')}}">My Store</a></li>
                  <li><a href="{{route('showUserEvent')}}">My Events</a></li>
                  <li><a href="{{route('showUserCalander')}}">My Calander</a></li>
                  <li><a href="{{route('showUserChannel')}}">My Channel</a></li>
                </ul><!-- close .dashboard-sub-menu -->
                <div class="container-fluid">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ Session::get('message') }}</strong>
                        </div>
                    @endif
                    <div style="border-top: 1px solid rgba(0,0,0, 0.09);" class="row">

                        <div style="margin-top: 15px" class="col-md-2">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="btn "  href="{{route('showUserStore')}}" >Dashboard</a>
                                  <a class="btn activeTab"  href="{{route('showAddProduct')}}" >Add New Product</a>
                                <a class="btn"  href="{{route('showProductList')}}" >Products List</a>
                                <a class="btn "  href="{{route('showUserBookings')}}">Bookings</a>

                            </div>

                        </div>
                        <div style="text-align: center; margin-top: 25px" class="col-md-10">
                            <div class="card">
                                <div class="card-body">
                                    <form action="{{route('handleAddProduct')}}" method="post"  enctype="multipart/form-data" style="margin-bottom: -10px;">
                                        @csrf

                                    <div class="row">

                                        <div class="col-md-3">

                                                <div id="account-edit-photo">


      <div><img src="https://via.placeholder.com/150" id="productImage" alt="Product Image"></div>
                                                    <label class="fileContainer btn btn-green-pro">
                                                        Chhose Product Image
                                                        <input type="file" id="imgInput" name="image" />
                                                    </label>
                                                    @if ($errors->has('image'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('image') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>


                                        </div>

                                        <div class="col-md-9">


                                                <h5>Add New Product</h5>
                                                <div class="row">
                                                    <div class="col-sm">
                                                        <div class="form-group">
                                                            <label for="first-name" class="col-form-label">Name:</label>
                                                            <input type="text" value="{{old('name')}}" class="form-control" name="name">
                                                            @if ($errors->has('name'))
                                                                <span class="help-block">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div><!-- close .col -->
                                                    <div class="col-sm">
                                                        <div class="form-group">
                                                            <label for="last-name" class="col-form-label">Price:</label>
                                                            <input onkeydown="javascript: return event.keyCode == 69 ? false : true" type="number" value="{{old('price')}}" class="form-control" name="price" >
                                                            @if ($errors->has('price'))
                                                                <span class="help-block">
                                                        <strong>{{ $errors->first('price') }}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div><!-- close .col -->

                                                </div><!-- close .row -->
                                                <div class="row">
                                                    <div class="col-sm">
                                                        <div class="form-group">
                                                            <label for="last-name" class="col-form-label">Quantity:</label>
                                                            <input onkeydown="javascript: return event.keyCode == 69 ? false : true" type="number" value="{{old('qty')}}" class="form-control" name="qty" >
                                                            @if ($errors->has('qty'))
                                                                <span class="help-block">
                                                        <strong>{{ $errors->first('qty') }}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div><!-- close .col -->
                                                    <div class="col-sm">
                                                        <div class="form-group">
                                                            <label for="country" class="col-form-label">Choose Category :</label>
                                                            <select class="custom-select"  name="categoryId">
                                                                @foreach($categories as $category)
                                                                    <option value="{{$category->id}}"> {{$category->name}}</option>
                                                         @endforeach
                                                            </select>
                                                            @if ($errors->has('categoryId'))
                                                                <span class="help-block">
                                                        <strong>{{ $errors->first('categoryId') }}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div><!-- close .col -->

                                                </div>

                                                <div class="row">
                                                    <div class="col-sm">
                                                        <div class="form-group">
                                                            <label for="first-name" class="col-form-label">Description:</label>
                                                            <textarea class="form-control" name="desc" rows="3" cols="78 "></textarea>

                                                            @if ($errors->has('desc'))
                                                                <span class="help-block">
                                            <strong>{{ $errors->first('desc') }}</strong>
                                        </span>
                                                            @endif
                                                        </div>
                                                    </div><!-- close .col -->

                                                </div>





                                                <div class="clearfix"></div>
                                                    <p><button type="submit" class="btn btn-green-pro btn-block">Submit</button></p>


                                        </div>

                                    </div>

                                </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- close .dashboard-container -->


        </main>

    </div>

    @include('User::frontOffice.store.modals.createStore')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#productImage').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInput").change(function() {
            readURL(this);
        });
    </script>
@endsection
