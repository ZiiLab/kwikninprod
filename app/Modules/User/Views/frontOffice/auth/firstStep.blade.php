@extends('frontOffice.mastlayout')


@section('content')
    <style>
        .help-block {
            color: red;
            font-size: 12px;
            font-weight: 200;
        }

        .register-complete{
          width: 100%;
          float: left;
          position: relative;
          margin-bottom: 0px;
          padding-right: 20px;
        }


        ul#registration-steps-pro li {


            width:50%;


        }




        @media only screen and (min-width: 768px) and (max-width: 959px) {

            ul#registration-steps-pro li {

                width:50%;
                margin-bottom:25px;
            }


        }

        @media only screen and (max-width: 767px) {
            ul#registration-steps-pro li {

                width:100%;
                margin-bottom:20px;
            }

        }
    </style>
    <div id="content-pro">

        <div class="container">
            <div class="centered-headings-pro pricing-plans-headings">
                <h6>Find the best healers all over the world</h6>
                <h1>Sign Up And Join The Kwiknin Community !</h1>
            </div>
        </div><!-- close .container -->

        <div class="container">
        
            <div class="clearfix"></div>
        </div>

        <div id="pricing-plans-background-image">

            <div class="container">
                <div class="registration-steps-page-container">

                    <form class="registration-steps-form" method="post" >
                        {{csrf_field()}}

                        <div class="registration-social-login-container"  style="border:none;width:100%">

                          <div class="row">
                              <div class="col-sm">
                                  <div class="form-group">
                                      <label for="first-name" class="col-form-label">First Name:</label>
                                      <input type="text" class="form-control" name="first_name" id="first-name" value="{{Auth::user()->first_name}}">
                                      @if ($errors->has('first_name'))
                                          <span class="help-block">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div><!-- close .col -->
                              <div class="col-sm">
                                  <div class="form-group">
                                      <label for="last-name" class="col-form-label">Last Name:</label>
                                      <input type="text" class="form-control" name="last_name" id="last-name" value="{{Auth::user()->last_name}}">
                                      @if ($errors->has('last_name'))
                                          <span class="help-block">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div><!-- close .col -->
                              <div class="col-sm">
                                  <div class="form-group">
                                      <label for="last-name" class="col-form-label">Slug:</label>
                                      {{--<select class="custom-select">--}}
                                          {{--<option value="0">All Countries</option>--}}
                                          {{--<option value="1">Argentina</option>--}}
                                          {{--<option value="2">Australia</option>--}}
                                          {{--<option value="3">Bahamas</option>--}}
                                          {{--<option value="4">Belgium</option>--}}
                                          {{--<option value="5">Brazil</option>--}}
                                          {{--<option value="6">Canada</option>--}}
                                          {{--<option value="7">Chile</option>--}}
                                          {{--<option value="8">China</option>--}}
                                          {{--<option value="9">Denmark</option>--}}
                                          {{--<option value="10">Ecuador</option>--}}
                                          {{--<option value="11">France</option>--}}
                                          {{--<option value="12">Germany</option>--}}
                                          {{--<option value="13">Greece</option>--}}
                                          {{--<option value="14">Guatemala</option>--}}
                                          {{--<option value="15">Italy</option>--}}
                                          {{--<option value="16">Japan</option>--}}
                                          {{--<option value="17">asdfasdf</option>--}}
                                          {{--<option value="18">Korea</option>--}}
                                          {{--<option value="19">Malaysia</option>--}}
                                          {{--<option value="20">Monaco</option>--}}
                                          {{--<option value="21">Morocco</option>--}}
                                          {{--<option value="22">New Zealand</option>--}}
                                          {{--<option value="23">Panama</option>--}}
                                          {{--<option value="24">Portugal</option>--}}
                                          {{--<option value="25">Russia</option>--}}
                                          {{--<option value="26">United Kingdom</option>--}}
                                          {{--<option selected>United States</option>--}}
                                      {{--</select>--}}
                                      <input type="text" class="form-control" name="slug" id="slug" value="{{Auth::user()->slug}}">
                                      @if ($errors->has('slug'))
                                          <span class="help-block">
                                            <strong>{{ $errors->first('slug') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div><!-- close .col -->
                          </div><!-- close .row -->
                          <div class="row">
                              <div class="col-sm">
                                  <div class="form-group">
                                      <label for="country" class="col-form-label">Country :</label>
                                      <select class="custom-select" id ="country" name="country">
                                      <option value="0">All Countries</option>
                                      <option value="1">Argentina</option>
                                      <option value="2">Australia</option>
                                      <option value="3">Bahamas</option>
                                      <option value="4">Belgium</option>
                                      <option value="5">Brazil</option>
                                      <option value="6">Canada</option>
                                      <option value="7">Chile</option>
                                      <option value="8">China</option>
                                      <option value="9">Denmark</option>
                                      <option value="10">Ecuador</option>
                                      <option value="11">France</option>
                                      <option value="12">Germany</option>
                                      <option value="13">Greece</option>
                                      <option value="14">Guatemala</option>
                                      <option value="15">Italy</option>
                                      <option value="16">Japan</option>
                                      <option value="17">asdfasdf</option>
                                      <option value="18">Korea</option>
                                      <option value="19">Malaysia</option>
                                      <option value="20">Monaco</option>
                                      <option value="21">Morocco</option>
                                      <option value="22">New Zealand</option>
                                      <option value="23">Panama</option>
                                      <option value="24">Portugal</option>
                                      <option value="25">Russia</option>
                                      <option value="26">United Kingdom</option>
                                      <option selected>United States</option>
                                      </select>
                                      @if ($errors->has('country'))
                                          <span class="help-block">
                                            <strong>{{ $errors->first('country') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div><!-- close .col -->
                              <div class="col-sm">
                                  <div class="form-group">
                                      <label for="last-name" class="col-form-label">City:</label>
                                      <input type="text" class="form-control" name="city" id="city" placeholder="City"
                                          @if (!is_null(Auth::user()->address_id))
                                              value="{{Auth::user()->address->city}}"
                                          @endif>
                                      @if ($errors->has('city'))
                                          <span class="help-block">
                                            <strong>{{ $errors->first('city') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div><!-- close .col -->
                              <div class="col-sm">
                                  <div class="form-group">
                                      <label for="last-name" class="col-form-label">Zip Code:</label>
                                      <input type="text" class="form-control" name="postal_code" id="postal_code" placeholder="xxxxxx"
                                          @if (!is_null(Auth::user()->address_id))
                                              value="{{Auth::user()->address->postal_code}}"
                                          @endif>
                                      @if ($errors->has('postal_code'))
                                          <span class="help-block">
                                            <strong>{{ $errors->first('postal_code') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div><!-- close .col -->
                          </div><!-- close .row -->
                          <div class="row">
                              <div class="col-sm">
                                  <div class="form-group">
                                      <label for="e-mail" class="col-form-label">Address</label>
                                      <input type="text" class="form-control" name="address" id="address" placeholder="Address"
                                      @if (!is_null(Auth::user()->address_id))
                                          value="{{Auth::user()->address->address}}"
                                      @endif>
                                      @if ($errors->has('address'))
                                          <span class="help-block">
                                            <strong>{{ $errors->first('address') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div><!-- close .col -->


                          </div><!-- close .row -->

                        </div><!-- close .registration-social-login-container -->
                        <div class="clearfix"></div>

                        <div class="form-group last-form-group-continue">
                            <button type="submit" class="btn btn-green-pro" id="continue">Continue</button>
                        </div>

                        <div class="clearfix"></div>

                    </form>

                </div><!-- close .registration-steps-page-container -->

            </div><!-- close .container -->
        </div><!-- close #pricing-plans-background-image -->

    </div><!-- close #content-pro -->



@endsection
