@extends('frontOffice.mastlayout')


@section('content')
    <style>
        .help-block {
            color: red;
            font-size: 12px;
            font-weight: 200;
        }


        ul#registration-steps-pro li {


            width:50%;


        }




        @media only screen and (min-width: 768px) and (max-width: 959px) {

            ul#registration-steps-pro li {

                width:50%;
                margin-bottom:25px;
            }


        }

        @media only screen and (max-width: 767px) {
            ul#registration-steps-pro li {

                width:100%;
                margin-bottom:20px;
            }

        }
    </style>
    <div id="content-pro">

        <div class="container">
            <div class="centered-headings-pro pricing-plans-headings">
                <h6>Find the best healers all over the world</h6>
                <h1>Sign Up And Join The Kwiknin Community !</h1>
            </div>
        </div><!-- close .container -->

        <div class="container">
            
            <div class="clearfix"></div>
        </div>

        <div id="pricing-plans-background-image">

            <div class="container">
                <div class="registration-steps-page-container">

                    <form class="registration-steps-form" method="post" action="{{route('handleRegisterUserStepOne',$type)}}">
                        {{csrf_field()}}

                        <div class="registration-social-login-container">

                            <div class="form-group">
                                <div class="row">
                                    <div class="col">
                                        <label for="firstname" class="col-form-label">First name</label>
                                        <input value="{{old('first_name')}}" type="text" class="form-control" name="first_name" placeholder="First name">
                                        @if ($errors->has('first_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col">
                                        <label for="lastname" class="col-form-label">Last name</label>
                                        <input value="{{old('last_name')}}" type="text" class="form-control" name="last_name" placeholder="Last name">
                                        @if ($errors->has('last_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('last_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-form-label">Email</label>
                                <input value="{{old('email')}}" type="text" class="form-control" name="email" placeholder="jondoe@gmail.com">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col">
                                        <label for="password" class="col-form-label">Password</label>
                                        <input type="password" class="form-control" name="password" placeholder="&middot;&middot;&middot;&middot;&middot;&middot;">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col">
                                        <label for="confirm-password" class="col-form-label">&nbsp;</label>
                                        <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password">

                                    </div>
                                </div>
                            </div>

                        </div><!-- close .registration-social-login-container -->
                        <div class="registration-social-login-options">
                            <h6>Fill the required informations below to create your account !</h6>
                            <img src="{{ asset ('frontOffice') }}/images/kwiknin-logo.jpg" alt="">

                        </div><!-- close .registration-social-login-options -->

                        <div class="clearfix"></div>


                        <div class="clearfix"></div>
                        <div class="form-group last-form-group-continue">
                            <button type="submit" class="btn btn-green-pro" id="continue">Continue</button>
                            <span class="checkbox-remember-pro"><input type="checkbox" id="checkbox-terms" name="checkbox_terms"><label for="checkbox-terms" class="col-form-label">By clicking "Continue", you agree to our <a href="#!">Terms of Use</a> and
                              <a href="#!">Privacy Policy</a> including the use of cookies.</label></span>

                              @if ($errors->has('checkbox_terms'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('checkbox_terms') }}</strong>
                                  </span>
                              @endif
                            <div class="clearfix"></div>
                        </div>
                    </form>

                </div><!-- close .registration-steps-page-container -->

            </div><!-- close .container -->
        </div><!-- close #pricing-plans-background-image -->

    </div><!-- close #content-pro -->


    <script type="text/javascript">

    $(document).ready(function(){

      var test = $('input[type="checkbox"]').prop("checked");

      if (!test) {


        $('#continue').prop("disabled", true);

      }

      $('input[type="checkbox"]').click(function(){

          if($(this).prop("checked") == true){

              $('#continue').prop("disabled", false);

          }

          else if($(this).prop("checked") == false){

                $('#continue').prop("disabled", true);

          }

      });

  });

    </script>
@endsection
