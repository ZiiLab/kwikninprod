
@extends('frontOffice.mastlayout')


@section('content')
 <style>
        .help-block {
            color: red;
            font-size: 12px;
            font-weight: 200;
        }


        ul#registration-steps-pro li {


            width:50%;


        }




        @media only screen and (min-width: 768px) and (max-width: 959px) {

            ul#registration-steps-pro li {

                width:50%;
                margin-bottom:25px;
            }


        }

        @media only screen and (max-width: 767px) {
            ul#registration-steps-pro li {

                width:100%;
                margin-bottom:20px;
            }

        }
    </style>
<div id="content-pro">

    <div class="container">
        <div class="centered-headings-pro pricing-plans-headings">
            <h6>For one low monthly price</h6>
            <h1>Discover lightworkers all over the word</h1>
        </div>
    </div><!-- close .container -->

    <div class="container">
       <ul id="registration-steps-pro">

                <li>
                    <a >
                        <div class="registration-step-circle-icon">01</div>
                        <div class="registration-step-number">Step 1</div>
                        <h5 class="registration-step-title">Create Your Account</h5>
                    </a>
                </li>

                <li class="current-menu-item">
                    <a >
                        <div class="registration-step-circle-icon">02</div>
                        <div class="registration-step-number">Step 2</div>
                        <h5 class="registration-step-title">Done !</h5>
                    </a>
                </li>

            </ul>
        <div class="clearfix"></div>
    </div>


    <div id="pricing-plans-background-image">
        {{-- <div class="container">
                <div class="registration-step-final-padding">
                    <h2 class="registration-final-heading">Welcome You now a member in the<span> Kwiknin! </span>community</h2>

                    <div class="alert alert-primary  fade show" role="alert" style="text-align:center;">
                    <strong>The register is done ! </strong>We have sent you an activation link to your e-mail.
                      <button type="button" class="close" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-paper-plane " aria-hidden="true"></i></span>
                      </button>
                    </div>
                    <div class="registration-step-final-footer">
                        <a href="{{route('showHome')}}" class="btn btn-block btn-green-pro">Go Home</a>
                    </div>

                </div>




            </div><!-- close .registration-steps-page-container -->

        </div><!-- close .container --> --}}

        @include('User::frontOffice.modals.welcomeModal')
    </div><!-- close #pricing-plans-background-image -->

</div><!-- close #content-pro -->


<script type="text/javascript">

$(function() {
// $('#welcomeModal').modal('show');
//
// $('#dismiss').click(function functionName(e) {
//   e.preventDefault();
//   $('#welcomeModal').modal('hide');
// })
$('#welcomeModal').modal({backdrop:'static',keyboard:false, show:true});
})

</script>
@endsection
