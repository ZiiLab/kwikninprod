@extends('frontOffice.layout')

@section('css')

<link rel="stylesheet" href="{{asset('frontOffice/css/cmxform.css')}}">

@endsection
@section('content')
<style media="screen">
    .help-block strong{
      font-size: 10px;
      color: red;
    }
    .fileContainer {
        overflow: hidden;
        position: relative;
    }

    .fileContainer [type=file] {
        cursor: inherit;
        display: block;
        font-size: 999px;
        filter: alpha(opacity=0);
        min-height: 100%;
        min-width: 100%;
        opacity: 0;
        position: absolute;
        right: 0;
        text-align: right;
        top: 0;
    }
    .hide{
      display: none;
    }

  </style>
<div id="sidebar-bg">
    @include('frontOffice.inc.sidebar')
    <main id="col-main">

        <div class="dashboard-container">

            <ul class="dashboard-sub-menu">
                <li><a href="{{route('showProfile')}}">Account Settings</a></li>
                <li class="current"><a href="{{route('showHealerInfos')}}">My Info</a></li>
                <li><a href="{{route('showSessions')}}">My Sessions</a></li>
                <li  ><a href="{{route('showUserStore')}}">My Store</a></li>
                <li><a href="{{route('showUserEvent')}}">My Events</a></li>
                <li><a href="{{route('showUserCalander')}}">My Calander</a></li>
                <li><a href="{{route('showUserChannel')}}">My Channel</a></li>
            </ul><!-- close .dashboard-sub-menu -->

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12  col-md-1">

                    </div><!-- close .col -->
                    <div class="col-12">

                        @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ Session::get('message') }}</strong>
                        </div>
                        @endif


                        <form class="account-settings-form" method="post"  action = {{route('hundleUpdateHealerBioInfos')}} id="form" enctype="multipart/form-data">
                            @csrf
                            <h5>Additional Infos</h5>
                            <p class="small-paragraph-spacing">By letting us know your address, we can make our support experience much more personal.</p>
                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label for="first-name" class="col-form-label">Bio:</label>
                                        <textarea class="form-control" name="bio" rows="6" cols="78 ">@if(!empty(Auth::user()->additionalInfos)){{Auth::user()->additionalInfos->bio}}@endif</textarea>

                                        @if ($errors->has('bio'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('bio') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div><!-- close .col -->

                                <div class="col-sm">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="state" class="col-form-label">Gender :</label>
                                                <select class="custom-select" id="state" name="state">
                                                    <option selected="" value="1">Male</option>
                                                    <option value="2">Female</option>
                                                    <option value="3">Other</option>
                                                </select>
                                                @if ($errors->has('state'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('state') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">

                                        </div>
                                    </div>

                                </div><!-- close .col -->

                            </div><!-- close .row -->

                            <div class="row">



<div class="col-md-12">
    <p><button type="submit" value="healer" name="healer" class="btn btn-green-pro">Save Changes</button></p>

</div>

                              <br>

                            </div>

                                                       <h5>Certifications</h5>
                            <p class="small-paragraph-spacing">You can change the password you use for your account here.</p>

                            <div id="certificationMsg">


                            </div>
                            <div id="certifications">


                            </div>


                            <a href="#" id="add_certification" class="btn float-right"><i class="fa fa-plus"></i>Add new Certication</a>

                            <hr style="margin-top: 70px;">


                            <div class="clearfix"></div>



                            <br>
                        </form>

                    </div><!-- close .col -->

                </div><!-- close .row -->
            </div><!-- close .container-fluid -->

        </div><!-- close .dashboard-container -->
    </main>

</div>


<script type="text/javascript">

$(function() {
  var education = localStorage.getItem('education');

  console.log(education);

  $('#educations').append(education);

})

    $('#add_education').click(function(e) {
        e.preventDefault();

        $('#add_education').hide();
        var education = '<div id="education_0">';
        education += '<a href="#"  data-education = "0" class="btn float-right delete_education" style="padding: 5px;"><i style="margin-right: 0px;" class="fa fa-times-circle cross-keywrd"></i></a>';
        education += '<div class="row">';
        education += '<div class="col-sm">';
        education += '<div class="form-group">';
        education += '<label for="school" class="col-form-label">School</label>';
        education += '<input type="text" class="form-control" name="school" id="school" placeholder="School" >';
        education += '<div id = "school_"></div>';
        education += '</div>';
        education += '</div>';
        education += '<div class="col-sm">';
        education += '<div class="form-group">';
        education += '<label for="diploma" class="col-form-label">diploma</label>';
        education += '<input type="text" class="form-control" name="diploma" id="diploma" placeholder="diploma" >';
        education += '<div id = "diploma_"></div>';
        education += '</div>';
        education += '</div>';
        education += '<div class="col-sm">';
        education += '<div class="form-group">';
        education += '<label for="study_field" class="col-form-label">Study field</label>';
        education += '<input type="text" class="form-control" name="study_field" id="study_field"  placeholder="Study field">';
        education += '<div id = "diploma_"></div>';
        education += '</div>';
        education += '</div>';
        education += '</div>';
        education += '<div class="row">';
        education += '<div class="col-sm-4">';
        education += '<div class="form-group">';
        education += '<label for="start_year" class="col-form-label">Star Year</label>';
        education += '<input type="date" name="start_year"  id ="start_year" class="form-control startdate" >';
        education += '<div id = "start_year_"></div>';
        education += '</div>';
        education += '</div>';
        education += '<div class="col-sm-4">';
        education += '<div class="form-group">';
        education += '<label for="" class="col-form-label">End Year</label>';
        education += '<input type="date" name="end_year"  id ="end_year" class="form-control">';
        education += '<div id = "end_year_"></div>';
        education += '</div>';
        education += '</div>';
        education += '</div>';
        education +=  '<a href ="#" style ="margin-left:15px" class="btn btn-green-pro healer_e"  value="healer" id = "eHealer" name="healer" >Save Changes</a>'
        education += '</div>';
        $('#educations').append(education);

        // localStorage.setItem('education',education);
    })

    var i = 0;
    $('#add_certification').click(function(e) {
        i++;
        e.preventDefault();
        $('#add_certification').hide();
        var certification = '<div id = "ceritf_0">'
        certification +='<form id="data" method="post" enctype="multipart/form-data">'
        certification += '<a href="#!" data-certification = "0" class="btn float-right delete_certification" style="padding: 5px;"><i style="margin-right: 0px;" class="fa fa-times-circle cross-keywrd"></i></a>';
        certification += '<div class="row" >';
        certification += '<div class="col-sm">';
        certification += ' <div class="row">';
        certification += ' <div class="col-md-12">';
        certification += '<div class="form-group">';
        certification += ' <label for="title" class="col-form-label">Title :</label>';
        certification += ' <input type="text" id="certifTitle" class="form-control title" name="certifTitle"  placeholder="Title" >';
        certification += '<div id="title_"></div>';
        certification += '</div>';
        certification += '</div>';
        certification += '</div>';
        certification += '<div class="row">';
        certification += '<div class="col-md-12">';
        certification += '<div class="form-group">';
        certification += '<label for="institution" class="col-form-label">Institution :</label>';
        certification += '<input type="text" class="form-control" name="institution" id="institution" placeholder="Institution" >';
        certification += '</div>';
        certification += '</div>';
        certification += '</div>';
        certification += '<div class="row">';
        certification += '<div class="col-md-12">';
        certification += '<div class="form-group">';
        certification += '<label class="fileContainer btn btn-green-pro" style="width: 100%;"> Add Certif Photo <input type="file" class="form-control"  name="asset" id="asset"/></label>';
        certification += '</div>';
        certification += '</div>';
        certification += '</div>';
        certification += '</div>';
        certification += '<div class="col-sm">';
        certification += '<div class="form-group">';
        certification += '<label for="description" class="col-form-label">Description:</label>';
        certification += '<textarea name="description" id="description" class = "form-control" rows="6" cols="78 "></textarea>';
        certification += '</div>';
        certification += '</div>';
        certification += '</div>';
         // certification += '<a href ="#" style ="margin-left:15px" class="btn btn-green-pro healer_e"  value="healer" id = "cHealer" name="healer" >Save Changes</a>'
        certification += '<button  id = "test" class = "btn btn-green-pro">Save Changes</button>'
        certification += '</form>';
        certification += '</div>';
        $('#certifications').append(certification);
    })
</script>

<script type="text/javascript">
    $(document).on("click", ".delete_certification", function(e) {
        e.preventDefault();
        console.log($(this).data('certification'));
        var id = $(this).data('certification');
        $('#add_certification').show();

        $("#ceritf_" + id).remove();
    });

    $(document).on("click", ".delete_education", function(e) {
        e.preventDefault();
        console.log($(this).data('education'));
        var id = $(this).data('education');
          $('#add_education').show();
        $("#education_" + id).remove();
    });
</script>





<script type="text/javascript">

$(document).on("click", "#eHealer", function(e) {
    e.preventDefault();
    var data = {
    'school' : $('#school').val(),
    'diploma' : $('#diploma').val(),
    'study_field' : $('#study_field').val(),
    'start_year' : $('#start_year').val(),
    'end_year' : $('#end_year').val()
    }



    $.post('{{route('apiHundleUpdateHealerEducationInfos')}}',
    {
    '_token': $('meta[name=csrf-token]').attr('content'), data : data

    })
    .done(function (res) {

    console.log(res.errors);

    $('#school_').html('');
    $('#diploma_').html('');
    $('#start_year_').html('');
    $('#end_year_').html('');

    if (res.errors) {
        if (res.errors.school) {
          $('#school_').addClass('help-block').html("<strong>"+res.errors.school[0]+"</strong>")
          console.log(res.errors.school[0]);
        }

        if (res.errors.diploma) {
          $('#diploma_').addClass('help-block').html("<strong>"+res.errors.diploma[0]+"</strong>")
        }

        if (res.errors.start_year) {
          $('#start_year_').addClass('help-block').html("<strong>"+res.errors.start_year[0]+"</strong>")
        }

        if (res.errors.end_year) {
          $('#end_year_').addClass('help-block').html("<strong>"+res.errors.end_year[0]+"</strong>")
        }

    }

    if (res.status == 200) {


        $('#educationMsg').html('<div class="alert alert-success alert-block"><button type="button" class="close" data-dismiss="alert">×</button><strong>Your Education added to Your Profile</strong></div>');
        $('#school').val("");
        $('#diploma').val("");
        $('#study_field').val("");
        $('#start_year').val("");
        $('#end_year').val("");

      }
    });
});

</script>

<script type="text/javascript">
$(document).on("click", "#test", function(e) {


  $.ajaxSetup({

  headers: {

    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

  }

  });

  e.preventDefault();

 var formData = new FormData();


if ($('#certifTitle').val() !== "") {
formData.append('title', $('#certifTitle').val());
}
if ($('#description').val() !== "") {
formData.append('description', $('#description').val());
}
if ($('#institution').val() !== "") {
  formData.append('institution', $('#institution').val());
}

if ( typeof $('input[type=file]')[0].files[0] !== "undefined") {
 formData.append('asset', $('input[type=file]')[0].files[0]);
}
















  $.ajax({

     type:'POST',

     url: "{{ route('apiHundleUpdateHealerCertificationInfos')}}",

     data:formData,

     cache:false,

     contentType: false,

     processData: false,

     success:function(data){

       console.log(data);

       $('#title_').html('');

       if (data.errors) {

         $('#title_').addClass('help-block').html("<strong>"+data.errors+"</strong>")

       }

        if (data.status == 200) {


               $('#certificationMsg').html('<div class="alert alert-success alert-block"><button type="button" class="close" data-dismiss="alert">×</button><strong>Your Certification added to Your Profile</strong></div>');

               $('#certifTitle').val("");
               $('#description').val("");
               $('#asset').val("");
               $('#institution').val("");
             }

     },

     error: function(data){

         console.log(data);

     }

  });
});



</script>

@endsection
