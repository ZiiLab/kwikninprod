@extends('frontOffice.mastlayout')


@section('content')

 <style>
        .help-block {
            color: red;
            font-size: 12px;
            font-weight: 200;
        }


        ul#registration-steps-pro li {


            width:50%;


        }





        @media only screen and (min-width: 768px) and (max-width: 959px) {

            ul#registration-steps-pro li {

                width:50%;
                margin-bottom:25px;
            }


        }

        @media only screen and (max-width: 767px) {
            ul#registration-steps-pro li {

                width:100%;
                margin-bottom:20px;
            }

        }
    </style>
<div id="content-pro">

    <div class="container">
        <div class="centered-headings-pro pricing-plans-headings">
            <h6>Find the best healers all over the world</h6>
            <h1>Sign Up And Join The Kwiknin Community !</h1>
        </div>
    </div><!-- close .container -->
    <div class="container">
        <ul id="registration-steps-pro">

                <li >
                    <a >
                        <div class="registration-step-circle-icon">01</div>
                        <div class="registration-step-number">Step 1</div>
                        <h5 class="registration-step-title">Create Your Account</h5>
                    </a>
                </li>

                <li class="current-menu-item">
                    <a>
                        <div class="registration-step-circle-icon">02</div>
                        <div class="registration-step-number">Step 2</div>
                        <h5 class="registration-step-title">Billing Information</h5>
                    </a>
                </li>
                   <li>
                    <a >
                        <div class="registration-step-circle-icon">03</div>
                        <div class="registration-step-number">Step 3</div>
                        <h5 class="registration-step-title">Done !</h5>
                    </a>
                </li>

                <li>
                    <a >
                        <div class="registration-step-circle-icon">02</div>
                        <div class="registration-step-number">Step 2</div>
                        <h5 class="registration-step-title">Done !</h5>
                    </a>
                </li>

            </ul>
        <div class="clearfix"></div>
    </div>
    <div id="pricing-plans-background-image">
        <div class="container">
            <div class="registration-steps-page-container">

                <div class="registration-billing-form">
                    <div class="row">
                        <div class="col-md">

                            <div id="visaToClick" class="jumbotron jumbotron-fluid jumbotron-pro jumbotron-selected">
                                <div  class="container">
                                    <i class="checkVisa fas fa-check-circle"></i>
                                    <img src="{{asset('frontOffice/images/demo/billing-credit-card.png')}}" alt="Credit Card">
                                    <h6 class="light-weight-heading">Pay with Credit Card</h6>
                                </div>
                            </div><!-- close .jumbotron -->

                        </div><!-- close .col-md -->
                        <div class="col-md">

                            <div id="paypalToClick" class="jumbotron jumbotron-fluid jumbotron-pro">
                                <div class="container">
                                     <i class="checkPaypal"></i>
                                    <img src="{{asset('frontOffice/images/demo/billing-paypal.png')}}" alt="Credit Card">
                                    <h6 class="light-weight-heading">Pay with PayPal</h6>
                                </div>
                            </div><!-- close .jumbotron -->

                        </div><!-- close .col-md -->
                    </div><!-- close .row -->


                    <div  class="row">
                        <div class="billing-form-pro">
                            <form action="{{route('handleRegisterUserStepTwo',$type)}}" method="post">
                                 {{csrf_field()}}
                                <div id="visa">

                                    <input type="hidden" id="type" name="type" value="0">
                                     <input type="hidden" name="userId" value="{{$userId}}">
                                <div class="form-group">
                                    <label for="cardholder" class="col-form-label">Cardholder Name:</label>
                                    <input @if (isset($data)) value="{{$data['card_holder_name']}}" @endif name="card_holder_name" type="text" class="form-control" id="cardholder">
                                     @if ($errors->has('card_holder_name'))
                                    <span class="help-block">
                      <strong>{{ $errors->first('card_holder_name') }}</strong>
                  </span>
                                @endif
                                </div>

                                <div class="form-group">
                                    <label for="cardnumber" class="col-form-label">Card Number:</label>
                                    <input  @if (isset($data)) value="{{$data['card_number']}}" @endif name="card_number" type="tel" class="cc-number form-control" id="cardnumber">
                                       @if ($errors->has('card_number'))
                                    <span class="help-block">
                      <strong>{{ $errors->first('card_number') }}</strong>
                  </span>
                                @endif
                                </div>

                                <div class="row adjust-margin-top adjust-margin-bottom">
                                    <div class="col-sm">
                                        <div class="form-group">
                                            <label for="expire" class="col-form-label">Expiration Date:</label>
                                            <input @if (isset($data)) value="{{$data['exp_date']}}" @endif id="expire" class="form-control cc-exp"  name="exp_date" type="tel" placeholder="MM / YY" autocomplete="cc-exp" >
                                               @if ($errors->has('exp_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('exp_date') }}</strong>
                                    </span>
                                  @endif
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <div class="form-group">
                                            <label for="ccv" class="col-form-label">CCV:</label>
                                            <input @if (isset($data)) value="{{$data['cvv']}}" @endif name="cvv" type="tel" class="cc-cvc form-control" id="ccv" placeholder="XXX">
                                                    @if ($errors->has('cvv'))
                                    <span class="help-block">
                        <strong>{{ $errors->first('cvv') }}</strong>
                    </span>
                                @endif
                                        </div>
                                    </div>

                                    <div class="col-sm">
                                        <div class="form-group">
                                            <label for="zip" class="col-form-label">Zip:</label>
                                            <input  @if (isset($data)) value="{{$data['zip']}}" @endif name="zip" type="text" class="form-control" id="zip" placeholder="00000">
                                @if ($errors->has('zip'))
                                    <span class="help-block">
                        <strong>{{ $errors->first('zip') }}</strong>
                    </span>
                                @endif
                                        </div>
                                    </div>

                                </div><!-- close .row -->

  </div>
                                <div class="form-group">

                                    <div class="billing-plan-container">
                                    </div><!-- close .billing-plan-container -->

</div>
                                    <button type="submit" class="btn btn-green-pro">Sign Up</button>
                                    <div class="clearfix"></div>
                                </div>
                            </form>
                        </div>
                    </div><!-- close .row -->

                </div><!-- close .registration-billing-form -->

            </div><!-- close .registration-steps-page-container -->

        </div><!-- close .container -->
    </div><!-- close #pricing-plans-background-image -->

</div><!-- close #content-pro -->


<div id="test">

  <script src="{{asset('frontOffice/js/jquery.payment.js')}}"></script>

  <script>
    jQuery(function($) {
      $('[data-numeric]').payment('restrictNumeric');
      $('.cc-number').payment('formatCardNumber');
      $('.cc-exp').payment('formatCardExpiry');
      $('.cc-cvc').payment('formatCardCVC');

    });
  </script>



</div>
<script>
    $('document').ready( function() {

        $('#paypalToClick').on('click', function () {
            $('#visaToClick').removeClass('jumbotron-selected');
            $(this).addClass('jumbotron-selected');
            $('#visa').hide();

            $('#test').hide();
            $('.checkVisa').removeClass('fas fa-check-circle');
            $('#expire').removeClass('cc-exp');
            $('.checkPaypal').addClass('fas fa-check-circle');
            $('#type').val(1);
        });

         $('#visaToClick').on('click', function () {

             $('#test').show();
            $(this).addClass('jumbotron-selected');
            $('#paypalToClick').removeClass('jumbotron-selected');
            $('#visa').show();
            $('.checkVisa').addClass('fas fa-check-circle');
            $('#expire').addClass('cc-exp');
            $('.checkPaypal').removeClass('fas fa-check-circle');
            $('#type').val(0);
        });

    })





</script>


@endsection
