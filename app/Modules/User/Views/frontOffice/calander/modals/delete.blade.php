<div class="modal fade" id="deleteEvent" tabindex="-1" role="dialog" aria-labelledby="welcomeModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header-pro">
                <h2>Delete Timestamp</h2>
                <p>Do you really want to delete this timestamp ?</p>
                <form action="{{ route('handleDeleteAvailability') }}" method="post">
                  {{ csrf_field() }}
                    <input type="hidden" name="id" id="eventId" value="">
                  <div class="form-group">

                  <button type="submit" class="btn">Yes</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>

                  </div>

    </form>
            </div>


        </div><!-- close .modal-content -->
    </div><!-- close .modal-dialog -->
</div><!-- close .modal -->
