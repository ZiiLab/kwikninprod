<div class="modal fade" id="bookedEvent" tabindex="-1" role="dialog" aria-labelledby="welcomeModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div style="margin-bottom : 20px" class="modal-header-pro">
                <h2>Booked Session</h2>
                  <p>This session has been booked at the exact time placed in the calander</p>
                  <form action="{{ route('handleAcceptSession') }}" method="post">
                    {{ csrf_field() }}

                    <ul class="list-group participationsList">
                      <li class="list-group-item">
                        <span id="customerName" class="float-left"></span>
                        <a target='_blank' style="margin-left: 10px" class="float-right" id="customerLink" href="#"><i class="fas fa-external-link-alt"></i>
                        </a>
                      </li>
                      <li class="list-group-item">
                        <span id="bookedTitle" class="float-left"></span>

                      </li>

                      <li class="list-group-item">
                        <span id="bookedDesc" class="float-left"></span>

                      </li>


                    </ul>

                    <input type="hidden" name="id" id="bookedId"  value="">
                    <input type="hidden" name="bookedstarttime" id="bookedstarttime"  value="">
                    <input type="hidden" name="bookedendtime" id="bookedendtime" value="">
                    <input type="hidden" name="bookedallday" id="bookedallDay" value="">
                    <div class="form-group">
                      <button type="submit" name="accept" value="accepted" class="btn">Accept</button>
                      <button type="submit" name="refuse" value="refused" class="btn btn-danger">Refuse</button>
                    </div>

      </form>

            </div>



        </div><!-- close .modal-content -->
    </div><!-- close .modal-dialog -->
</div><!-- close .modal -->
