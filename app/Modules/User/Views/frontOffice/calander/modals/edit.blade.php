<div class="modal fade" id="editEvent" tabindex="-1" role="dialog" aria-labelledby="welcomeModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header-pro">
                <h2>Edit Event</h2>
                <p>Will you be available in this timestamp ?</p>
                <form action="{{ route('handleUpdateAvailability') }}" method="post">
                  {{ csrf_field() }}
                  <div class="form-group">
                    <label class="radio-inline">
                      <input type="radio" name="optradio" value="1" checked>Yes
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="optradio" value='0'>No
                    </label>
                  </div>
                  <input type="hidden" name="starttime" id="editstarttime"  value="">
                  <input type="hidden" name="endtime" id="editendtime" value="">
                  <input type="hidden" name="allday" id="editallDay" value="">
                  <input type="hidden" name="id" id="editeventid" value="">
                  <div class="form-group">
                  <button type="submit" class="btn">Submit</button>
                  </div>

    </form>

            </div>



        </div><!-- close .modal-content -->
    </div><!-- close .modal-dialog -->
</div><!-- close .modal -->
