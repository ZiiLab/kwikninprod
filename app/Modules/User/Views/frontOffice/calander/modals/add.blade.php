<div class="modal fade" id="addEvent" tabindex="-1" role="dialog" aria-labelledby="welcomeModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div style="margin-bottom : 20px" class="modal-header-pro">
                <h2>Set Availabilty</h2>
                  <p>Will you be available in this timestamp ?</p>
                  <form action="{{ route('handleSetAvailability') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <label class="radio-inline">
                        <input type="radio" name="optradio" value="1" checked>Yes
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="optradio" value='0'>No
                      </label>
                    </div>
                    <input type="hidden" name="starttime" id="starttime"  value="">
                    <input type="hidden" name="endtime" id="endtime" value="">
                    <input type="hidden" name="allday" id="allDay" value="">
                    <div class="form-group">
                    <button type="submit" class="btn">Submit</button>
                    </div>

      </form>

            </div>



        </div><!-- close .modal-content -->
    </div><!-- close .modal-dialog -->
</div><!-- close .modal -->
