@extends('frontOffice.layout')

@section('content')

    <style>
        .btn {
            margin-top: 10px;
        }
        .card-counter{
            box-shadow: 2px 2px 10px #DADADA;
            margin: 5px;
            padding: 20px 10px;
            background-color: #fff;
            height: 100px;
            border-radius: 5px;
            transition: .3s linear all;
        }



        .card-counter:hover{
            box-shadow: 4px 4px 20px #DADADA;
            transition: .3s linear all;
        }

        .card-counter.primary{
            background-color: #007bff;
            color: #FFF;
        }

        .card-counter.danger{
            background-color: #ef5350;
            color: #FFF;
        }

        .card-counter.success{
            background-color: #66bb6a;
            color: #FFF;
        }

        .card-counter.info{
            background-color: #26c6da;
            color: #FFF;
        }

        .card-counter i{
            font-size: 5em;
            opacity: 0.2;
        }

        .card-counter .count-numbers{
            position: absolute;
            right: 35px;
            top: 20px;
            font-size: 32px;
            display: block;
        }

        .card-counter .count-name{
            position: absolute;
            right: 35px;
            top: 65px;
            font-style: italic;
            text-transform: capitalize;
            opacity: 0.5;
            display: block;
            font-size: 18px;
        }
        .activeTab {
            color: white;
            background-color: #02536B;
        }
    </style>


    <div id="sidebar-bg">
        @include('frontOffice.inc.sidebar')
        <main id="col-main">

            <div class="dashboard-container">


                <ul class="dashboard-sub-menu">
                    <li><a href="{{route('showProfile')}}">Account Settings</a></li>
                    <li><a href="{{route('showHealerInfos')}}">My Info</a></li>
                    <li ><a href="{{route('showSessions')}}">My Sessions</a></li>
                    <li  ><a href="{{route('showUserStore')}}">My Store</a></li>
                    <li><a href="{{route('showUserEvent')}}">My Events</a></li>
                    <li class="current"><a href="{{route('showUserCalander')}}">My Calander</a></li>
                    <li><a href="{{route('showUserChannel')}}">My Channel</a></li>
                </ul><!-- close .dashboard-sub-menu -->
                <div class="container-fluid">
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ Session::get('message') }}</strong>
                        </div>
                    @endif
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>By a simple click at a timestamp in the calander, you can set your availability !</strong>
                    </div>
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>You can switch to Day or Week view for more details !</strong>
                    </div>
                    <div  class="row">
                    <div  class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                              <div class="response"></div>
                              <div id='calendar'></div>
                            </div>
                        </div>
                    </div>


                    </div>
                </div>

            </div><!-- close .dashboard-container -->


        </main>
        @include('User::frontOffice.calander.modals.add')
        @include('User::frontOffice.calander.modals.edit')
        @include('User::frontOffice.calander.modals.delete')
	      @include('User::frontOffice.calander.modals.booked')
    </div>
    <script>
      $(document).ready(function () {

            var SITEURL = "{{url('/')}}";
            $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
            });

            var calendar = $('#calendar').fullCalendar({

                editable: true,

                header: {
              left: 'prev,next today',
              center: 'title',
              right: 'month,agendaWeek,agendaDay'
          },
                events : [
                @foreach($sessionEvents as $sessionEvent)
                {
                    id : '{{ $sessionEvent->id }}',
                    title : '{{ $sessionEvent->title }}',
                    @if($sessionEvent->allDay == 1)
                      start : '{{ $sessionEvent->start_date }}',
                    allDay: true,
                    @else
                        start : '{{ $sessionEvent->start_date }}',
                        end : '{{ $sessionEvent->end_date }}',
                        allDay: false,
                    @endif

                    editable: true,
                    @if($sessionEvent->availability == 0)
                    color: 'red',
                    textColor: 'white',
                    @elseif($sessionEvent->availability == 1)
                    color: 'green',
                    textColor: 'white',
                    @else
                    color: 'yellow',
                    textColor: 'red',
                    @endif

                                  },


                @endforeach
            ],
                displayEventTime: true,
                editable: true,
                eventRender: function (event, element, view) {
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                },
                selectable: true,
                selectHelper: true,
                select: function (start, end, allDay) {

                  var starttime = moment(start).format('YYYY-MM-DD HH:mm:ss');
                  var endtime = moment(end).format('YYYY-MM-DD HH:mm:ss');
                  var allDay = !start.hasTime() && !end.hasTime();

                  $('#starttime').val(starttime);
                  $('#endtime').val(endtime);
                  $('#allDay').val(allDay);
                  $('#addEvent').modal('show');

                },

                eventDrop: function (event, delta) {

                   var editstarttime = moment(event.start).format('YYYY-MM-DD HH:mm:ss');
                   var editendtime = moment(event.end).format('YYYY-MM-DD HH:mm:ss');

                  $('#editeventid').val(event.id);
                  $('#editstarttime').val(editstarttime);
                  $('#editendtime').val(event.end);
                  $('#editallDay').val(event.allDay);
                  $('#editEvent').modal('show');
                        },
                eventClick: function (event) {
                    $.get("{{ route('apiCheckSession')}}?id=" + event.id ).done( function (res) {
                      if(res.status == 201) {
                        $('#eventId').val(event.id);
                        $('#deleteEvent').modal('show');
                      }else {
                        $('#bookedTitle').text( 'Title : ' + event.title);
                        $('#bookedId').val(event.id);
                        $('#bookedDesc').text( 'Description : ' +res.desc);
                        $('#customerName').text('Customer : ' + res.customerName);
                        var base_url = '{!! url('/') !!}'
                        var link = base_url+'/user/'+ res.slug
                        $('#customerLink').attr("href", link);
                        $('#bookedEvent').modal('show');
                      }


                    });

                },
                eventResize : function(event, delta, revertFunc) {

                  var editstarttime = moment(event.start).format('YYYY-MM-DD HH:mm:ss');
                  var editendtime = moment(event.end).format('YYYY-MM-DD HH:mm:ss');

                 $('#editeventid').val(event.id);
                 $('#editstarttime').val(editstarttime);
                 $('#editendtime').val(editendtime);
                 $('#editallDay').val(event.allDay);
                 $('#editEvent').modal('show');
                }

            });
      });

      function displayMessage(message) {
        $(".response").html("<div class='success'>"+message+"</div>");
        setInterval(function() { $(".success").fadeOut(); }, 1000);
      }
    </script>



@endsection
