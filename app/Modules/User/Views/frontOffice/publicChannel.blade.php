@extends('frontOffice.layout')

@section('content')
    <style media="screen">
        #follow img{
            width: 25px;
        }
        .btn {
          color: white;
          background: #02536B;
        }
    </style>
    <div id="sidebar-bg">

        @include('frontOffice.inc.sidebar')

        <div id="content-sidebar-pro">

            <div id="content-sidebar-info">
                <img src=" @if (!is_null($user->image)) {{asset($user->image)}} @else {{asset('frontOffice/images/unknown.png')}} @endif


                        "  alt="{{$user->first_name}}">
                <div id="profile-sidebar-name">
                    <h5>{{$user->first_name}} {{$user->last_name}}</h5>
                </div>
                <div id="profile-sidebar-gradient"></div>

                @current($user->id)
                <a href="{{route('showProfile')}}" class="edit-profile-sidebar"><i class="fas fa-pencil-alt"></i></a>
                @endcurrent

            </div>





            <div class="content-sidebar-section">
                <ul id="profile-watched-stats">
                    <li id="followers"><span>{{count($user->streamer->streams)}}</span> Sessions</li>
                    <li><span>{{count($user->store->products())}}</span> Products</li>
                    <li><span>{{count($user->events)}}</span> Events</li>

                </ul>
            </div><!-- close .content-sidebar-section -->
            <div class="content-sidebar-section">
                <ul id="profile-watched-stats">
                  <li><span><a href="#" data-toggle="modal" data-target="#bookPrivateSession" class="btn">Book Private Session</a></span></li>

                  </ul>
            </div><!-- close .content-sidebar-section -->




        </div><!-- close #content-sidebar-pro -->
        <main  id="col-main-with-sidebar">

            <div class="dashboard-container">

                <ul class="dashboard-sub-menu">
                    <li ><a href="{{route('showPublicInfos',$user->slug)}}">Personelle Information</a></li>
                    <li ><a href="{{route('showPublicProfile',$user->slug)}}">Store</a></li>
                    <li ><a href="{{route('showPublicEvents',$user->slug)}}">Events</a></li>
                    <li class="current"><a href="{{route('showPublicChannel',$user->slug)}}">Channel</a></li>

                </ul><!-- close .dashboard-sub-menu -->
                @if(Session::has('message'))
                    <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ Session::get('message') }}</strong>
                    </div>
                @endif


                <div class="row">

                    @if(count($videos) > 0)
                      <div class="col-md-12">
                          <div class="card">
                              <div class="card-body">
                                  <table id="mytable" class="table table-bordred table-striped">

                                      <thead>

                                      <th>Title</th>
                                      <th>Category</th>
                                      <th>Description</th>
                                      <th>Price</th>
                                      <th>Action</th>
                                      </thead>
                                      <tbody>
                                        @foreach($videos as $video)

                                      <tr>
                                          <td>{{$video->title}}</td>
                                          <td>{{$video->category->label}}</td>
                                          <td>{{$video->desc}}</td>
                                          <td>{{$video->price ? $video->price.' $' : 'FREE'}}</td>
                                          <td> @if(!$video->free)<a data-toggle="modal" data-target="#buyProduct" href="#" class="btn">Buy</a> @else <a href="{{ asset('storage/uploads/videos/'.$video->video) }}" download>Download</a>@endif</td>
                                      </tr>
                                          @endforeach
                                      </tbody>

                                  </table>

                              </div>
                          </div>
                      </div>

                        @else
                        <div style="height: 400px;" class="col-md-12">

                        <div class="card">

                            <div class="card-body">
                                <div class="row">

                                    <p>
                                        This user uploaded no videos yet !.
                                    </p>
                                </div>

                            </div>

                    </div>
                    </div>
                    @endif


                </div><!-- close .row -->

                <div class="row">
                    <div class="col-md-12">

                        <div>
                            {{$videos->links()}}
                        </div>
                    </div>
                </div>







            </div><!-- close .dashboard-container -->
        </main>
    </div>
    @include('General::store.modals.buyProduct')
    @include('User::frontOffice.modals.bookPrivateSession')
    @include('User::frontOffice.modals.book')

@endsection
