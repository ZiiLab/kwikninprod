<?php
namespace App\Modules\User\Controllers;

use App\Modules\User\Models\Paypal;
use App\Modules\User\Models\Transaction as UserTransaction;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use Carbon\Carbon;
use App\Modules\User\Models\User;

/** All Paypal Details class **/
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use Session;
use URL;

use App\Modules\User\Models\Product;

class PaymentController extends Controller
{
    private $_api_context;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['sandbox_client_id'],
            $paypal_conf['sandbox_secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);

    }
    public function index()
    {
        return view('paywithpaypal');
    }
    public function payWithpaypal(Request $request)
    {

        $product = Product::find($request->itemId);

        $price = $product->price;

        $bookingQuantity =  $request->quantity;

        $totalPrice      =  $request->quantity * $price ;

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item_1 = new Item();

        $item_1->setName($product->name) /** item name **/
            ->setCurrency('USD')
            ->setQuantity($bookingQuantity)
            ->setPrice($price); /** unit price **/

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal($totalPrice);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Your transaction description');

        $redirect_urls = new RedirectUrls();

        $redirect_urls->setReturnUrl(route('status')) /** Specify return URL **/
            ->setCancelUrl(route('status'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        /** dd($payment->create($this->_api_context));exit; **/
        try {

            $payment->create($this->_api_context);

        } catch (\PayPal\Exception\PPConnectionException $ex) {

            if (\Config::get('app.debug')) {

                \Session::put('error', 'Connection timeout');
                return Redirect::to('/');

            } else {

                \Session::put('error', 'Some error occur, sorry for inconvenient');
                return Redirect::to('/');

            }

        }

        foreach ($payment->getLinks() as $link) {

            if ($link->getRel() == 'approval_url') {

                $redirect_url = $link->getHref();
                break;

            }

        }

        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        Session::put('product_id', $product->id);
        Session::put('quantity', $bookingQuantity);
        if (isset($redirect_url)) {

            /** redirect to paypal **/
            return Redirect::away($redirect_url);

        }

        \Session::put('error', 'Unknown error occurred');
        return Redirect::to('/');

    }

    public function getPaymentStatus()
    {

        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        $productId = Session::get('product_id');
        $bookingQuantity = Session::get('quantity');

        $pruduct = Product::find($productId);
        $user = User::find(Auth::user()->id);

        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        Session::forget('product_id');
        Session::forget('quantity');

        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {

            \Session::put('error', 'Payment failed');
            return Redirect::to('/');

        }



        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));



        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

        $email = $user->paypals->where('email',$result->payer->payer_info->email);

        if (count($email) == 0) {
          Paypal::create([
            'email'  => $result->payer->payer_info->email,
            'adresse_line'  => $result->payer->payer_info->shipping_address->line1,
            'postal_code'  => $result->payer->payer_info->shipping_address->postal_code,
            'city'  => $result->payer->payer_info->shipping_address->city,
            'state'  => $result->payer->payer_info->shipping_address->state,
            'country_code'  => $result->payer->payer_info->shipping_address->country_code,
            'user_id'  => Auth::user()->id
          ]);
        }

        UserTransaction::create([
          'id_transaction'  => $result->payer->payer_info->email,
          'amount'  => $result->transactions[0]->amount->total,
          'currency_code'  => $result->transactions[0]->amount->currency,
          'transaction_date'  =>date('Y-m-d H:i:s', strtotime($result->create_time)),
          'user_id'  => Auth::user()->id
        ]);


          if ($result->getState() == 'approved') {
                Auth::user()->productBookings()->create([

                  'product_id' => $productId,
                  'quantity'   => $bookingQuantity

                ]);
                $pruduct->quantity -= $bookingQuantity;
                $pruduct->save();

              \Session::put('success', 'Payment success');
              return redirect()->route('showHome');

          }


          \Session::put('error', 'Payment failed');
            return redirect()->route('showHome');

    }

}
