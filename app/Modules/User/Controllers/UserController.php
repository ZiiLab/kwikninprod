<?php

namespace App\Modules\User\Controllers;

use App\Modules\Broadcasting\Models\SessionBooking;
use App\Modules\User\Models\Product;
use App\Modules\User\Models\ProductBooking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\User\Models\User;
use App\Modules\User\Models\Store;
use App\Modules\User\Models\StoreCategories;
use App\Modules\User\Models\UploadedSession;
use App\Modules\User\Models\PaymentInfo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\Modules\User\Models\Address;
use App\Modules\Broadcasting\Models\Category;
use App\Modules\Broadcasting\Models\Streamer;
use Session;
use Image;
use File;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Modules\User\Models\EventBookings;
use App\Modules\User\Models\SessionEvent;




class UserController extends Controller
{

    /**
     * @desc show 1st registring page
     * @return view
     */

    public function showRegisterUserStepOne($type)
    {
        return view('User::frontOffice.auth.registerStepOne', [
          'type' => $type]);
    }

    /**
     * @desc show 2nd registring page
     * @return view
     */
    public function showHealerInfos()
    {

        return view('User::frontOffice.auth.secondStep');
    }


    /**
     * @desc show 3rd registring page
     * @return view
     */
     public function showRegisterUserStepTwo()
     {
         if (Auth::user()->type != 0) {
             return redirect()->route('showHome');
         }

         return view('User::frontOffice.auth.registerStepTwo');
     }

    /**
     * @desc show welcoming page
     * @return view
     */

    public function showWelcomePage($type)
    {
        return view('User::frontOffice.auth.welcome', [
          'type' => $type]);
    }

    /**
     * @desc show welcoming page
     * @return view
     */

    public function showWModelVerification()
    {
        $userId = Session::get('userId');
        $user = User::find($userId);
        Session::put('userStatus', $user->status);
        return redirect()->route('showHome');
    }

    /**
     * @desc show welcoming page
     * @return view
     */

    public function showUserEvent()
    {

        $rows = [];
        $bookings = [];

        foreach(Auth::user()->events as $event) {
            if(EventBookings::where('event_id',$event->id)->first())
                $rows[] = EventBookings::where('event_id',$event->id)->get();
        }

        foreach ($rows as $row) {
            foreach ($row as $booking) {
                $bookings[] = $booking;
            }
        }

        return view('User::frontOffice.event.index',
            [
                'bookings' => $bookings
            ]);

    }

    /**
     * @desc show welcoming page
     * @return view
     */
    public function showUserStore()
    {
        $rows = [];
        $bookings = [];

      if(Auth::user()->store->products()) {
          foreach(Auth::user()->store->products() as $product) {
              if(ProductBooking::where('product_id',$product->id)->first())
                  $rows[] = ProductBooking::where('product_id',$product->id)->get();
          }

          foreach ($rows as $row) {
              foreach ($row as $booking) {
                  $bookings[] = $booking;
              }
          }
      }
        return view('User::frontOffice.store.index', [
            'store' => Auth::user()->store,
            'bookings' => $bookings,
        ]);
    }

    public function showUserBookings(Request $request)
    {
        $rows = [];
        $bookings = [];
          if(Auth::user()->store->products) {
        foreach(Auth::user()->store->products as $product) {
            if(ProductBooking::where('product_id',$product->id)->first())
                $rows[] = ProductBooking::where('product_id',$product->id)->get();
        }

        foreach ($rows as $row) {
            foreach ($row as $booking) {
                $bookings[] = $booking;
            }
        }
}
        return view('User::frontOffice.store.bookings', [
            'bookings' =>$bookings,

        ]);
    }


    public function handleCreateStore() {

        $data = Input::all();

        Store::create([
            'name' => $data['name'],
            'description' => $data['description'] ? $data['description'] :  null,
            'user_id' => Auth::id()
        ]);

        Session::flash('message', 'Store Created !');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('showUserStore');

    }

    public function showAddCategory() {
        return view('User::frontOffice.store.addCategory',[
            'store' => Auth::user()->store
        ]);
    }

    public function handleAddCategory() {
        $data = Input::all();

        StoreCategories::create([
            'name' => $data['name'],
            'store_id' => Auth::user()->store->id
        ]);

        Session::flash('message', 'Category Created !');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('showAddCategory');
    }

    public function handleDeleteCategory($id) {
       $category =  StoreCategories::find($id);
       $category->delete();
        Session::flash('message', 'Category Deleted !');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('showAddCategory');
    }

    public function showAddProduct() {
        return view('User::frontOffice.store.addProduct',[
            'categories' => StoreCategories::all()
        ]);
    }

    public function handleAddProduct() {
        $data = Input::all();

        $rules = [
            'name' => 'required',
            'price' => 'required|numeric',
            'qty' => 'required',
            'image' => 'required',
        ];

        $messages = [
            'name.required' => 'Product Name Is Required',
            'price.required' => 'Product Price Is Required',
            'price.numeric' => 'Product Price Is Invalid',
            'qty.required' => 'PLease Specify A Quantity',
            'image.required' => 'Product Image Is Required'
        ];

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }

        $image =null;

        if (isset($data['image'])) {
            $img = 'product-' . str_random(5) . time() . '.' . $data['image']->getClientOriginalExtension();
            $fullImagePath = public_path('storage/uploads/users/store/products/' . $img);
            Image::make($data['image']->getRealPath())->save($fullImagePath);
            $image = 'storage/uploads/users/store/products/' . $img;
        }

        $productData = [
            'name' => $data['name'],
            'price' => $data['price'],
            'quantity' => $data['qty'],
            'description' => $data['desc'] ? $data['desc'] : null,
            'image' => $image,
            'category_id' => $data['categoryId'],
            'store_id' => Auth::user()->store->id
        ];

        Product::create($productData);

        Session::flash('message', 'Product Created !');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('showProductList');

    }

    public function showProductList(Request $request) {

        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        $itemCollection = collect(Auth::user()->store->products);

        $perPage = 5;

        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();

        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);

        $paginatedItems->setPath($request->url());

        return view('User::frontOffice.store.productList', [
            'products' => $paginatedItems
        ]);
    }

    public function handleDeleteProduct($id) {
        $product =  Product::find($id);
        $product->delete();
        Session::flash('message', 'Product Deleted !');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('showProductList');
    }

    public function showUpdateProduct($id) {
        return view('User::frontOffice.store.updateProduct', [
            'product' => Product::find($id),
            'categories' => Auth::user()->store->categories
        ]);
    }

    public function handleUpdateProduct($id) {
        $data = Input::all();

        $product = Product::find($id);

        $rules = [
            'name' => 'required',
            'price' => 'required|numeric',
            'qty' => 'required'
        ];

        $messages = [
            'name.required' => 'Product Name Is Required',
            'price.required' => 'Product Price Is Required',
            'price.numeric' => 'Product Price Is Invalid',
            'qty.required' => 'PLease Specify A Quantity'
        ];

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }
        $image = null;
        if (isset($data['image'])) {
            $img = 'product-' . str_random(5) . time() . '.' . $data['image']->getClientOriginalExtension();
            $fullImagePath = public_path('storage/uploads/users/store/products/' . $img);
            Image::make($data['image']->getRealPath())->save($fullImagePath);
            $image = 'storage/uploads/users/store/products/' . $img;
        }

        $productData = [
            'name' => $data['name'],
            'price' => $data['price'],
            'quantity' => $data['qty'],
            'description' => $data['desc'] ? $data['desc'] : $product->description,
            'image' => $image ? $image : $product->image,
            'category_id' => $data['categoryId']
        ];

        $product->update($productData);

        Session::flash('message', 'Product Updated !');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('showProductList');
    }

    public function handleEditCategory() {
        $data = Input::all();

        $category = StoreCategories::find($data['id']);

        $category->update([
            'name' => $data['name']
        ]);

        Session::flash('message', 'category Updated !');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('showAddCategory');
    }



    /**
     * @desc handle regestring new user 1st step
     * @param  Request
     * @return Response
     */
    public function handleRegisterUserStepOne($type)
    {
        $data = Input::all();


        $rules = [
           'first_name' => 'required',
           'last_name' => 'required',
           'email' => 'required|email|unique:users,email',
           'password' => 'required|min:6|confirmed',
           'checkbox_terms' => 'required'
       ];

        $messages = [
           'first_name.required'     => 'First name is required !',
           'last_name.required'      => 'Last name is required !',
           'email.required'          => 'Email is required !',
           'email.email'             => 'Invalid email !',
           'email.unique'            => 'Email already in use !',
           'password.required'       => 'Password is required !',
           'password.min'            => 'Password must contain at least 6 charecters !',
           'password.confirmed'      => 'Password must be confirmed !',
           'checkbox_terms.required' => 'You must agree to the latest Terms and Conditons in order to use this service'
       ];

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation->errors());
        }

        $email = $data['email'];

        $pinValidate = mt_rand(100000, 999999);
        $validation = str_random(30);


        $user = User::create([
           'pin'           => $pinValidate,
           'email'         => $email,
           'password'      => bcrypt($data['password']),
           'status'        => 0,
           'progress'      => 0,
           'first_name'    => $data['first_name'],
           'last_name'     => $data['last_name'],
           'type'          => $this->getUserType($type),
           'validation'    => $validation
       ]);

       if($type == 0){
         $user->streamer()->create([]);
       }

        $content = ['email' => $email, 'validation' => $validation, 'pin' => $pinValidate];
        Mail::send('User::frontOffice.mail.activation', $content, function ($message) use ($email) {
            $message->to($email);
            $message->subject('Account activation');
        });

        Session::put('userId', $user->id);
        return view('User::frontOffice.auth.welcome');
    }

    /**
     * @desc handle regestring new user 2nd step
     * @param  Request
     * @return Response
     */
    public function handleRegisterUserStepTwo()
    {
        $data = Input::all();

        $data['card_number'] = str_replace(' ', '', $data['card_number']);
        $today = Carbon::now()->format('m/Y');

        $user = User::find(Auth::user()->id);
        $email = $user->email;
        $validationString = str_random(30);
        $user->validation = $validationString;
        $user->save();
        $rules = [
           'card_holder_name' => 'required',
           'card_number' => ['required','numeric'],
           'exp_date' => 'required|date_format:m/Y|after_or_equal:'.$today,
           'cvv' => 'required|min:3|max:3',
           'zip' => 'required',
       ];

        $messages = [
           'card_holder_name.required'       => 'Cardholder name is required !',
           'card_number.required'            => 'Card number is required !',
           'card_number.number'              => 'Card number is invalid 5 !',
           'exp_date.required'               => 'Expiration date is required !',
           'exp_date.date_format'            => 'Expiration date is invalid !',
           'exp_date.after_or_equal'         => 'Expiration date is invalid !',
           'cvv.required'                    => 'CVV is required !',
           'cvv.min'                         => 'CVV must be 3 characters !',
           'cvv.max'                         => 'CVV must be 3 characters !',
           'zip.required'                    => 'Zip is required !'
       ];

     switch ($data['card_holder_name']) {
        case 'visa':
          array_push($rules['card_number'],'regex:/^4[0-9]{12}(?:[0-9]{3})?$/') ;
          array_push($rules['card_number'],'digits_between:16,16') ;
          $messages['card_number.digits_between'] = 'Card number must be 16 numbers !' ;

        break;
        case 'mastercard':
          array_push($rules['card_number'],'regex:/^5[1-5][0-9]{14}$|^2(?:2(?:2[1-9]|[3-9][0-9])|[3-6][0-9][0-9]|7(?:[01][0-9]|20))[0-9]{12}$/') ;
          array_push($rules['card_number'],'digits_between:16,16') ;
          $messages['card_number.digits_between'] = 'Card number must be 16 numbers !' ;

        break;
        case 'amex':
          array_push($rules['card_number'],'regex:/^3[47][0-9]{13}$/') ;
          array_push($rules['card_number'],'digits_between:15,15') ;
          $messages['card_number.digits_between'] = 'Card number must be 15 numbers !' ;

        break;
        case 'discover':
          array_push($rules['card_number'],'regex:/^65[4-9][0-9]{13}|64[4-9][0-9]{13}|6011[0-9]{12}|(622(?:12[6-9]|1[3-9][0-9]|[2-8][0-9][0-9]|9[01][0-9]|92[0-5])[0-9]{10})$/') ;
          array_push($rules['card_number'],'digits_between:16,16') ;
          $messages['card_number.digits_between'] = 'Card number must be 16 numbers !' ;

        break;

       }

        $validation = Validator::make($data, $rules, $messages);


        if ($validation->fails()) {
            return view('User::frontOffice.paymentInfo', [
                'data'   => $data
              ])->withErrors($validation->errors());
        }

        $dateMonthArray = explode('/', $data['exp_date']);
        $month = $dateMonthArray[0];
        $year = $dateMonthArray[1];

        $date = Carbon::createFromDate($year, $month, 1);

        $paymentData = [
               'card_holder_name' => $data['card_holder_name'],
               'card_number' => $data['card_number'],
               'exp_date' => $date,
               'cvv' => $data['cvv'],
               'zip' => $data['zip'],
               'user_id' => Auth::user()->id
           ];

        PaymentInfo::create($paymentData);


        \Session::put('success', 'Payment success');
        return redirect()->route('showPaymentInfos');

    }

    /**
     * @desc Verify the user email
     * @param  Code
     * @return Response
     */
    public function handleUserMailValidation($code)
    {
        Session::flush();

        $user = User::where('validation', '=', $code)->first();

        if ($user) {
            if ($user->status == 1) {
                // toastr()->info('You are already amoung us :D','Yow !');
                return redirect(route('showHome'));
            }

            Auth::login($user);
            $user->status = 1;
            $user->slug = mt_rand(100000, 999999).$user->id;
            $user->save();
            //  toastr()->success('Your account has been verified, you are one of us now :)', 'Nice !');

            $content = ['email' => $user->email];

            Mail::send('User::frontOffice.mail.welcome', $content, function ($message) use ($user) {
                $message->to($user->email);
                $message->subject('Welcome to Kwiknin');
            });
            return redirect(route('showProfile'));
        } else {

           // toastr()->warning('User not found', 'Oups !');

            return redirect(route('showHome'));
        }
    }

    /**
     * @desc handle login the user
     * @param  Request
     * @return Response
     */
    public function apiHandleUserLogin(Request $request)
    {

        $credentials = [
            'email' => $request->data['email'],
            'password' => $request->data['password'],
        ];

        if (Auth::attempt($credentials)) {
            $user= Auth::user();

            if ($user->status === 0) {
                Auth::logout();
                return response()->json(['status' => 401]); // acount not activated
            } elseif ($user->status === 1) {
                Auth::login($user);

                return response()->json(['status' => 200,'url' => route('showProfile')]);
            } else {
                Auth::logout();
                return response()->json(['status' => 400]); // something went wrong, bad request
            }
        }

        return response()->json(['status' => 402]); // wrong email or password
    }


    public function apiHandleUserCodeVlidation(Request $request)
    {
        $pin = $request->data['pin'];

        $user = User::where('pin', '=', $pin)->first();

        if ($user) {
            if ($user->status == 1) {
                return response()->json(['status' => 401]); //account your account actived
            }
            Session::flush();
            Auth::login($user);
            $user->status = 1;
            $user->slug = mt_rand(100000, 999999).$user->id;
            $user->save();
            $content = ['email' => $user->email];

            Mail::send('User::frontOffice.mail.welcome', $content, function ($message) use ($user) {
                $message->to($user->email);
                $message->subject('Welcome to Kwiknin');
            });
            return response()->json(['status' => 200,'url' => route('showProfile')]);
        } else {

         // toastr()->warning('User not found', 'Oups !');

            return response()->json(['status' => 402]); // wrong pin
        }
    }

    public function handleLogout()
    {
        Auth::logout();
        return redirect(route('showHome'));
    }


    public function showPublicProfile($slug, Request $request)
    {
        $user = User::where('slug', $slug)->first();
        if (!$user) {
            return redirect(route('showHome')); // not found
        }

        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        $itemCollection = collect($user->store->products);

        $perPage = 3;

        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();

        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);

        $paginatedItems->setPath($request->url());

        return view('User::frontOffice.publicProfile', [
          'user' => $user,
          'products' => $paginatedItems,
            'sessionEvents' => $user->ownedEvents()->where('availability', '!=' , 4)->where('availability', '!=' , 5)->get()
      ]);
    }

    public function showPublicInfos($slug)
    {
        $user = User::where('slug', $slug)->first();
        if (!$user) {
            return redirect(route('showHome')); // not found
        }


        return view('User::frontOffice.publicInfos', [
            'user' => $user,
            'sessionEvents' => $user->ownedEvents()->where('availability', '!=' , 4)->where('availability', '!=' , 5)->get()
        ]);
    }

    public function showProfile()
    {

        return view('User::frontOffice.privateProfile');
    }

    public function showPaymentInfos()
    {
        if (Auth::user()->type != 0) {
            return redirect()->route('showHome');
        }
        return view('User::frontOffice.paymentInfo');
    }


    public function handleUserUpdateProfile()
    {
        $data = Input::all();

        $user = Auth::user();

        $rules = [
          'first_name' => 'required',
          'last_name' => 'required',
          'email' => 'required|email|unique:users,email,'.$user->id
      ];

        $messages = [
          'first_name.required' => 'First name is required !',
          'last_name.required' => 'Last name is required !',
          'email.required' => 'Email is required !',
          'email.email' => 'Invalid email !',
          'email.unique' => 'Email already in use !'
      ];


        if (!is_null($data['city'])  || !is_null($data['postal_code']) || !is_null($data['address'])) {
            $rules['postal_code'] ='required|numeric';
            $rules['country'] ='required';
            $rules['city'] ='required';
            $rules['address'] ='required';
            $messages['country.required'] = 'Country is required !';
            $messages['city.required'] = 'City is required !';
            $messages['address.required'] = 'Adress is required !';
            $messages['postal_code.numeric'] = 'Zip code is Invalid !';
            $messages['postal_code.required'] = 'Zip code is required !';
        }

        if (!is_null($data['phone'])) {
            $rules['phone'] ='required|numeric';
            $messages['phone.numeric'] = 'Phone is Invalid !';
        }

        $validation = Validator::make($data, $rules, $messages);
        if ($validation->fails()) {
              return redirect()->back()->withErrors($validation->errors());
        }

        $checkEmail = User::where('id', '<>', Auth::user()->id)->where('email', '=', $data['email'])->first();

        if ($checkEmail) {
            Session::flash('message', 'Email is already used !');
            Session::flash('alert-class', 'alert-danger');
            return back();
        }

        $checkSlug = User::where('id', '<>', Auth::user()->id)->where('slug', '=', $data['slug'])->first();

        if ($checkSlug) {
            Session::flash('message', 'Slag is already used !');
            Session::flash('alert-class', 'alert-danger');
            return back();
        }


        if (isset($data['city']) || isset($data['postal_code']) || isset($data['address'])) {

            if (!$user->address_id) {

              $address = Address::create([

              'country'         =>  $data['country'],
              'city'            =>  $data['city'],
              'postal_code'     =>  $data['postal_code'],
              'address'         =>  $data['address']
            ]);
              $user->address_id = $address->id;
            } else {
                $address = $user->address;

                $address->update([

              'country'         =>  $data['country'],
              'city'            =>  $data['city'],
              'postal_code'     =>  $data['postal_code'],
              'address'         =>  $data['address']
            ]);
            }
        }

        $user->first_name      =  $data['first_name'];
        $user->last_name       =  $data['last_name'];
        $user->slug            =  $data['slug'];
        $user->email           =  $data['email'];
        $user->phone           =  $data['phone'];



        $user->save();
        if (isset($data['healer'])) {
            Session::flash('message', 'Profile Updated !');
            Session::flash('alert-class', 'alert-success');
            return back();
        } else {
            Session::flash('message', 'Profile Updated !');
            Session::flash('alert-class', 'alert-success');
            return back();
        }
    }



    public function handleUserUpdatePassword()
    {
        $data = Input::all();

        $user = User::find(Auth::user()->id);

        $rules = [
              'oldPassword' => 'required|min:6',
              'password' => 'required|min:6|confirmed'
          ];

          $messages = [
            'oldPassword.required' => 'Password is required !',
            'password.required' => 'Password is required !',
            'oldPassword.min' => 'Password must contain at least 6 charecters !',
            'password.min' => 'Password must contain at least 6 charecters !',
            'password.confirmed' => 'Password must be confirmed !'
        ];

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
          return redirect()->back()->withErrors($validation->errors(),'password');
        }


        if (!(Hash::check($data['oldPassword'], $user->password))) {
            Session::flash('message', 'Your current password does not match the password you provided. Try Again. !');
            Session::flash('alert-class', 'alert-danger');
            return back();
        }
        if ($data['oldPassword'] == $data['password']) {
            Session::flash('message', 'The new password can not be the same as your current password. Please choose a different password. !');
            Session::flash('alert-class', 'alert-danger');
            return back();
        }
        $user->password = bcrypt($data['password']);

        $user->save();

            Session::flash('message', 'Password Updated !');
            Session::flash('alert-class', 'alert-success');
            return back();

    }


    public function hundleUpdateHealerBioInfos()
    {
        $data = Input::all();
        $user = User::find(Auth::user()->id);

        $rules = [
          'bio' => 'required'
      ];

        $messages = [
          'bio.required' => 'Bio is required !'
      ];


        $validation = Validator::make($data, $rules, $messages);


        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors());
        }



        if (empty($user->additionalInfos)) {
            $user->additionalInfos()->create([

            'bio'    => $data['bio'],
            'state'    => $data['state']?$data['state']:null,

          ]);
        } else {
            $user->additionalInfos()->update([

            'bio'    => $data['bio'],
            'state'    => $data['state']?$data['state']:null,

          ]);
        }

        $user->progress  =  1;
        $user->save();
        Session::flash('message', 'Bio Updated !');
        Session::flash('alert-class', 'alert-success');
        return back();
    }

    public function apiHundleUpdateHealerEducationInfos(Request $request)
    {
        //Educations

        $data  = $request['data'];

        $user = User::find(Auth::user()->id);

        $rules = [
            'school' => 'required',
            'diploma' => 'required',
            'start_year' => 'date|required',
            'end_year' => 'date|required|after:start_year',

        ];

        $messages = [
            'school.required'      => 'School is required !',
            'diploma.required'     => 'Diploma is required !',
            'start_year.date'      => 'Invalid Date !',
            'start_year.required'  => 'Start Year is required !',
            'end_year.date'      => 'Invalid Date !',
            'end_year.required'  => 'End Year is required !',
            'end_year.after' => 'End year can\'t be inferior than start year !'
        ];

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return response()->json(['errors' => $validation->errors()]);
        }

        $school = $request['data']['school'];
        $diploma = $request['data']['diploma'];
        $start_year = $request['data']['start_year'];
        $end_year = $request['data']['end_year'];
        $study_field = null;

        $user->educations()->create([
            'school'    => $school,
            'diploma'    => $diploma,
            'study_field'    => $request['data']['study_field']?$request['data']['study_field']:null,
            'start_year'    => $start_year,
            'end_year'    => $end_year
          ]);


        return response()->json(['status' =>200]);
    }


    public function apiHundleUpdateHealerCertificationInfos(Request $request)
    {

        $user = User::find(Auth::user()->id);

        //Certification

        if (!$request['title']) {
            return response()->json(['errors' => 'Title is required !']);
        }

        $photoPath = null;
        if (isset($request['asset'])) {
            $certifAsset = 'photo-' . str_random(5) . time() . '.' . $request['asset']->getClientOriginalExtension();
            $fullImagePath = public_path('storage/uploads/users/certifs/' . $certifAsset);
            Image::make($request['asset']->getRealPath())->save($fullImagePath);
            $photoPath = 'storage/uploads/users/certifs/' . $certifAsset;
        }

        $user->certifications()->create([

          'title'          => $request['title'],
          'description'    => $request['description']?$request['description']:null,
          'asset'          => $photoPath ,
          'institution'    => $request['institution']?$request['institution']:null,
        ]);


        return response()->json(['status' =>200]);
    }



    public function apiHundleUpdateHealerCategoryInfos(Request $request)
    {
        $user = User::find(Auth::user()->id);

        $data = $request['data'];

        $category = $data['category'];

        if ($data['status'] == 'delete') {
            $user->streamer->categories->where('category_id', $category)->first()->delete();
        } elseif ($data['status'] == 'add') {
            $user->streamer->categories()->create(['category_id'=> $category]);
        } else {
            return response()->json(['status'=>400]);
        }

        return response()->json(['status'=>200]);
    }

    public function hundleUpdateProfilePhoto()
    {
        $data = Input::all();

        $user = User::find(Auth::user()->id);

        if (isset($data['photo'])) {
            $rules['photo'] = 'image|mimes:jpeg,png,jpg|max:5000';
            $messages['photo.image'] = 'La photo séléctionnée est invalide';
            $messages['photo.mimes'] = 'La photo séléctionnée est invalide';
            $messages['photo.max'] = 'La photo séléctionnée est trop grande';
            $validation = Validator::make($data, $rules, $messages);
            if ($validation->fails()) {
                return redirect()->back()->withErrors($validation->errors());
            }
        }
        if (isset($data['photo'])) {
            $photo = 'photo-' . str_random(5) . time() . '.' . $data['photo']->getClientOriginalExtension();
            $fullImagePath = public_path('storage/uploads/users/' . $photo);
            Image::make($data['photo']->getRealPath())->save($fullImagePath);
            $photoPath = 'storage/uploads/users/' . $photo;

            if ($user->image !== null) {
                File::delete(public_path() . '/' . $user->image);
                $user->image = null;
            }
            $user->image = $photoPath;

            Session::flash('message', 'Image Updated !');
            Session::flash('alert-class', 'alert-success');
        } else {
            File::delete(public_path() . '/' . $user->image);
            $user->image = null;

            Session::flash('message', 'Image Deleted !');
            Session::flash('alert-class', 'alert-success');
        }

        $user->save();

        return back();
    }



    public function apiHandleFollow(Request $request)
    {

        $streamerId = $request->data['streamerId'];

        $streamer = Streamer::find($streamerId);

        $isFolow =  Auth::user()->isFollow($streamerId);

        if ($isFolow) {

          Auth::user()->subcriptions->where('streamer_id', $streamerId)->first()->delete();

          return response()->json(['status'=>false,'followers'=>count($streamer->followers())]);

        }
        Auth::user()->subcriptions()->create([

             'streamer_id' => $streamer->id

        ]);

        return response()->json(['status'=>true,'followers'=>count($streamer->followers())]);
    }



     public function apiHandleBookmark(Request $request)
        {
                $user = Auth::user();
                $data = $request->data;

                $bookmarkEntity = 'App\Modules\User\Models\Product';

                if ($data['bookmark'] == 'event') {
                   $bookmarkEntity = 'App\Modules\User\Models\Event';
                }

                if ($data['bookmark'] == 'user'){
                  $bookmarkEntity = 'App\Modules\User\Models\User';
                }

                if (hasBookmark($data['bookmarkId'],$bookmarkEntity)) {
                  $user->allBookmarks->where('bookmarked_id', $data['bookmarkId'])->where('bookmarked_type', $bookmarkEntity)->first()->delete();
                  return response()->json(['status'=>200,'event'=>false]);
                }

                $bookmarkItem = $bookmarkEntity::find($data['bookmarkId']);
                $bookmarkItem->bookmarks()->create(['user_id' => $user->id]);
                return response()->json(['status'=>200,'event'=>true]);

          }


      public function showUserBookmark()
      {
          return view('User::frontOffice.bookmarks.index',[

              'events'   => Auth::user()->allBookmarks()->where('bookmarked_type','App\Modules\User\Models\Event')->get(),
              'products' => Auth::user()->allBookmarks()->where('bookmarked_type','App\Modules\User\Models\Product')->get(),
              'profiles' => Auth::user()->allBookmarks()->where('bookmarked_type','App\Modules\User\Models\User')->get(),

          ]);
      }

      public function showBookmarkEvents (Request $request)
      {
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect(Auth::user()->allBookmarks()->where('bookmarked_type','App\Modules\User\Models\Event')->get());
        $perPage = 5;

        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();

        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);

        $paginatedItems->setPath($request->url());
        return view('User::frontOffice.bookmarks.events',[

            'events'   => $paginatedItems

        ]);
      }

      public function showBookmarkProducts(Request $request)
      {

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect(Auth::user()->allBookmarks()->where('bookmarked_type','App\Modules\User\Models\Product')->get());
        $perPage = 5;

        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();

        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);

        $paginatedItems->setPath($request->url());
        return view('User::frontOffice.bookmarks.products',[

            'products' => $paginatedItems

        ]);
      }

      public function showBookmarkProfiles()
      {
        return view('User::frontOffice.bookmarks.profiles',[

          'profiles' => Auth::user()->allBookmarks()->where('bookmarked_type','App\Modules\User\Models\User')->paginate(5)

        ]);
      }


      public function showPublicEvents($slug) {

              $user = User::where('slug', $slug)->first();
              if (!$user) {
                  return redirect(route('showHome')); // not found
              }

              return view('User::frontOffice.publicEvents', [
                      'user' => $user,
                  'events' => $user->events()->paginate(3),
                    'sessionEvents' => $user->ownedEvents()->where('availability', '!=' , 4)->where('availability', '!=' , 5)->get()
              ]);
          }

          private function getUserType($type)
          {
              if ($type == 'seaker') {
                  return 1;
              }
              return 0;
          }

    public function showSessions() {
        return view('User::frontOffice.sessions.index', [
            "sessions" => Auth::user()->streamer->streams
        ]);
    }

    public function showParticipations() {

        $participations = [];

        $bookings = SessionBooking::where('user_id',Auth::id())->get();



        foreach ($bookings as $booking) {
            $participations[] = $booking->session;
        }

        return view('User::frontOffice.sessions.participations',[
            'participations' => $participations
        ]);
    }

    public function handleSubscriptionToSession($sId) {

        SessionBooking::create([
            'stream_id' => $sId,
            'user_id' =>Auth::id()
        ]);

        Session::flash('message', 'Subscription Succeded !');
        Session::flash('alert-class', 'alert-success');
        return back();

    }

    public function apiPayWithCC(Request $request)
    {
        $data = $request->data;
    }

    public function apiPayWithPaypal(Request $request)
    {
        $data = $request->data;
    }

    public function showUserCalander()
    {
      return view('User::frontOffice.calander.index',[
        'sessionEvents' => Auth::user()->ownedEvents()->where('availability', '!=' , 4)->where('availability', '!=' , 5)->get()
      ]);
    }

    public function handleSetAvailability()
    {
      $data = Input::all();


      SessionEvent::create([
        'title' => $data['optradio'] == 0 ? 'Unavailable' : 'Available',
        'availability' => $data['optradio'],
        'start_date' => $data['starttime'],
        'end_date' => $data['endtime'],
        'allDay' => $data['allday'] === 'true' ? 1 : 0,
        'owner_id' => Auth::id()
      ]);

      Session::flash('message', 'Availabilty Setted');
      Session::flash('alert-class', 'alert-success');
      return back();

    }

    public function handleUpdateAvailability()
    {
      $data = Input::all();

      $sessionEvent = SessionEvent::find($data['id']);

      $sessionEvent->update([
        'title' => $data['optradio'] == 0 ? 'Unavailable' : 'Available',
        'availability' => $data['optradio'],
        'start_date' => $data['starttime'],
        'end_date' => $data['allday'] === 'true' ? null  : $data['endtime'],
        'allDay' => $data['allday'] === 'true' ? 1 : 0,
        'owner_id' => Auth::id()
      ]);

      Session::flash('message', 'Availabilty Setted');
      Session::flash('alert-class', 'alert-success');
      return back();

    }

    public function handleDeleteAvailability()
    {
      $data = Input::all();

      $sessionEvent = SessionEvent::find($data['id']);
      $sessionEvent->delete();
            Session::flash('message', 'Availabilty Setted');
            Session::flash('alert-class', 'alert-success');
            return back();

    }

    public function apiCheckSession(Request $request)
    {
        $sessionEvent = SessionEvent::find($request->get('id'));

        if($sessionEvent->customer_id) {
          return response()->json([
          'status' => 200 ,
          'customerName' => $sessionEvent->customer->getFullName() ,
          'desc' => $sessionEvent->desc ,
          'slug' => $sessionEvent->customer->slug
         ]);
        }
        else {
          return response()->json(['status' => 201]);
        }
    }

    public function handleAcceptSession()
    {

      $data = Input::all();

      $sessionEvent = SessionEvent::find($data['id']);

      if(isset($data['refuse'])) {
      $sessionEvent->update([
        'availability' => 5
      ]);

      Session::flash('message', 'Session Refused');
      Session::flash('alert-class', 'alert-warning');
      return back();
      }
      else {
        $sessionEvent->update([
          'availability' => 4
        ]);

        Session::flash('message', 'Availabilty Accepted');
        Session::flash('alert-class', 'alert-success');
        return back();
      }


    }

    public function showPrivateSessions() {
      return view('User::frontOffice.sessions.private' , [
          'sessionEvents' => Auth::user()->ownedEvents()->where('customer_id', '!=' , null)->get()
      ]);
    }


    public function showUserChannel()
    {
      return view('User::frontOffice.channel.index', [
        'sessions' => Auth::user()->uploadedSessions
      ]);

    }

    public function handleUploadSession()
    {
      $data = Input::all();

      $rules = [
          'title' => 'required',
          'category' => 'required',
          'video' => 'mimes:mpeg,ogg,mp4,webm,3gp,mov,flv,avi,wmv,ts|required'
              ];

      $messages = [
          'title.required' => 'Session Title Is Required',
          'category.required' => 'PLease Specify A Category',
          'video.required' => 'You Must Upload A video',
          'video.mimes' => 'Video type not supported',
      ];

      $validation = Validator::make($data, $rules, $messages);

      if ($validation->fails()) {
          return redirect()->back()->withErrors($validation->errors(),'upload')->withInput();
      }

          $file = $data['video'];
          $video = time().$file->getClientOriginalName();
          $path = public_path().'/storage/uploads/videos';
          $file->move($path, $video);

    UploadedSession::create([
      'title' => $data['title'],
      'category_id' => $data['category'],
      'user_id' => Auth::id(),
      'desc' => $data['desc'] ? $data['desc'] : null ,
      'video' => $video,
      'free' => isset($data['free']) ? 1 : 0,  // 1 free , 0 no
      'price' => isset($data['free']) ? null : $data['price']
    ]);
    Session::flash('message', 'Session Uploaded !');
    Session::flash('alert-class', 'alert-success');
    return back();
    }

    public function showPublicChannel($slug) {
      $user = User::where('slug',$slug)->first();
      return view('User::frontOffice.publicChannel',[
        'videos' => $user->uploadedSessions()->paginate(5),
        'user' => $user,
          'sessionEvents' => $user->ownedEvents()->where('availability', '!=' , 4)->where('availability', '!=' , 5)->get()
      ]);
    }

    public function hanldeBookPrivateEvent($slug)
    {
          $user = User::where('slug',$slug)->first();
          $data = Input::all();
          SessionEvent::create([
            'title' => $data['title'],
            'availability' =>3,
            'desc' => $data['description'],
            'start_date' => $data['starttime'],
            'end_date' => $data['endtime'],
            'allDay' => $data['allday'] === 'true' ? 1 : 0,
            'owner_id' => $user->id,
            'customer_id'  => Auth::id(),
            'category_id' => $data['category']
          ]);

          Session::flash('message', 'Session Booked Successfly! You will recieve an email one the heale review it.');
          Session::flash('alert-class', 'alert-success');
          return back();
              }

              public function showPrivateParticipations()
              {
                return view('User::frontOffice.sessions.privateParticipations', [
                  'participations' => SessionEvent::where('customer_id',Auth::id())->get()
                ]);
              }
}
