<?php

namespace App\Modules\User\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Modules\User\Models\User;
use App\Modules\User\Models\Event;
use App\Modules\User\Models\Tag;
use App\Modules\User\Models\EventBookings;

use Session;
use Image;

use Carbon\Carbon;

class EventController extends Controller
{

      /**
       * @desc show 1st Dashboard page
       * @return view
       */

      public function showDashboard()
      {

          $rows = [];
          $bookings = [];

          foreach(Auth::user()->events as $event) {
              if(EventBookings::where('event_id',$event->id)->first())
                  $rows[] = EventBookings::where('event_id',$event->id)->get();
          }

          foreach ($rows as $row) {
              foreach ($row as $booking) {
                  $bookings[] = $booking;
              }
          }

          return view('User::frontOffice.event.index',
          [
              'bookings' => $bookings
          ]);

      }


    /**
     * @desc show 1st ListTags page
     * @return view
     */

    public function showListTags()
    {
        return view('User::frontOffice.event.listTags',[
          'tags' => Tag::All()
        ]);

    }


    /**
     * @desc show 1st Add Event page
     * @return view
     */

    public function showAddEvent()
    {
        return view('User::frontOffice.event.addEvent');

    }

    /**
     * @desc show 1st List Events page
     * @return view
     */

    public function showListEvents()
    {
        return view('User::frontOffice.event.listEvents', [
            'events' => Auth::user()->events()->paginate(5)
        ]);

    }

    /**
     * @desc show 1st List Bookings page
     * @return view
     */

    public function showListBookings()
    {
        $rows = [];
        $bookings = [];

        foreach(Auth::user()->events as $event) {
            if(EventBookings::where('event_id',$event->id)->first())
                $rows[] = EventBookings::where('event_id',$event->id)->get();
        }

        foreach ($rows as $row) {
            foreach ($row as $booking) {
                $bookings[] = $booking;
            }
        }
        return view('User::frontOffice.event.bookings',
            [
                'events' => $bookings
            ]);

    }


    /**
     * @desc handle Add new Tag
     * @param  Request
     * @return Response
     */
    public function handleAddTag()
    {
          $data = Input::all();

          $rules = [
             'name' => 'required',
         ];

          $messages = [
             'name.required'     => 'Tag\'s name is required !'
         ];

          $validation = Validator::make($data, $rules, $messages);

          if ($validation->fails()) {
              return redirect()->back()->withInput()->withErrors($validation->errors());
          }

          Tag::create([
            'name'=>$data['name'],
              'user_id' => Auth::id()
          ]);


          Session::flash('message', 'Category added !');
          Session::flash('alert-class', 'alert-success');
          return back();

    }

    /**
     * @desc handle Delete Tag
     * @param  Request
     * @return Response
     */
    public function hundleDeleteTag($tagId)
    {
          $tag = Tag::find($tagId);

          if (empty($tag)) {
            Session::flash('message', 'Category Not Found !');
            Session::flash('alert-class', 'alert-danger');
            return back();
          }

          $tag->delete();
          Session::flash('message', '  Category deleted !');
          Session::flash('alert-class', 'alert-success');
          return back();
    }

    /**
     * @desc handle Delete Tag
     * @param  Request
     * @return Response
     */
    public function hundleUpdateTag($tagId)
    {
        $tag = Tag::find($tagId);

        if (empty($yag)) {
          Session::flash('message', 'Category Not Found !');
          Session::flash('alert-class', 'alert-danger');
          return back();
        }
        $data = Input::all();
        $tag->name = $data['title'];
        $tag->save();
        Session::flash('message', 'Catagory updated !');
        Session::flash('alert-class', 'alert-success');
        return back();

    }

    /**
     * @desc handle Add new Event
     * @param  Request
     * @return Response
     */
    public function handleAddEvent()
    {
          $data = Input::all();

          $rules = [
             'title'             => 'required',
             'short_description' => 'required',
             'long_description'  => 'required',
             'start_date'        => 'required|after_or_equal:'.Carbon::now(),
             'end_date'          => 'required|after_or_equal:start_date',
             'photo'             => 'image|mimes:jpeg,png,jpg|max:5000|required'
         ];

          $messages = [
             'title.required'               => 'Event Name is required !',
             'short_description.required'   => 'Event Short Description is required !',
             'long_description.required'    => 'Event Long Description is required !',
             'start_date.required'          => 'Event\'s start day is required !',
             'start_date.after_or_equal'    => 'Event\'s start day is invalid !',
             'start_date.required'          => 'Event\'s end day is required !',
             'start_date.after_or_equal'    => 'Event\'s end day is invalid !',
             'photo.image'                  => 'Invalid image\'s form file',
             'photo.mimes'                  => 'Invalid image\'s form file',
             'photo.max'                    => 'Image\'s size too big ',
             'photo.required'                    => 'Event Image Is Required ',

         ];

          $validation = Validator::make($data, $rules, $messages);

          if ($validation->fails()) {
              return redirect()->back()->withInput()->withErrors($validation->errors());
          }

          $event = Auth::user()->events()->create([
            'title'=>$data['title'],
            'short_description'=>$data['short_description'],
            'long_description'=>$data['long_description'],
            'start_date'=>$data['start_date'],
            'end_date'=>$data['end_date'],
          ]);

          $event->tags()->attach($data['tags']);

          $eventAsset = 'photo-' . str_random(5) . time() . '.' . $data['photo']->getClientOriginalExtension();
          $fullImagePath = public_path('storage/uploads/users/events/' . $eventAsset);
          Image::make($data['photo']->getRealPath())->save($fullImagePath);
          $photoPath = 'storage/uploads/users/events/' . $eventAsset;

          $event->medias()->create([
            'link' => $photoPath
          ]);

          Session::flash('message', 'Event Created !');
          Session::flash('alert-class', 'alert-success');
          return redirect()->route('showListEvents');

    }

    public function showUpdateEvent($id) {
        return view('User::frontOffice.event.updateEvent', [
            'event' => Event::find($id)
        ]);
    }


    public function handleUpdateEvent($id) {
        $data = Input::all();

        $rules = [
            'title'             => 'required',
            'short_description' => 'required',
            'long_description'  => 'required',
            'start_date'        => 'required|after_or_equal:'.Carbon::now(),
            'end_date'          => 'required|after_or_equal:start_date'
        ];

        $messages = [
            'title.required'               => 'Event Name is required !',
            'short_description.required'   => 'Event Short Description is required !',
            'long_description.required'    => 'Event Long Description is required !',
            'start_date.required'          => 'Event\'s start day is required !',
            'start_date.after_or_equal'    => 'Event\'s start day is invalid !',
            'start_date.required'          => 'Event\'s end day is required !',
            'start_date.after_or_equal'    => 'Event\'s end day is invalid !'

        ];

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation->errors());
        }

        $event = Event::find($id);

        $event->update([
            'title'=>$data['title'],
            'short_description'=>$data['short_description'],
            'long_description'=>$data['long_description'],
            'start_date'=>$data['start_date'],
            'end_date'=>$data['end_date'],
        ]);


//        foreach ($data['tags'] as $tag) {
//
//            if ($event->tags()->contains($tag)) {
//                $event->tags()->attach($tag);
//            }
//        }

          $event->tags()->sync($data['tags']);

        $photoPath = null;
          if(isset($data['photo'])) {
            $eventAsset = 'photo-' . str_random(5) . time() . '.' . $data['photo']->getClientOriginalExtension();
            $fullImagePath = public_path('storage/uploads/users/events/' . $eventAsset);
            Image::make($data['photo']->getRealPath())->save($fullImagePath);
            $photoPath = 'storage/uploads/users/events/' . $eventAsset;

          }
           $event->medias()->update([
            'link' => $photoPath ? $photoPath : $event->medias->first()->link
           ]);

        Session::flash('message', 'Event Updated !');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('showListEvents');
    }

    /**
     * @desc handle Delete Event
     * @param  Request
     * @return Response
     */
    public function hundleDeleteEvent($eventId)
    {
          $event = Event::find($eventId);

          if (empty($event)) {
            Session::flash('message', 'Event Not Found !');
            Session::flash('alert-class', 'alert-danger');
            return back();
          }

          $event->delete();
          Session::flash('message', '  Event Deleted !');
          Session::flash('alert-class', 'alert-success');
          return back();
    }

    /**
     * @desc handle regestring new user 1st step
     * @param  Request
     * @return Response
     */
    public function apiBookingEvent(Request $request)
    {

          $eventId = $request->data['eventId'];
          $event = Event::find($eventId);

          if (empty($event)) {
            return response()->json('status',404);
          }

          $hasEventBooking =  Auth::user()->hasEventBooking($event->id);
          if ($hasEventBooking) {
              $booking = Auth::user()->bookings->where('event_id', $eventId)->first()->delete();

              return response()->json(['status'=>200,'booking'=>false]);
          }

          Auth::user()->bookings()->create([

               'event_id' => $event->id

          ]);

          return response()->json(['status'=>200,'booking'=>true]);
    }


}
