<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'stores';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'user_id'
    ];



    public function user() {
        return $this->hasOne('App\Modules\User\Models\User','id','user_id');
    }

    public function products()
    {
      return $this->hasMany('App\Modules\User\Models\Product','store_id','id');
    }





}
