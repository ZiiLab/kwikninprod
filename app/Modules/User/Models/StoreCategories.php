<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class StoreCategories extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'store_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function products() {
        return $this->hasMany('App\Modules\User\Models\Product','category_id','id');
    }

}
