<?php

namespace App\Modules\User\Models;


use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'type',
        'password',
        'address_id',
        'phone',
        'status',
        'progress',
        'image',
        'validation',
        'pin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles(){
        return $this->belongsToMany(
            'App\Modules\User\Models\Role',
            'user_roles',
            'user_id',
            'role_id'
        )->withTimestamps();
    }

    public function assignRole($role){
        if(is_string($role)){
            $role = Role::where('title',$role)->get();
        }
        if(!$role) return false;
        $this->roles()->attach($role);
    }

    public function retractRole($role){
        $this->roles()->detach($role);
    }

    public function address(){
        return $this->hasOne('App\Modules\User\Models\Address','id','address_id');
    }

    public function payments(){
        return $this->hasMany('App\Modules\General\Models\PaymentInfo', 'user_id', 'id');
    }

    public function paypals(){
        return $this->hasMany('App\Modules\User\Models\Paypal', 'user_id', 'id');
    }

    public function transactions(){
        return $this->hasMany('App\Modules\User\Models\Transaction', 'user_id', 'id');
    }


      public function educations(){
        return $this->hasMany('App\Modules\User\Models\Education');
    }

      public function certifications(){
        return $this->hasMany('App\Modules\User\Models\Certification');
    }


      public function additionalInfos(){
        return $this->hasOne('App\Modules\User\Models\AdditionalInfo','user_id');
    }


    public function streamer()
    {
        return $this->hasOne('App\Modules\Broadcasting\Models\Streamer');
    }

        public function messages()
    {
        return $this->hasMany('App\Modules\Broadcasting\Models\Message');
    }

    public function subcriptions()
    {
      return $this->hasMany('App\Modules\Broadcasting\Models\Subscription', 'user_id', 'id');
    }

    public function libraries()
    {
      return $this->hasMany('App\Modules\General\Models\Library');
    }


    public function getFullName()
    {
      return $this->first_name.' '.$this->last_name;
    }

    public function isFollow($streamerId)
    {
        $sub = Auth::user()->subcriptions->where('streamer_id', $streamerId)->first();

        if (empty($sub)) {
          return false;
        }
        return true;
    }
    public function hasEventBooking($eventId)
    {

        $booking = Auth::user()->bookings->where('event_id', $eventId)->first();

        if (empty($booking)) {
          return false;
        }
        return true;
    }
    public function bookings()
    {
        return $this->hasMany('App\Modules\User\Models\EventBookings');
    }

    public function productBookings()
    {
        return $this->hasMany('App\Modules\User\Models\ProductBooking');
    }


    public function events(){
        return $this->hasMany('App\Modules\User\Models\Event', 'user_id', 'id');
    }

    public function tags(){
        return $this->hasMany('App\Modules\User\Models\Tag', 'user_id', 'id');
    }

    public function store() {
        return $this->hasOne('App\Modules\User\Models\Store','user_id','id');
    }

    public function SessionBookings() {
        return $this->hasMany('App\Modules\Broadcasting\Models\SessionBooking');
    }


    public function allBookmarks(){
          return $this->hasMany('App\Modules\User\Models\Bookmark','user_id');
      }

      public function bookmarks(){

      return $this->morphMany('App\Modules\User\Models\Bookmark','bookmarked');
  }

  public function ownedEvents() {
    return $this->hasMany('App\Modules\User\Models\SessionEvent','owner_id');
  }

  public function bookedEvents() {
    return $this->hasMany('App\Modules\User\Models\SessionEvent','customer_id');
  }

  public function uploadedSessions() {
    return $this->hasMany('App\Modules\User\Models\UploadedSession','user_id');
  }


}
