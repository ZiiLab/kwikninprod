<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Certification extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'certifications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'title',
    'description',
    'asset',
    'institution'
    ];


         public function user()
    {
       return $this->belongsTo('App\Modules\User\Models\User');
    }


}
