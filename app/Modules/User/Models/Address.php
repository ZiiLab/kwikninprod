<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'addresses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'address',
        'city',
        'postal_code',
        'country',
    ];

    public function user(){
        return $this->hasOne('App\Modules\User\Models\User');
    }

}
