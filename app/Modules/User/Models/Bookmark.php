<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Bookmark extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bookmarks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

     protected $fillable = [
        'bookmarked_id',
        'bookmarked_type',
        'user_id'

    ];

       public function marker(){

         return $this->belongsTo('App\Modules\User\Models\User','user_id');
     }

     public function bookmarked(){

        return $this->morphTo();
    }

}
