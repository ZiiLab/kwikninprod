<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id_transaction',
        'amount',
        'currency_code',
        'transaction_date',
        'user_id'
    ];

    protected $dates = [
        'transaction_date',
    ];

    public function user(){
        return $this->hasOne('App\Modules\User\Models\User', 'id', 'user_id');
    }

}
