<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class SessionEvent extends Model {

  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   */
  public $timestamps = true;

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'session_events';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'title',
    'desc',
    'availability',
    'price',
    'start_date',
    'end_date',
    'allDay',
    'owner_id',
    'customer_id',
    'category_id'

  ];

  protected $dates = [
    'start_date',
    'end_date'
  ];


public function owner()
{
  return $this->hasOne('App\Modules\User\Models\User','id','owner_id');
}

public function customer()
{
  return $this->hasOne('App\Modules\User\Models\User','id','customer_id');
}


public function category()
{
  return $this->hasOne('App\Modules\Broadcasting\Models\Category','id','category_id');
}



}
