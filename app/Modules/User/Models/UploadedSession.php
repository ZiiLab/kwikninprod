<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class UploadedSession extends Model {

  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   */
  public $timestamps = true;

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'uploaded_sessions';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'title',
    'desc',
    'price',
    'free',
    'video',
    'user_id',
    'category_id'

  ];



public function user()
{
  return $this->belongsTo('App\Modules\User\Models\User','id','user_id');
}

public function category()
{
  return $this->hasOne('App\Modules\Broadcasting\Models\Category','id','category_id');
}




}
