<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'quantity',
        'price',
        'description',
        'image',
        'category_id',
        'store_id'

    ];

    public function category() {
       return $this->hasOne('App\Modules\User\Models\StoreCategories','id','category_id');
    }


    public function store() {
       return $this->belongsTo('App\Modules\User\Models\Store');
    }
    public function bookmarks(){

    return $this->morphMany('App\Modules\User\Models\Bookmark','bookmarked');
}

public function productBookings()
{
    return $this->hasMany('App\Modules\User\Models\ProductBooking');
}

 /*   public function bookings() {
        bookings = null ;
        if($this->productBookings) {

        }

        return bookings;
    }*/


}
