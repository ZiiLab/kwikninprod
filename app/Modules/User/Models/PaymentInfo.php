<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentInfo extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'payments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'card_holder_name',
        'card_number',
        'exp_date',
        'cvv',
        'zip',
        'user_id'
    ];

    protected $dates = [
        'exp_date',
    ];

    public function user(){
        return $this->hasOne('App\Modules\User\Models\User', 'id', 'user_id');
    }

}
