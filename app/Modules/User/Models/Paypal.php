<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Paypal extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'email',
      'adresse_line',
      'postal_code',
      'city',
      'state',
      'postal_code',
      'country_code',
      'user_id',
    ];

    protected $dates = [
        'exp_date',
    ];

    public function user(){
        return $this->hasOne('App\Modules\User\Models\User', 'id', 'user_id');
    }

}
