<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;


class Education extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'educations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
  'school',
'diploma',
'study_field',
'start_year',
'end_year',
'user_id'
    ];

    protected $dates = [
        'start_year',
        'end_year'
    ];

        public function user()
    {
        return $this->belongsTo('App\Modules\User\Models\User');
    }


}
