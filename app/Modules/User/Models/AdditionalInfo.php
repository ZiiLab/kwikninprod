<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;


class AdditionalInfo extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'additional_infos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'bio',
        'state',
        'study_level',
        'user_id'
    ];


    public function user()
    {
        return $this->hasOne('App\Modules\User\Models\User','user_id');
    }


}
