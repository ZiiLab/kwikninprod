<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model {

  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   */
  public $timestamps = true;

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'events';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'title',
    'short_description',
    'long_description',
    'user_id',
    'start_date',
    'end_date'
  ];

  protected $dates = [
      'start_date',
      'end_date'
  ];

      public function medias()
  {
      return $this->hasMany('App\Modules\General\Models\Media');
  }

  public function tags()
    {
        return $this->belongsToMany(
            'App\Modules\User\Models\Tag',
            'event_tags',
            'event_id',
            'tag_id'
        )->withTimestamps();
    }

    public function bookings()
    {
        return $this->hasMany('App\Modules\User\Models\EventBookings');
    }


    public function user(){
        return $this->belongsTo('App\Modules\User\Models\User');
    }

      public function bookmarks(){

      return $this->morphMany('App\Modules\User\Models\Bookmark','bookmarked');
  }

}
