<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class ProductBooking extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_bookings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quantity',
        'user_id',
        'product_id'

    ];

    public function product() {
        return $this->belongsTo('App\Modules\User\Models\Product');
    }

    public function user() {
        return $this->belongsTo('App\Modules\User\Models\User');
    }

}
