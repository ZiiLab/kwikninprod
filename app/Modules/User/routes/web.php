<?php

Route::group(['module' => 'User', 'middleware' => ['web'], 'namespace' => 'App\Modules\User\Controllers'], function() {

    Route::get('/register/{type?}', 'UserController@showRegisterUserStepOne')->name('showRegisterUserStepOne');
    Route::get('/register_step_two/{type?}', 'UserController@showRegisterUserStepTwo')->name('showRegisterUserStepTwo');
    Route::get('/register_step_three/{type?}', 'UserController@showRegisterUserStepThree')->name('showRegisterUserStepThree');
    Route::get('/welcome/{type?}', 'UserController@showWelcomePage')->name('showWelcomePage');
    Route::post('/register_step_one/{type?}', 'UserController@handleRegisterUserStepOne')->name('handleRegisterUserStepOne');
    Route::post('/register_step_two/{type?}', 'UserController@handleRegisterUserStepTwo')->name('handleRegisterUserStepTwo');

    Route::post('/login', 'UserController@apiHandleUserLogin')->name('apiHandleUserLogin');
    Route::get('/logout', 'UserController@handleLogout')->name('handleLogout');
    Route::get('/validate_email/{code}', 'UserController@handleUserMailValidation')->name('handleUserMailValidation');

    // Route::get('/first_step', 'UserController@handlefirstStep')->name('handlefirstStep');


    // Route::get('/second_step', 'UserController@handlesecondStep')->name('handlefirstStep');


    Route::post('/validate_code', 'UserController@apiHandleUserCodeVlidation')->name('apiHandleUserCodeVlidation');

    Route::get('/show_model_verification', 'UserController@showWModelVerification')->name('showWModelVerification');


    Route::group(['prefix' => 'user', 'middleware' => ['user']],function(){

        Route::group(['prefix' => 'store', 'middleware' => ['user']], function() {

            Route::get('/', 'UserController@showUserStore')->name('showUserStore');
            Route::post('/', 'UserController@handleCreateStore')->name('handleCreateStore');
            Route::get('/addCategory', 'UserController@showAddCategory')->name('showAddCategory');
            Route::post('/addCategory', 'UserController@handleAddCategory')->name('handleAddCategory');
            Route::get('/deleteCategory/{id}', 'UserController@handleDeleteCategory')->name('handleDeleteCategory');
            Route::get('addProduct', 'UserController@showAddProduct')->name('showAddProduct');
            Route::post('addProduct', 'UserController@handleAddProduct')->name('handleAddProduct');
            Route::get('list', 'UserController@showProductList')->name('showProductList');
            Route::get('/deleteProduct/{id}', 'UserController@handleDeleteProduct')->name('handleDeleteProduct');
            Route::get('/updateProduct/{id}', 'UserController@showUpdateProduct')->name('showUpdateProduct');
            Route::post('/updateProduct/{id}', 'UserController@handleUpdateProduct')->name('handleUpdateProduct');
            Route::post('/editCategory', 'UserController@handleEditCategory')->name('handleEditCategory');
            Route::get('/events/{slug}', 'UserController@showPublicEvents')->name('showPublicEvents');
            Route::get('/bookings', 'UserController@showUserBookings')->name('showUserBookings');


        });

        Route::group(['prefix' => 'sessions', 'middleware' => ['user']], function() {

            Route::get('/', 'UserController@showSessions')->name('showSessions');
            Route::get('/participations', 'UserController@showParticipations')->name('showParticipations');
            Route::get('/private', 'UserController@showPrivateSessions')->name('showPrivateSessions');
            Route::get('/private-participations', 'UserController@showPrivateParticipations')->name('showPrivateParticipations');
            Route::get('/subscribe/{sId}', 'UserController@handleSubscriptionToSession')->name('handleSubscriptionToSession');

        });

        Route::group(['prefix' => 'channel', 'middleware' => ['user']], function() {

          Route::get('/', 'UserController@showUserChannel')->name('showUserChannel');
          Route::post('/upload', 'UserController@handleUploadSession')->name('handleUploadSession');

        });

        Route::group(['prefix' => 'calander', 'middleware' => ['user']], function() {

          Route::get('/', 'UserController@showUserCalander')->name('showUserCalander');
          Route::get('/events', 'UserController@apiGetUserEvents')->name('apiGetUserEvents');
          Route::post('/setAvailability', 'UserController@handleSetAvailability')->name('handleSetAvailability');
          Route::post('/updateAvailability', 'UserController@handleUpdateAvailability')->name('handleUpdateAvailability');
          Route::post('/delete', 'UserController@handleDeleteAvailability')->name('handleDeleteAvailability');
          Route::get('/check', 'UserController@apiCheckSession')->name('apiCheckSession');
          Route::post('/accept', 'UserController@handleAcceptSession')->name('handleAcceptSession');

        });

        Route::group(['prefix' => 'events', 'middleware' => ['user']],function(){
            Route::get('/dashboard', 'EventController@showDashboard')->name('showDashboard');
            Route::get('/tags', 'EventController@showListTags')->name('showListTags');
            Route::get('/tags/{id}', 'EventController@hundleDeleteTag')->name('hundleDeleteTag');
            Route::get('/addEvent', 'EventController@showAddEvent')->name('showAddEvent');
            Route::get('/events', 'EventController@showListEvents')->name('showListEvents');
            Route::get('/events/{id}', 'EventController@hundleDeleteEvent')->name('hundleDeleteEvent');
            Route::get('/updateEvent/{id}', 'EventController@showUpdateEvent')->name('showUpdateEvent');
            Route::post('/updateEvent/{id}', 'EventController@handleUpdateEvent')->name('handleUpdateEvent');
            Route::get('/bookings', 'EventController@showListBookings')->name('showListBookings');
            Route::post('/tags', 'EventController@handleAddTag')->name('handleAddTag');
            Route::post('/addEvent', 'EventController@handleAddEvent')->name('handleAddEvent');
            Route::post('/bookings', 'EventController@apiBookingEvent')->name('apiBookingEvent');

        });

        Route::group(['prefix' => 'books', 'middleware' => ['user']],function(){
            Route::get('/bookmark', 'UserController@showUserBookmark')->name('showUserBookmark');
            Route::get('/events', 'UserController@showBookmarkEvents')->name('showBookmarkEvents');
            Route::get('/products', 'UserController@showBookmarkProducts')->name('showBookmarkProducts');
            Route::get('/profiles', 'UserController@showBookmarkProfiles')->name('showBookmarkProfiles');
            Route::post('bookmark-add', 'UserController@apiHandleBookmark')->name('apiHandleBookmark');

        });



        Route::get('/profile', 'UserController@showProfile')->name('showProfile');
        Route::post('/profile/update', 'UserController@handleUserUpdateProfile')->name('handleUserUpdateProfile');

        Route::post('/profile/update/passeword', 'UserController@handleUserUpdatePassword')->name('handleUserUpdatePassword');

        Route::post('/profile/photo/','UserController@hundleUpdateProfilePhoto')->name('hundleUpdateProfilePhoto');
        Route::get('/profile/infos', 'UserController@showHealerInfos')->name('showHealerInfos');

        Route::post('/profile/infos/bio', 'UserController@hundleUpdateHealerBioInfos')->name('hundleUpdateHealerBioInfos');

        Route::post('/profile/infos/education', 'UserController@apiHundleUpdateHealerEducationInfos')->name('apiHundleUpdateHealerEducationInfos');
        Route::post('/profile/infos/certification', 'UserController@apiHundleUpdateHealerCertificationInfos')->name('apiHundleUpdateHealerCertificationInfos');
        Route::post('/profile/infos/category', 'UserController@apiHundleUpdateHealerCategoryInfos')->name('apiHundleUpdateHealerCategoryInfos');

        Route::get('/profile/paymentInfos', 'UserController@showPaymentInfos')->name('showPaymentInfos');
        Route::get('/{slug}', 'UserController@showPublicProfile')->name('showPublicProfile');
        Route::get('/infos/{slug}', 'UserController@showPublicInfos')->name('showPublicInfos');
        Route::post('/follow', 'UserController@apiHandleFollow')->name('apiHandleFollow');

        Route::get('/profile/event', 'UserController@showUserEvent')->name('showUserEvent');
        Route::get('/channel/{slug}', 'UserController@showPublicChannel')->name('showPublicChannel');
        Route::post('/book/{slug}', 'UserController@hanldeBookPrivateEvent')->name('hanldeBookPrivateEvent');




        // route for processing payment
          Route::post('/profile/paypal/{itemId}', 'PaymentController@payWithpaypal')->name('paypal');

      // route for check status of the payment
          Route::get('/profile/status', 'PaymentController@getPaymentStatus')->name('status');


    });



});
