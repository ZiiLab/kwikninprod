<?php
use Illuminate\Support\Facades\Auth;


/**
 *	User Helper
 */


 function hasBookmark($bookmarkId,$bookmarkEntity)
{

        $hasBoomark = Auth::user()->allBookmarks()->where('bookmarked_id', $bookmarkId)->where('bookmarked_type', $bookmarkEntity)->first();

        if (empty($hasBoomark)) {
                return false;
        }

        return true;

}
