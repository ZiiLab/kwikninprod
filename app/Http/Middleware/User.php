<?php

namespace App\Http\Middleware;


use Closure;
use Auth;

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$guard = null)
    {
        if(!Auth::guard($guard)->check()) {
          //  Alert::warning('Attention', 'Vous n\'êtes pas connecté!')->persistent('Fermer');
            return redirect()->route('showHome')->with( ['data' => 'data'] );
        }else {
            return $next($request);
        }
    }


}
