<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBroadcastingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('streamers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });


              Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label');
             $table->string('description')->nullable();
            $table->timestamps();
        });

          Schema::create('streams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('type'); //0 public / 1 private
            $table->string('description')->nullable();
            $table->integer('streamer_id')->unsigned()->nullable();
            $table->foreign('streamer_id')->references('id')->on('streamers');
            $table->integer('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->timestamps();
        });

            Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('streamer_id')->unsigned()->nullable();
            $table->foreign('streamer_id')->references('id')->on('streamers');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });

              Schema::create('streamers_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('streamer_id')->unsigned()->nullable();
            $table->foreign('streamer_id')->references('id')->on('streamers');
            $table->integer('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->timestamps();
        });

                      Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('body');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('stream_id')->unsigned()->nullable();
            $table->foreign('stream_id')->references('id')->on('streams');
            $table->timestamps();
        });








    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('streamers');
             Schema::dropIfExists('categories');
             Schema::dropIfExists('streams');
             Schema::dropIfExists('subscriptions');
             Schema::dropIfExists('streamers_categories');
             Schema::dropIfExists('messages');
    }
}
