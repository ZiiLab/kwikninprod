<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('city')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('country')->nullable();
            $table->string('address')->nullable();
        });

        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title'); // 1 Admin // 2 User
            $table->timestamps();
        });

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 30);
            $table->string('last_name', 30);
            $table->tinyInteger('type'); // 0 healer / 1 seeker
            $table->string('slug')->nullable();
            $table->string('email');
            $table->string('password', 60);
            $table->string('phone')->nullable();
            $table->integer('status'); // 0 mail invalide / 1 mail valid
            $table->integer('progress'); // 0 bio Info / 1 Payment Info
            $table->string('image')->nullable();
            $table->string('validation')->nullable();
            $table->integer('pin')->nullable();
            $table->integer('address_id')->unsigned()->nullable();
            $table->foreign('address_id')->references('id')->on('addresses');
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('user_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->unsigned();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('card_holder_name')->nullable();
            $table->string('card_number')->nullable();
            $table->date('exp_date')->nullable();
            $table->string('cvv')->nullable();
            $table->string('zip')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });


        Schema::create('paypals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->nullable();
            $table->string('adresse_line')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('country_code')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_transaction')->nullable();
            $table->string('amount')->nullable();
            $table->string('currency_code')->nullable();
            $table->date('transaction_date')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

         Schema::create('additional_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('bio');
            $table->integer('state')->nullable();
            $table->integer('study_level')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });

          Schema::create('educations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('school');
            $table->string('diploma');
            $table->string('study_field')->nullable();
            $table->date('start_year');
            $table->date('end_year');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });


          Schema::create('certifications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description')->nullable();
            $table->string('asset')->nullable();
            $table->string('institution')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });


        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('store_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('store_id')->unsigned();
            $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
            $table->timestamps();
        });


        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('quantity');
            $table->float('price');
            $table->string('description')->nullable();
            $table->string('image')->nullable();
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('store_categories')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('product_bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('quantity');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('bookmarks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('bookmarked_id');
            $table->string('bookmarked_type');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_roles');
        Schema::dropIfExists('users');
        Schema::dropIfExists('roles');
        Schema::dropIfExists('addresses');
        Schema::dropIfExists('payments');
         Schema::dropIfExists('certifications');
          Schema::dropIfExists('educations');
           Schema::dropIfExists('additional_infos');
    }
}
