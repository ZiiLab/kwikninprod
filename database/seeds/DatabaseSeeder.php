<?php

use Illuminate\Database\Seeder;
use App\Modules\User\Models\User;
use App\Modules\Broadcasting\Models\Streamer;

use App\Modules\Broadcasting\Models\Stream;

use App\Modules\Broadcasting\Models\Category;
use App\Modules\Broadcasting\Models\Message;
use App\Modules\User\Models\Event;
use App\Modules\User\Models\Tag;
use App\Modules\General\Models\Media;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      $user1 =  User::create([
       	'first_name' => 'Zak',
       	'last_name'  => 'Kh',
       	'email'  => 'zak@kh.com',
        'slug'  => 'zak123',
       	'password'  => bcrypt('zak123'),
       	'type'  => 0,
       	'status' => 1,
        'progress' => 0
       ]);


       $user2 = User::create([
       	'first_name' => 'Zii',
       	'last_name'  => 'Kh',
       	'email'  => 'zii@kh.com',
        'slug'  => 'zii1234',
       	'password'  => bcrypt('zii1234'),
       	'type'  => 1,
       	'status' => 1,
        'progress' => 2
       ]);

       $user3 =  User::create([
          'first_name' => 'med',
          'last_name'  => 'Kh',
          'email'  => 'med@kh.com',
         'slug'  => 'med123',
          'password'  => bcrypt('zak123'),
          'type'  => 0,
          'status' => 1,
         'progress' => 0
        ]);

        $user4 =  User::create([
           'first_name' => 'med1',
           'last_name'  => 'Kh',
           'email'  => 'med1@kh.com',
          'slug'  => 'med124',
           'password'  => bcrypt('zak123'),
           'type'  => 0,
           'status' => 1,
          'progress' => 0
         ]);

         $user5 =  User::create([
            'first_name' => 'med2',
            'last_name'  => 'Kh',
            'email'  => 'med2@kh.com',
           'slug'  => 'med321',
            'password'  => bcrypt('zak123'),
            'type'  => 0,
            'status' => 1,
           'progress' => 0
          ]);

       $streamer = Streamer::create([
        'username' => 'ZiiShow',
        'user_id' => $user1->id
       ]);

       $streamer1 = Streamer::create([
        'username' => 'med',
        'user_id' => $user3->id
       ]);

       $streamer2 = Streamer::create([
        'username' => 'med1',
        'user_id' => $user4->id
       ]);

       $streamer3 = Streamer::create([
        'username' => 'med2',
        'user_id' => $user5->id
       ]);

       $user1->subcriptions()->create([

            'streamer_id' => $streamer1->id

       ]);

       $user1->subcriptions()->create([

            'streamer_id' => $streamer2->id

       ]);

       $user1->subcriptions()->create([

            'streamer_id' => $streamer3->id

       ]);

       $category = Category::create([
        'label' => 'Body',
        'description' => 'Body'
      ]);
      $category = Category::create([
       'label' => 'Mind',
       'description' => 'Mind'
     ]);
       $category = Category::create([
        'label' => 'Spiritual',
        'description' => 'Spiritual'
      ]);
      $category = Category::create([
       'label' => 'Meridians',
       'description' => 'Meridians'
     ]);
       $category = Category::create([
        'label' => 'Vibrational',
        'description' => 'Vibrational'
      ]);


      //  $category = Category::create([
      //   'label' => 'test',
      //   'description' => 'test'
      // ]);
      //
       $stream = Stream::create([
        'title'=> 'test' ,
        'type'=> 0,
        'description'=> 'test',
        'streamer_id'=> $streamer2->id,
        'category_id'=>  $category->id
      ]);

      $stream2 = Stream::create([
       'title'=> 'test3' ,
       'type'=> 0,
       'description'=> 'test3',
       'streamer_id'=> $streamer3->id,
       'category_id'=>  $category->id
     ]);

     $stream3 = Stream::create([
      'title'=> 'test' ,
      'type'=> 0,
      'description'=> 'test',
      'streamer_id'=> $streamer->id,
      'category_id'=>  $category->id
    ]);

       Message::create([
        'body' => 'test msg',
        'user_id' => $user1->id,
        'stream_id' => $stream->id

       ]);
        Message::create([
        'body' => 'test msg 2 ',
        'user_id' => $user2->id,
        'stream_id' => $stream->id

       ]);

      $tag1 = Tag::create([
        'name' => 'catagory1'
      ]);

      $tag2 = Tag::create([
        'name' => 'catagory2'
      ]);

      $tag3 = Tag::create([
        'name' => 'catagory3'
      ]);

      $event1 = $user1->events()->create([
       'title' => 'Event1',
       'short_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, cupiditate!',
       'long_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio alias labore deserunt dolore ex deleniti natus obcaecati molestiae excepturi atque!',
       'start_date' =>date_format(date_create("2019-07-15 12:03"),"Y-m-d H:i:s"),
       'end_date' => date_format(date_create("2019-07-16 12:03"),"Y-m-d H:i:s")

      ]);


        $event1->tags()->attach([$tag1->id,$tag2->id,$tag3->id]);

      $event1->medias()->create([
        'link'=>'images/events/event1.jpg'
      ]);

      $event2 = $user1->events()->create([
       'title' => 'Event2',
       'short_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, cupiditate!',
       'long_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio alias labore deserunt dolore ex deleniti natus obcaecati molestiae excepturi atque!j',
       'start_date' =>date_format(date_create("2019-09-15 12:03"),"Y-m-d H:i:s"),
       'end_date' => date_format(date_create("2019-10-16 12:03"),"Y-m-d H:i:s")
      ]);
      $event2->tags()->attach([$tag1->id,$tag2->id,$tag3->id]);

      $event2->medias()->create([
        'link'=>'images/events/event2.jpg'
      ]);


      $event3 = $user1->events()->create([
       'title' => 'Event3',
       'short_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, cupiditate!',
       'long_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio alias labore deserunt dolore ex deleniti natus obcaecati molestiae excepturi atque!j',
       'start_date' =>date_format(date_create("2019-10-15 12:03"),"Y-m-d H:i:s"),
       'end_date' => date_format(date_create("2019-10-16 12:03"),"Y-m-d H:i:s")
      ]);
      $event3->tags()->attach([$tag1->id,$tag2->id,$tag3->id]);

      $event3->medias()->create([
        'link'=>'images/events/event3.jpg'
      ]);


      $event4 = $user1->events()->create([
       'title' => 'Event4',
       'short_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, cupiditate!',
       'long_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio alias labore deserunt dolore ex deleniti natus obcaecati molestiae excepturi atque!j',
       'start_date' =>date_format(date_create("2019-10-20 12:03"),"Y-m-d H:i:s"),
       'end_date' => date_format(date_create("2019-11-16 12:03"),"Y-m-d H:i:s")
      ]);
    $event4->tags()->attach([$tag1->id,$tag2->id,$tag3->id]);
      $event4->medias()->create([
        'link'=>'images/events/event4.jpg'
      ]);


      $event5 = $user1->events()->create([
       'title' => 'Event5',
       'short_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, cupiditate!',
       'long_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio alias labore deserunt dolore ex deleniti natus obcaecati molestiae excepturi atque!j',
       'start_date' =>date_format(date_create("2019-11-20 12:03"),"Y-m-d H:i:s"),
       'end_date' => date_format(date_create("2019-11-30 12:03"),"Y-m-d H:i:s")
      ]);
    $event5->tags()->attach([$tag1->id,$tag2->id,$tag3->id]);
      $event5->medias()->create([
        'link'=>'images/events/event5.jpg'
      ]);

      $event6 = $user1->events()->create([
       'title' => 'Event6',
       'short_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, cupiditate!',
       'long_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio alias labore deserunt dolore ex deleniti natus obcaecati molestiae excepturi atque!j',
       'start_date' =>date_format(date_create("2019-11-25 12:03"),"Y-m-d H:i:s"),
       'end_date' => date_format(date_create("2019-11-27 12:03"),"Y-m-d H:i:s")
      ]);

      $event6->medias()->create([
        'link'=>'images/events/event6.jpg'
      ]);
    $event6->tags()->attach([$tag1->id,$tag2->id,$tag3->id]);
      $event7 = $user1->events()->create([
       'title' => 'Event7',
       'short_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, cupiditate!',
       'long_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio alias labore deserunt dolore ex deleniti natus obcaecati molestiae excepturi atque!j',
       'start_date' =>date_format(date_create("2019-11-20 12:03"),"Y-m-d H:i:s"),
       'end_date' => date_format(date_create("2019-12-27 12:03"),"Y-m-d H:i:s")
      ]);
      $event7->medias()->create([
        'link'=>'images/events/event7.jpg'
      ]);
      $event7->tags()->attach([$tag1->id,$tag2->id,$tag3->id]);

    }
}
