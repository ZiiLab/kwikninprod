
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('chat-composer', require('./components/ChatComposer.vue'));
Vue.component('chat-log', require('./components/ChatLog.vue'));
Vue.component('chat-message', require('./components/ChatMessage.vue'));

const app = new Vue({
    el: '#app',
    data: {
        messages: [],
        usersInRoom: []
    },
    methods: {
        addMessage(message) {

          axios.get('/message?body='+message.body+'&sId='+message.streamId).then(res => {
          });


        }
    },

    created(){


        axios.get('/messages?sId='+ window.location.pathname.split('/').pop()).then(res => {
           this.messages = res.data;
        });

        Echo.join('chatroom')
            .here((users) => {
              this.usersInRoom = users;
        })
            .joining((user) => {
                this.usersInRoom.push(user);
        })
            .leaving((user) => {
                this.usersInRoom = this.usersInRoom.filter(u => u != user);
        })
            .listen('MeesagePosted', (e) => {
                this.messages.push({
            body: e.message.body,
            first_name: e.user.first_name
        });
            console.log(e);
    });

    }
});
