<!DOCTYPE html >
<html lang="en">
<head>
    @include('frontOffice.inc.head')
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css'>
    @yield('css')

</head>
<body>

  @auth
    @if (Auth::user()->progress == 0 and Auth::user()->type == 0)
      @include('frontOffice.inc.notification')
    @endif
  @endauth



    @include('frontOffice.inc.header')
    @yield('content')

@include('frontOffice.inc.footer')
@include('frontOffice.inc.scripts')



@yield('js')
</body>
</html>
