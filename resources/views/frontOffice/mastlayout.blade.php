<!DOCTYPE html >
<html lang="en">
<head>
    @include('frontOffice.inc.head')
</head>
<body>

@include('frontOffice.inc.mastheader')
@yield('content')

@include('frontOffice.inc.masthfooter')
@include('frontOffice.inc.scripts')
</body>
</html>
