<!-- Header for sign up page -->


<header id="masthead-pro">
    <div class="container">


        <nav id="site-navigation-pro">
            <ul class="sf-menu">
                <li class="normal-item-pro">
                    <a href="{{route('showHome')}}">Home</a>
                </li>
                <li class="normal-item-pro current-menu-item">
                    <a href="#">Terms and privacy rules</a>
                </li>

            </ul>
        </nav>


        <button class="btn btn-header-pro noselect" data-toggle="modal" data-target="#LoginModal" role="button">Sign In</button>


        <div id="mobile-bars-icon-pro" class="noselect"><i class="fas fa-bars"></i></div>

        <div class="clearfix"></div>
    </div><!-- close .container -->

    <nav id="mobile-navigation-pro">

        <ul id="mobile-menu-pro">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                <a href="dashboard-home.html">New Releases</a>
                <!-- Mobile Sub-Menu Example >
                <ul>
                    <li class="normal-item-pro">
                        <a href="#!">Sub-menu item 1</a>
                    </li>
                    <li class="normal-item-pro">
                        <a href="#!">Sub-menu item 2</a>
                    </li>
                    <li class="normal-item-pro">
                        <a href="#!">Sub-menu item 3</a>
                    </li>
                </ul>
                < End Mobile Sub-Menu Example -->
            </li>
            <li>
                <a href="signup-step1.html">Pricing Plans</a>
            </li>
            <li>
                <a href="faqs.html">FAQs</a>
            </li>
        </ul>
        <div class="clearfix"></div>

        <button class="btn btn-mobile-pro btn-green-pro noselect" data-toggle="modal" data-target="#LoginModal" role="button">Sign In</button>

    </nav>
</header>
@include('User::frontOffice.modals.loginModal')
