<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="api-base-url" content="{{ url('') }}" />
<meta name="user" @auth content="{{ Auth::user()->first_name}}" @endauth content="" >

<link rel="stylesheet" href="{{ asset ('frontOffice') }}/css/bootstrap.min.css">

          @if (!Auth::user())
          <style>
              #footer-pro {
                  margin-left: 240px;

              }

              #video-search-header-filtering {
                  width: calc(88vw - 160px);
              }

              main#col-main {
                 margin-right: -25px;
                 margin-left: 215px;
              }

              nav#sidebar-nav {
                  position: fixed;
                  top: 0;
                  left: 0;
                  width: 300px;
                  /* Sidebar Width */
                  z-index: 25;
              }

              #sidebar-bg:before {
                  background-color: #ffffff;
                  box-shadow: 0 0 12px rgba(0, 0, 0, 0.08);
                  content: "";
                  display: block;
                  height: 100%;
                  min-height: 100%;
                  position: fixed;
                  top: 0;
                  left: 0;
                  width: 239px;
                  /* Sidebar Width */
                  z-index: 1;
                  /* Fixes flashing bug with scrolling on Safari */
              }
          </style>
          @else
          <style media="screen">
              #footer-pro {
                  margin-left: 160px;
              }

              #video-search-header-filtering {
                  width: calc(100vw - 160px);
              }

              main#col-main {
                  margin-left: 161px;
              }

              nav#sidebar-nav {
                  position: fixed;
                  top: -26px;
                  left: 0;
                  width: 160px;
                  /* Sidebar Width */
                  z-index: 25;
              }

              #sidebar-bg:before {
                  background-color: #ffffff;
                  box-shadow: 0 0 12px rgba(0, 0, 0, 0.08);
                  content: "";
                  display: block;
                  height: 100%;
                  min-height: 100%;
                  position: fixed;
                  top: 0;
                  left: 0;
                  width: 160px;
                  /* Sidebar Width */
                  z-index: 1;
                  /* Fixes flashing bug with scrolling on Safari */
              }
          </style>
          @endif


<style>
 .tooltip-inner {
  max-width: 300px;
  padding: 0.25rem 0.5rem;
  color: #fff;
  text-align: center;
  background-color: red;
  border-radius: 0.25rem;
}

.bs-tooltip-auto[x-placement^=right] .arrow::before, .bs-tooltip-right .arrow::before {
    right: 0;
    border-width: .4rem .4rem .4rem 0;
    border-right-color: red;
}
</style>
<link rel="stylesheet" href="{{ asset ('frontOffice') }}/style.css">
<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Lato:400,700%7CMontserrat:300,400,600,700">

<link rel="stylesheet" href="{{ asset ('frontOffice') }}/icons/fontawesome/css/fontawesome-all.min.css"><!-- FontAwesome Icons -->
<link rel="stylesheet" href="{{ asset ('frontOffice') }}/icons/Iconsmind__Ultimate_Pack/Line%20icons/styles.min.css"><!-- iconsmind.com Icons -->

<link rel="stylesheet" href="{{ asset ('frontOffice') }}/notification/notification.css"><!-- Notification Css -->

<script src="{{asset('frontOffice')}}/js/libs/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js"></script><!-- jQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.js"></script>



<script src="{{asset('frontOffice/js/jquery.validate.js')}}" charset="utf-8"></script>
<script src="{{asset('frontOffice/js/additional-methods.js')}}" charset="utf-8"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" charset="utf-8"></script>
<title>Kwiknin - Homepage</title>
