@if (!Auth::user())

  <nav style="margin-left: -61px;" id="sidebar-nav"><!-- Add class="sticky-sidebar-js" for auto-height sidebar -->
  <div   class="row">
      <div style="width: 70%; margin-left: 79px" class="item-listing-container-skrn">
          <a href="{{route('showHome')}}">
              <img src="{{ asset ('frontOffice') }}/images/kwiknin-logo.jpg" alt="Listing">
          </a>
          <div class="item-listing-text-skrn-sidebar">

              <div class="item-listing-text-skrn-vertical-align">
                {{-- <h6 >
                    <adata-toggle="modal" href="#" data-target="#registerModal">"Join the Kwiknin Community!"
                    </a>
                </h6> --}}

                <h6 style="font-size: 16px!important">
                    <a href="#">"Discover Lightworkers All Over The World"
                    </a>
                </h6>
                <a style="      margin-bottom: 12px;
    padding: 6px 20px; margin-left: 41px; background: #02536B;" class="btn btn-green-pro btn-slider-pro btn-shadow-pro afterglow" data-toggle="modal" href="#" data-target="#registerModal">Sign up</a>
              </div><!-- close .item-listing-text-skrn-vertical-align -->
          </div><!-- close .item-listing-text-skrn -->
      </div>
  </div>

      <div class="clearfix"></div>
  </nav>
@else

  <nav id="sidebar-nav">
			<!-- Add class="sticky-sidebar-js" for auto-height sidebar -->
			<ul id="vertical-sidebar-nav" class="sf-menu">
				<li class="normal-item-pro current-menu-item">
					<a href="{{route('showHome')}}">
						<span class="icon-Home"></span>
						Home
					</a>
				</li>

				<li class="normal-item-pro">
					<a href="#" data-toggle="modal" data-target="#startStreamingModal">
						<span class="icon-Movie"></span>
						Start New Session
					</a>
				</li>


        <li class="normal-item-pro">
					<a href="{{route('showStore')}}">
						<span class="icon-Clothing-Store"></span>
						Store
					</a>
				</li>

				<li class="normal-item-pro">
					<a href="{{route('showEvents')}}">
						<span class="icon-Clock"></span>
						Events
					</a>
				</li>

			</ul>
			<div class="clearfix"></div>
		</nav>

@endif
@include('User::frontOffice.modals.registerModal')
@include('Broadcasting::frontOffice.modals.startStreamingModal')
