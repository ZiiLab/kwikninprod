<footer id="footer-pro">
  <div class="container">
    <div class="row">
      <div class="col-md">
        <div class="copyright-text-pro">&copy; Copyright 2019 Kwiknin. All Rights Reserved</div>
      </div><!-- close .col -->
      <div class="col-md">
        <ul class="social-icons-pro">
          <li class="facebook-color"><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
          <li class="twitter-color"><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
          <li class="youtube-color"><a href="#"><i class="fab fa-youtube"></i></a></li>
          <li class="vimeo-color"><a href="#" target="_blank"><i class="fab fa-vimeo-v"></i></a></li>
        </ul>
      </div><!-- close .col -->
    </div><!-- close .row -->
  </div><!-- close .container -->
</footer>
