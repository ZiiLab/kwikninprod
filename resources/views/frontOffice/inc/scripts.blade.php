<!-- Required Framework JavaScript -->
<script src="{{asset('frontOffice')}}/js/libs/popper.min.js" defer></script><!-- Bootstrap Popper/Extras JS -->
<script src="{{asset('frontOffice')}}/js/libs/bootstrap.min.js" defer></script><!-- Bootstrap Main JS -->
<!-- All JavaScript in Footer -->

<!-- Additional Plugins and JavaScript -->
<script src="{{asset('frontOffice')}}/js/navigation.js" defer></script><!-- Header Navigation JS Plugin -->
<script src="{{asset('frontOffice')}}/js/jquery.flexslider-min.js" defer></script><!-- FlexSlider JS Plugin -->
<script src="{{asset('frontOffice')}}/js/jquery-asRange.min.js" defer></script><!-- Range Slider JS Plugin -->
<script src="{{asset('frontOffice')}}/js/circle-progress.min.js" defer></script><!-- Circle Progress JS Plugin -->
<script src="{{asset('frontOffice')}}/js/afterglow.min.js" defer></script><!-- Video Player JS Plugin -->
<script src="{{asset('frontOffice')}}/js/script.js" defer></script><!-- Custom Document Ready JS -->
<script src="{{asset('frontOffice')}}/js/script-dashboard.js" defer></script><!-- Custom Document Ready for Dashboard Only JS -->
