#


  <style media="screen">

  footer#footer-pro-masth {
  	border-top:1px solid rgba(0,0,0,  0.09);
  }
    #footer-pro-masth{
      width: 100%;
    }

    footer#footer-pro-masth ul.social-icons-pro {
    	padding:20px 0px;
    }

    footer#footer-pro-masth ul.social-icons-pro {
    	text-align:right;
    }

    .copyright-text-pro,
    footer#footer-pro-masth ul.social-icons-pro {
      text-align:center;
    }



  </style>



<footer id="footer-pro-masth">
  <div class="container">
    <div class="row">
      <div class="col-md">
        <div class="copyright-text-pro">&copy; Copyright 2019 Kwiknin. All Rights Reserved</div>
      </div><!-- close .col -->
      <div class="col-md">
        <ul class="social-icons-pro">
          <li class="facebook-color"><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
          <li class="twitter-color"><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
          <li class="youtube-color"><a href="#" target="_blank"><i class="fab fa-youtube"></i></a></li>
          <li class="vimeo-color"><a href="#" target="_blank"><i class="fab fa-vimeo-v"></i></a></li>
        </ul>
      </div><!-- close .col -->
    </div><!-- close .row -->
  </div><!-- close .container -->
</footer>
