<header id="videohead-pro" class="sticky-header">

  <div id="video-logo-background"><a href="{{route('showHome')}}"><img src="{{ asset ('frontOffice') }}/images/kwiknin-logo-transparent.png" alt="Logo"></a></div>
  @if (!Auth::user())

    <div id="header-user-logIn">
        <div id="header-user-logIn-click" data-toggle="modal" data-target="#LoginModal" class="noselect">
            <a href="#">Log In</a>
        </div><!-- close #header-user-profile-click -->
    </div><!-- close #header-user-profile -->

    <div id="header-user-signUp">
        <div id="header-user-signUp-click" data-toggle="modal" data-target="#registerModal" class="noselect">
            <a href="#">Sign up</a>
        </div><!-- close #header-user-profile-click -->
    </div><!-- close #header-user-notification -->


    @else

      <div id="header-user-profile">
    <div style="cursor: pointer;" id="header-user-profile-click" class="">
      <img style="height: 40px!important; border-radius: 50%" src="{{Auth::user()->image ? asset(Auth::user()->image) : asset('frontOffice/images/unknown.png')}}" alt="{{Auth::user()->first_name}}">
      <div id="header-username" value="{{Auth::user()->first_name}}" >{{Auth::user()->first_name.' '.Auth::user()->last_name}}</div><i class="fas fa-angle-down"></i>
    </div><!-- close #header-user-profile-click -->
    <div id="header-user-profile-menu">
      <ul>
        <li><a href="{{route('showProfile')}}"><span class="icon-User"></span>My Profile</a></li>
        <li><a href="{{route('showUserBookmark')}}">	<span class="icon-Bookmark"></span>My Bookmarks</a></li>
        <li><a href="{{ route('handleLogout') }}"><span class="icon-Power-3"></span>Log Out</a></li>
      </ul>
    </div><!-- close #header-user-profile-menu -->
  </div><!-- close #header-user-profile -->

  <div id="header-user-notification">
    <div style="cursor: pointer;" id="header-user-notification-click" class="noselect">
      <i class="far fa-bell"></i>
      <span class="user-notification-count">0</span>
    </div><!-- close #header-user-profile-click -->
    <div id="header-user-notification-menu">
      <h3>Notifications</h3>
      <div id="header-notification-menu-padding">
        <ul id="header-user-notification-list">
                </ul>
        <div class="clearfix"></div>
      </div><!-- close #header-user-profile-menu -->
    </div>
  </div><!-- close #header-user-notification -->
  @endif
</header>
@include('User::frontOffice.modals.loginModal')
@include('User::frontOffice.modals.registerModal')
