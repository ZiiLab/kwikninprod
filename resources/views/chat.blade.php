<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('frontOffice/chatWidget/style.css')}}">
    <link rel="stylesheet" href="{{ asset ('frontOffice') }}/css/bootstrap.min.css">
    <title>Chat</title>
</head>
<body>
<h1>Chat</h1>
<div id="app">

    <div class="row chat-window col-xs-12 col-md-12" id="chat_window_1" style="padding:0;margin:0">
        <div class="col-xs-12 col-md-12" style="padding:0">
            <div class="card">
                <div class="card-heading top-bar">
                    <div class="col-md-8 col-xs-8" style="margin-left:42px;">
                        <p class="card-title"><span class="glyphicon glyphicon-comment"></span> Chat </p>
                    </div>
                    <div class="col-md-4 col-xs-4" style="text-align: right;">
                        <a href="#"><span id="minim_chat_window" class="glyphicon glyphicon-minus icon_minim"></span></a>
                        <a href="#"><span class="glyphicon glyphicon-remove icon_close" data-id="chat_window_1"></span></a>
                    </div>
                </div>
                <div class="card-body msg_container_base">

                    <chat-log :messages="messages"></chat-log>
                </div>
                <chat-composer v-on:messagesent="addMessage"></chat-composer>
            </div>
        </div>
    </div>


</div>
<script src="js/app.js" charset="utf-8"></script>
</body>
</html>