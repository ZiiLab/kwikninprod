<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'twilio' => [
        'sid' => env('TWILIO_ACCOUNT_SID','AC37fb103e94404ef099da7fc960473bba'),
        'token' => env('TWILIO_ACCOUNT_TOKEN','21a61465ee58a3db5d8a93e65a247b81'),
        'key' => env('TWILIO_API_KEY','SKb33ff57907b733ae5409b8a39c44d442'),
        'secret' => env('TWILIO_API_SECRET','b49Vimrk0jLlp4evdsfQgEUj29XgOzJL')
    ]

];
