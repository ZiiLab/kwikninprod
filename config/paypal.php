<?php

return array(

    /**
     * Set our Sandbox and Live credentials
     */
    'sandbox_client_id' => env('PAYPAL_SANDBOX_CLIENT_ID', 'Ad6qEvNhm0MwCsHqBDr0BA0Vq9NERcMZj7lGfouuQUQaHMsKCKtN_96G77lG3uwwnjc-SjGg5ExP-Jyr'),
    'sandbox_secret' => env('PAYPAL_SANDBOX_SECRET', 'EGYax6xUSNXX8BsXfGjGwS_cRjECx1slOpxo6A7Xa6b-yCQCzINao4hxXkXrDq6vUEjl9yJ0tSfU1ZXU'),
    'live_client_id' => env('PAYPAL_LIVE_CLIENT_ID', 'Ad6qEvNhm0MwCsHqBDr0BA0Vq9NERcMZj7lGfouuQUQaHMsKCKtN_96G77lG3uwwnjc-SjGg5ExP-Jyr'),
    'live_secret' => env('PAYPAL_LIVE_SECRET', 'EGYax6xUSNXX8BsXfGjGwS_cRjECx1slOpxo6A7Xa6b-yCQCzINao4hxXkXrDq6vUEjl9yJ0tSfU1ZXU'),
    'paypal_sanbox_plan_id'=>env('PAYPAL_SANDBOX_PLAN_ID', 'P-04X094766C974534VWVYZ3SI'),
    'paypal_live_plan_id'=>  env('PAYPAL_LIVE_SECRET', 'P-04X094766C974534VWVYZ3SI'),

    /**
     * SDK configuration settings
     */
    'settings' => array(

        /**
         * Payment Mode
         *
         * Available options are 'sandbox' or 'live'
         */
        'mode' => env('PAYPAL_MODE', 'sandbox'),

        // Specify the max connection attempt (3000 = 3 seconds)
        'http.ConnectionTimeOut' => 3000,

        // Specify whether or not we want to store logs
        'log.LogEnabled' => true,

        // Specigy the location for our paypal logs
        'log.FileName' => storage_path() . '/logs/paypal.log',

        /**
         * Log Level
         *
         * Available options: 'DEBUG', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the DEBUG level and decreases
         * as you proceed towards ERROR. WARN or ERROR would be a
         * recommended option for live environments.
         *
         */
        'log.LogLevel' => 'DEBUG'
    ),
);
